# Use a base image where CB-Geo MPM dependencies are already installed. If a newer version requires an additional dependency, please update `Dockerfile_base`, build the image locally and push it to `registry.forgemia.inra.fr/mpm-at-recover/cbgeo:base`
FROM registry.forgemia.inra.fr/mpm-at-recover/cbgeo:base

LABEL maintainer="sacha.duverger@inrae.fr"

ENV DEBIAN_FRONTEND=noninteractive

# Create a user
RUN useradd --create-home --shell /bin/bash user

# Set the working directory
WORKDIR /home/user/mpm

# Copy the project files into the container
COPY . .

# Create build directory
RUN mkdir -p /home/user/mpm/build-24.04

# Set the working directory to the build directory
WORKDIR /home/user/mpm/build-24.04

# Change ownership to the user
RUN chown -R user:user /home/user
USER user

# Create the install directory (~/bin)
RUN mkdir /home/user/bin
RUN echo "\n# Add ~/bin to PATH\nexport PATH=$PATH:$HOME/bin" >> /home/user/.bashrc

# Print setup information
RUN echo "Dependencies installed" && \
    echo "Build setup over"

# Compile CB-Geo MPM
USER user
RUN echo "Start compiling CB-Geo MPM" && \
    cmake -DWITH_TESTS=ON -DUSE_PYTHON=ON -DCMAKE_INSTALL_PREFIX=/home/user/bin .. && \
    make -j8 install && \
    echo "Build is over"

# Setup TERM environement variable, to have colors in bash
ENV TERM=xterm-256color

# Set the default command to run your application
CMD ["bash"]
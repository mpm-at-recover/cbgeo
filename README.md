[![pipeline status](https://forgemia.inra.fr/mpm-at-recover/cbgeo/badges/main/pipeline.svg)](https://forgemia.inra.fr/mpm-at-recover/cbgeo/-/commits/main)

The present Material Point Method (MPM) code has been developed as a fork of the [CB-Geo MPM](https://github.com/cb-geo/mpm) software initially proposed by the [CB-Geo Computational Geomechanics Research Group](https://www.cb-geo.com) (starting from its March 2021 86ba10eeca3badba31b37c49e11a3930ee6f2c16 revision, still included herein in the `gitHubVersion` branch).

# Documentation

The [original documentation of CB-Geo MPM](https://mpm.cb-geo.com/) is a great place to start understanding how to run a simulation, and what are the various features available.

A quite detailed presentation of source code is also available in § 3.3 of the following PhD manuscript:

Duverger, S. (2023) [A multi-scale, MPMxDEM, numerical modelling approach for geotechnical structures under severe loading](https://theses.hal.science/tel-04101270) PhD Thesis, Aix-Marseille University

# Installation on Ubuntu 22.04

It is assumed that the user has obtained this project source files (e.g. with `git clone git@forgemia.inra.fr:mpm-at-recover/cbgeo.git`) and created a build directory where to execute compilation (e.g. `cbgeo/build`).

## Dependencies

First, install the following dependencies:
```shell
$ sudo apt-get install \
    gcc g++ git cmake \
    libboost-all-dev libeigen3-dev software-properties-common freeglut3-dev libxt-dev libtbb-dev \
    python3 python3-numpy \
    libvtk9-dev qtbase5-dev qtdeclarative5-dev
```
Note that:

 - `python3` and `python3-numpy` are only required by the Python coupling (if `USE_PYTHON=ON`),
 - `libvtk9-dev`, `qtbase5-dev` and `qtdeclarative5-dev` are only required by VTK (if `USE_VTK=ON`).

## Executing compilation

After moving into the above build directory (e.g., `cbgeo/build`) in all cases, a default installation can be simply obtained running:
```shell
$ cmake ..
$ make
```
and the resulting executable will then be obtained as `mpm` within that build directory (e.g., `cbgeo/build/mpm`).

On the other hand, the behavior can be customized before running `make` using the below options available as `cmake` variables (e.g., `cmake -DWITH_TESTS=ON ...`):

 - `CMAKE_INSTALL_PREFIX`: if specified, the executables will be copied from the build directory into this directory when compiling with a `make install` step. This may require to update linker library path to avoid linking startup errors, with e.g.:
 ```shell
 $ echo -e "\n# Add libjawt.so and libjvm.so directories to LD_LIBRARY_PATH\nexport LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:/usr/lib/jvm/java-11-openjdk-amd64/lib/:/usr/lib/jvm/java-11-openjdk-amd64/lib/server/" >> /home/toto/.bashrc # replace toto with your own user name
 ```
 - `EXECUTABLE_NAME`: name of the executable to be produced, default to `mpm`. It also controls the name of the additional executable for the Python coupling (`$EXECUTABLE_NAME-python` so `mpm-python` by default).
 - `CMAKE_BUILD_TYPE`: common `CMake` variable, possible values in this project are `Release` (default), `Debug` and `ASan` (compiling using clang with the `-fsanitize=address` flag).
 - `WITH_TESTS`: this replaces `MPM_BUILD_TESTING` from the initial version of the code (at GitHub) and will produce, if set to `ON` (default to `OFF`), an additional executable named `$EXECUTABLE_NAME-test` (so `mpm-test` by default) that runs regression tests as described below.
 - `TIMING_DEPTH`: depth, with respect to the main MPM loop, of the probing for the timing feature. The timing feature can be deactivated by setting it to a negative value, e.g. `-DTIMING_DEPTH=-1`, or it can be set to `0` (for only measuring the function calls in the main loop), `1` (to probe one level deeper, default value), or `2` (one level further, much more expensive in terms of execution time).
 - `PYTHON_VERSION`: the Python version to use for the Python coupling. By default, the first version found by CMake is used.
 - `USE_PYTHON`: whether to compile the Python parts of the code, default to `OFF`. Note that setting `PYTHON_VERSION` automatically sets `USE_PYTHON` to `ON`.
 - `USE_VTK`: whether to compile the VTK parts of the code, default to `ON`. If enabled, VTK files will be saved for each saved step.
 - `USE_MPI`: whether to compile the MPI parts of the code, default to `OFF`. Note that the current MPI implementation was not tested with the rest of the new code from this branch, and will certainly conflicts the TBB implementation.

Parallel compilation can also be triggered with, e.g., `make -j 8` depending on your hardware (8 CPU cores would be used here).

## Regression tests

A number of regression tests can be run through the executable suffixed `-test` (`mpm-test` by default) if one first compiles with the `WITH_TESTS` option set to `ON`. If the tests are successful, the following message should appear (or similar):
```
===============================================================================
All tests passed (41081 assertions in 69 test cases)
```

In the case of a failure, more details will be printed about the failed test. The [`Catch2`](https://github.com/catchorg/Catch2/) library used to perform the tests makes available several runtime options for the tests. One can for instance focus on the [`SECTION("MohrCoulomb check yield correction based on current stress")` in the `TEST_CASE("MohrCoulomb is checked in 2D (cohesion only*)` part](https://forgemia.inra.fr/mpm-at-recover/cbgeo/-/blob/152a765010d2df10dc5cb1dc9c228701aad48758/tests/materials/mohr_coulomb_test.cc#L386) with

```shell
./mpm-test "MohrCoulomb is checked in 2D (cohesion only*" -c "MohrCoulomb check yield correction based on current stress"
```

See `mpm-test -?` for more details and Catch2 documentation, e.g., [here](https://github.com/catchorg/Catch2/blob/devel/docs/command-line.md).

# Usage

Running a simulation in particular requires a [`JSON` file for input configuration](https://mpm.cb-geo.com/#/user/preprocess/input), which can be obtained (together with all other necessary input files, e.g., about the grid mesh) through the [Python module (PyCBG)](https://forgemia.inra.fr/mpm-at-recover/pycbg/-/tree/main?ref_type=heads) also provided. The latter is up to date with the new features brought by the present fork of CB-Geo MPM.

With a `input_file.json` available in some directory `working_dir`, the simulation is launched with, e.g.:

```shell
$ ./mpm -i input_file.json -f working_dir
```

where `./mpm` can be replaced with `CMAKE_INSTALL_PREFIX/EXECUTABLE_NAME` depending on used `cmake` options and more details about the syntax can be obtained with `./mpm --help`

Post-processing files (with material points i.e. particles data in .csv files that replace the [.hdf5 ones](https://mpm.cb-geo.com/#/user/postprocess/hdf5) from initial code) will be created in `working_dir/results` on the fly while simulation runs. Those can conveniently be post-processed in Python with the same [PyCBG](https://forgemia.inra.fr/mpm-at-recover/pycbg/-/tree/main) Python module previously used for the input files.

# Advanced considerations

## Using the Docker image

The CI pipeline in this project automatically pushes docker image resulting from the most recent build job to the container registry `registry.forgemia.inra.fr/mpm-at-recover/cbgeo`. All images in this registry are tagged using the short SHA1 of the most recent commit, except for 2 images: the one tagged `latest` and the one tagged `base`. The former is pushed by the pipeline only if the current branch is `main`, so this image should always be operational. The latter, `base`, is used only to built the other images faster and is not intended for the users.

Here is a summary of how to retrieve and run the docker image from the container registry (requires access privileges):
```shell
$ docker login registry.forgemia.inra.fr -u myusername # Enter password / access token when prompted
$ docker pull registry.forgemia.inra.fr/mpm-at-recover/cbgeo # Pulls the tag 'latest' by default, suffiw with ':mytag' for a specific tag
$ docker run -it registry.forgemia.inra.fr/mpm-at-recover/cbgeo
```

# Citation

In case of publishing results obtained with the code, please consider quoting the following papers:

Duverger, S., Duriez, J., Philippe, P. & Bonelli, S. (2024) Critical Comparison of Motion Integration Strategies and Discretization Choices in the Material Point Method. Archives of Computational Methods in Engineering. [https://doi.org/10.1007/s11831-024-10170-y](https://doi.org/10.1007/s11831-024-10170-y) (see also alternate fulltext accesses [here](https://rdcu.be/dULJc) or [there](https://hal.inrae.fr/hal-04707839))

Kumar, K., Salmond, J., Kularathna, S., Wilkes, C., Tjung, E., Biscontin, G., & Soga, K. (2019). Scalable and modular material point method for large scale simulations. 2nd International Conference on the Material Point Method. Cambridge, UK. [https://arxiv.org/abs/1909.13380](https://arxiv.org/abs/1909.13380)
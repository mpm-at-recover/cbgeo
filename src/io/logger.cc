#include "logger.h"

// Default stdout_sink
std::shared_ptr<spdlog::sinks::sink> mpm::stdout_sink = std::make_shared<spdlog::sinks::stdout_color_sink_mt>();

// Function to redirect the output to a file
void mpm::use_file_sink(const std::string& filename) {
    stdout_sink = std::make_shared<spdlog::sinks::basic_file_sink_mt>(filename);

    // Update loggers
    mpm::Logger::io_logger->sinks().clear();
    mpm::Logger::io_logger->sinks().emplace_back(stdout_sink);

    mpm::Logger::io_mesh_logger->sinks().clear();
    mpm::Logger::io_mesh_logger->sinks().emplace_back(stdout_sink);

    mpm::Logger::io_mesh_ascii_logger->sinks().clear();
    mpm::Logger::io_mesh_ascii_logger->sinks().emplace_back(stdout_sink);

    mpm::Logger::point_generator_logger->sinks().clear();
    mpm::Logger::point_generator_logger->sinks().emplace_back(stdout_sink);

    mpm::Logger::mpm_logger->sinks().clear();
    mpm::Logger::mpm_logger->sinks().emplace_back(stdout_sink);

    mpm::Logger::mpm_base_logger->sinks().clear();
    mpm::Logger::mpm_base_logger->sinks().emplace_back(stdout_sink);

    mpm::Logger::mpm_explicit_logger->sinks().clear();
    mpm::Logger::mpm_explicit_logger->sinks().emplace_back(stdout_sink);

    mpm::Logger::mpm_explicit_usf_logger->sinks().clear();
    mpm::Logger::mpm_explicit_usf_logger->sinks().emplace_back(stdout_sink);

    mpm::Logger::mpm_explicit_usl_logger->sinks().clear();
    mpm::Logger::mpm_explicit_usl_logger->sinks().emplace_back(stdout_sink);
}

// Create a logger for IO
const std::shared_ptr<spdlog::logger> mpm::Logger::io_logger =
    spdlog::stdout_color_st("IO");

// Create a logger for reading mesh
const std::shared_ptr<spdlog::logger> mpm::Logger::io_mesh_logger =
    spdlog::stdout_color_st("IOMesh");

// Create a logger for reading ascii mesh
const std::shared_ptr<spdlog::logger> mpm::Logger::io_mesh_ascii_logger =
    spdlog::stdout_color_st("IOMeshAscii");

// Create a logger for point generator
const std::shared_ptr<spdlog::logger> mpm::Logger::point_generator_logger =
    spdlog::stdout_color_st("PointGenerator");

// Create a logger for MPM
const std::shared_ptr<spdlog::logger> mpm::Logger::mpm_logger =
    spdlog::stdout_color_st("MPM");

// Create a logger for MPM Base
const std::shared_ptr<spdlog::logger> mpm::Logger::mpm_base_logger =
    spdlog::stdout_color_st("MPMBase");

// Create a logger for MPM Explicit
const std::shared_ptr<spdlog::logger> mpm::Logger::mpm_explicit_logger =
    spdlog::stdout_color_st("MPMExplicit");

// Create a logger for MPM Explicit USF
const std::shared_ptr<spdlog::logger> mpm::Logger::mpm_explicit_usf_logger =
    spdlog::stdout_color_st("MPMExplicitUSF");

// Create a logger for MPM Explicit USL
const std::shared_ptr<spdlog::logger> mpm::Logger::mpm_explicit_usl_logger =
    spdlog::stdout_color_st("MPMExplicitUSL");

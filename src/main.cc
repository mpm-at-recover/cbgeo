#include <iostream>
#include <memory>

#if USE_MPI
#include "mpi.h"
#endif
#include "spdlog/spdlog.h"
#include "spdlog/sinks/basic_file_sink.h"
#include "logger.h"

#include "git.h"
#include "io.h"
#include "mpm.h"

#include "tbb_tools.h"
#include <thread>

#include "registry_initializer.h"

int main(int argc, char** argv) {

  registerEverything();

#if USE_MPI
  // Initialise MPI
  MPI_Init(&argc, &argv);
  int mpi_rank;
  MPI_Comm_rank(MPI_COMM_WORLD, &mpi_rank);
  // Get number of MPI ranks
  int mpi_size;
  MPI_Comm_size(MPI_COMM_WORLD, &mpi_size);

  // Allocate enough space to issue the buffered send
  int mpi_buffer_size = 2000000000;
  void* mpi_buffer = malloc(mpi_buffer_size);
  // Pass the buffer allocated to MPI so it uses it when we issue MPI_Bsend
  MPI_Buffer_attach(mpi_buffer, mpi_buffer_size);

#endif

  try {
    // Logger level (trace, debug, info, warn, error, critical, off)
    spdlog::set_level(spdlog::level::trace);

    // Initialise logger
    std::shared_ptr<spdlog::logger> console;

    // Create an IO object
    auto io = std::make_shared<mpm::IO>(argc, argv);

    // Redirect output if logfile was specified by the user
    std::string logfile = io->logfile();
    if (!logfile.empty()) {
      mpm::use_file_sink(logfile);
      console = spdlog::basic_logger_mt("main (file logger)", logfile);
    }
    else console = spdlog::stdout_color_mt("main");

    // Print git revision
    console->info("git revision: {}", GitMetadata::CommitSHA1());

    // If number of threads are positive set to nthreads
    unsigned nthreads = io->nthreads() > 0 ? io->nthreads() : std::thread::hardware_concurrency();
    tbb_tools::initialise_task_arena(nthreads);

    // Get analysis type
    const std::string analysis = io->analysis_type();

    // Create an MPM analysis
    auto mpm =
        Factory<mpm::MPM, const std::shared_ptr<mpm::IO>&>::instance()->create(
            analysis, std::move(io));
    // Solve
    mpm->solve();

    // Simulation successful
    return 0;

  } catch (std::exception& exception) {
    std::cerr << "MPM main: " << exception.what() << std::endl;
#if USE_MPI
    free(mpi_buffer);
    MPI_Buffer_detach(&mpi_buffer, &mpi_buffer_size);
    MPI_Abort(MPI_COMM_WORLD, 1);
#endif
    std::terminate();
  }

#if USE_MPI
  free(mpi_buffer);
  MPI_Buffer_detach(&mpi_buffer, &mpi_buffer_size);
  MPI_Finalize();
#endif
}

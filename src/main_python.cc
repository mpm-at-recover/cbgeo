#if USE_PYTHON

#include <iostream>
#include <fstream>
#include <memory>

#include "spdlog/spdlog.h"

#include "io.h"
#include "json.hpp"

#include "registry_initializer.h"

#include <poll.h>

#include <_python_model_mat.h>
#include <unistd.h>
#include <dirent.h>
#include <sys/resource.h>

#define errExit(msg)    do { perror(msg); exit(EXIT_FAILURE); } while (0)

//! Send a message to parent
void send_message(char* fifo_chr, std::string message_str) {
  // Convert message to char*
  char message[900];
  sprintf(message, "%s", message_str.c_str());

  // Open FIFO in write mode
  int fd = open(fifo_chr, O_WRONLY);
  if (fd<0) {
    std::cout << "\nCannot find named pipe " << fifo_chr << "\n";
  }

  // Write message and close the FIFO
  int res = write(fd, message, strlen(message)+1);
  if (res<0) {
    std::cout << "\nCannot send message\n";
  }
  close(fd);
}

//! Receive a message from parent
std::string receive_message(char* fifo_chr) {
  // Open FIFO in read mode
  int fd = open(fifo_chr, O_RDONLY);
  if (fd<0) {
    std::cout << "\nCannot find named pipe " << fifo_chr << "\n";
  }

  // Create poll to wait for response (reduce CPU usage)
  struct pollfd *pfd;
  pfd = (struct pollfd *) calloc(1, sizeof(struct pollfd));
  if (pfd == NULL) {
    errExit("malloc");
  }

  pfd[0].fd = fd;
  if (pfd[0].fd == -1) {
    errExit("open");
  }     

  pfd[0].events = POLLIN;

  // Get message and close FIFO
  char message[900];
  int ready = poll(pfd, 1, -1);
  if (ready == -1) {
    errExit("poll");
  }
  free(pfd);

  int sz = read(fd, message, sizeof(message));
  if (sz<0) {
   std::cout << "\nCannot receive message\n";
  }
  close(fd);

  // Convert message into a string
  std::string message_str(message);

  return message_str;
}

std::vector<std::string> str_split(const std::string &s, char delim) {
    std::vector<std::string> result;
    std::stringstream ss (s);
    std::string item;
    while (getline (ss, item, delim)) {
        result.push_back (item);
    }
    return result;
}

int main(int argc, char** argv) {
  registerEverything();
  
  try {
    // Set CPU affinity
    int part_id = atoi(argv[4]);
    cpu_set_t  mask;
    CPU_ZERO(&mask);
    CPU_SET(atoi(argv[5]), &mask);
    int result = sched_setaffinity(0, sizeof(mask), &mask);

    // Get input JSON file
    std::ifstream ifs(argv[1]);
    Json input_json = Json::parse(ifs);

    Json material_props = input_json.at("materials")[0];
    material_props += Json::object_t::value_type("input_file_path", argv[1]);

    // Create the logger string
    std::string logger_string = "PythonModel-MP" + std::to_string(part_id);
    material_props += Json::object_t::value_type("logger_string", logger_string);

    // Initialize the Python session
    pymat::init_session(material_props);

    // Create state variabes
    pymat::dense_map state_variables = pymat::initialise_state_variables();

    // Begin compute stress loop
    std::string input_str, output_str;
    
    while (1) {
      // Wait for input
      input_str = receive_message(argv[3]);

      // Construct stress and dstrain input
      pymat::Vector6d stress, dstrain;
      std::vector<std::string> splited_input = str_split(input_str, ' ');
      for (unsigned i=0; i<6; i++) {
        std::istringstream in1(splited_input[i]);
        stress[i] = atof(in1.str().c_str());

        std::istringstream in2(splited_input[i+6]);
        dstrain[i] = atof(in2.str().c_str());
      }

      // Construct state variables input
      int n_state_vars = splited_input.size()-12;
      for (unsigned i=12; i < n_state_vars+12; i++) {
        std::string var_name = "svars_";
        var_name += std::to_string(i-12);
        std::istringstream in3(splited_input[i]);
        state_variables.at(var_name) = atof(in3.str().c_str());
      }

      // Compute stress
      pymat::Vector6d out_stress = pymat::compute_stress(stress, dstrain, &state_variables, part_id);

      // Construct output string
      std::ostringstream streamObj;
      for (unsigned i=0; i < n_state_vars+6; i++) {
        if (i>5) { // the current index corresponds to a state variable 
          std::string var_name = "svars_";
          var_name += std::to_string(i-6);
          streamObj << state_variables.at(var_name) << " ";
        }
        else { // the current index corresponds to a stress component 
          streamObj << out_stress[i] << " ";
        }
      }
      std::string output_str = streamObj.str();

      // Send output string
      send_message(argv[3], output_str);
    }

  } catch (std::exception& exception) {
    std::cerr << "Python launcher: " << exception.what() << std::endl;
    std::terminate();
  }
}

#endif // USE_PYTHON
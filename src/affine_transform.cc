#include "affine_transform.h"

// 1D
// M=[-1 1; 1 1];
// K1 = transpose(M) * inverse (M*transpose(M));
const Eigen::Matrix<double, 2, 1> mpm::TransformR2UAffine<1>::KA =
    (Eigen::Matrix<double, 2, 1>() << -0.500000f, 0.500000f).finished();

const Eigen::Matrix<double, 2, 1> mpm::TransformR2UAffine<1>::Kb =
    (Eigen::Matrix<double, 2, 1>() << 0.500000f, 0.500000f).finished();

// 2D
// M=[-1 1 1 -1;-1 -1 1 1;1 1 1 1];
// K2 = transpose(M) * inverse (M*transpose(M));
// clang-format off
const Eigen::Matrix<double, 4, 2> mpm::TransformR2UAffine<2>::KA =
    (Eigen::Matrix<double, 4, 2>() <<
     -0.250000f, -0.2500000f,
      0.250000f, -0.2500000f,
      0.250000f,  0.2500000f,
     -0.250000f,  0.2500000f).finished();
// clang-format on

const Eigen::Matrix<double, 4, 1> mpm::TransformR2UAffine<2>::Kb =
    (Eigen::Matrix<double, 4, 1>() << 0.250000f, 0.250000f, 0.250000f,
     0.250000f)
        .finished();

// 3D
// clang-format off
// M=[-1  1  1 -1 -1  1 1 -1; ...
//    -1 -1  1  1 -1 -1 1  1; ...
//    -1 -1 -1 -1  1  1 1  1; ...
//     1  1  1  1  1  1 1  1];
// K3 = transpose(M) * inverse(M*transpose(M))
const Eigen::Matrix<double, 8, 3> mpm::TransformR2UAffine<3>::KA =
    (Eigen::Matrix<double, 8, 3>() <<
     -0.125000f, -0.125000f, -0.125000f,
      0.125000f, -0.125000f, -0.125000f,
      0.125000f,  0.125000f, -0.125000f,
     -0.125000f,  0.125000f, -0.125000f,
     -0.125000f, -0.125000f,  0.125000f,
      0.125000f, -0.125000f,  0.125000f,
      0.125000f,  0.125000f,  0.125000f,
     -0.125000f,  0.125000f,  0.125000f).finished();
// clang-format on

const Eigen::Matrix<double, 8, 1> mpm::TransformR2UAffine<3>::Kb =
    (Eigen::Matrix<double, 8, 1>() << 0.125000f, 0.125000f, 0.125000f,
     0.125000f, 0.125000f, 0.125000f, 0.125000f, 0.125000f)
        .finished();

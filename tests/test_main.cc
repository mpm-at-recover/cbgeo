#define CATCH_CONFIG_FAST_COMPILE
#define CATCH_CONFIG_RUNNER

#include <iostream>

#include "registry_initializer.h"

#include <catch2/catch.hpp>
// MPI
#if USE_MPI
#include "mpi.h"
#endif

const double absTolerance = 1E-8;

int main(int argc, char** argv) {

  registerEverything();

  try {
    Catch::Session session;
    // Let Catch (using Clara) parse the command line
    int returnCode = session.applyCommandLine(argc, argv);
    if (returnCode != 0)  // Indicates a command line error
      return returnCode;

#if USE_MPI
    // Initialise MPI
    MPI_Init(&argc, &argv);
#endif

    int result = session.run();

#if USE_MPI
    MPI_Finalize();
#endif

    return result;
  } catch (std::exception& exception) {
    std::cerr << "Test: " << exception.what() << std::endl;
#if USE_MPI
    MPI_Abort(MPI_COMM_WORLD, 1);
#endif
  }
}

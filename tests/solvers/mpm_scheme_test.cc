#include <cmath>
#include <limits>
#include <memory>
#include <iostream>

#include <Eigen/Dense>
#include <boost/filesystem.hpp>

#include <catch2/catch.hpp>
// MPI
#if USE_MPI
#include "mpi.h"
#endif

#include "constraints.h"
#include "element.h"
#include "function_base.h"
#include "hexahedron_element.h"
#include "linear_function.h"
#include "mesh.h"
#include "mpm_scheme.h"
#include "mpm_scheme_usf.h"
#include "mpm_scheme_usl.h"
#include "node.h"
#include "quadrilateral_element.h"

const double absTolerance = 1E-8;

// Function to print a vector of matrices with a label
void printMatrices(const std::string& label, const std::vector<Eigen::Matrix<double, 3, 1>>& matrices) {
    std::cout << "_____________" << label << "_______________\n" << std::endl;

    for (const auto& matrix : matrices) {
        std::cout << matrix.transpose().format(Eigen::IOFormat(Eigen::StreamPrecision, Eigen::DontAlignCols, ", ", ", ", "", "", "[", "]")) << std::endl;
    }
}

//! \brief Check stress update 3D case
TEST_CASE("Stress update is checked for USF and USL",
          "[MPMScheme][USF][USL][3D]") {
  // Dimension
  const unsigned Dim = 3;
  // Degrees of freedom
  const unsigned Dof = 6;
  // Number of phases
  const unsigned Nphases = 1;
  // Number of nodes per cell
  const unsigned Nnodes = 8;
  // relTolerance
  const double relTolerance = 1.E-9;

  // Assign material
  unsigned mid = 0;
  std::vector<unsigned> mids(1, mid);
  // Initialise material
  Json jmaterial;
  jmaterial["density"] = 1000.;
  jmaterial["youngs_modulus"] = 1.0E+7;
  jmaterial["poisson_ratio"] = 0.3;

  auto material =
      Factory<mpm::Material<Dim>, unsigned, const Json&>::instance()->create(
          "LinearElastic3D", std::move(0), jmaterial);

  std::map<unsigned, std::shared_ptr<mpm::Material<Dim>>> materials;
  materials[mid] = material;

  // 8-noded hexahedron element
  std::shared_ptr<mpm::Element<Dim>> element =
      Factory<mpm::Element<Dim>>::instance()->create("ED3H8");

  // Particle 1
  mpm::Index id1 = 0;
  Eigen::Vector3d coords;
  coords << .5, .5, .5;
  std::shared_ptr<mpm::ParticleBase<Dim>> particle1 =
      std::make_shared<mpm::Particle<Dim>>(id1, coords);

  // Particle 2
  mpm::Index id2 = 1;
  coords << 1.5, 1.5, 1.5;
  std::shared_ptr<mpm::ParticleBase<Dim>> particle2 =
      std::make_shared<mpm::Particle<Dim>>(id2, coords);

  auto mesh = std::make_shared<mpm::Mesh<Dim>>(0);
  // Check mesh is active
  REQUIRE(mesh->status() == false);

  // Check nodal coordinates size
  REQUIRE(mesh->nodal_coordinates(true).size() == 0);
  // Check node pairs size
  REQUIRE(mesh->node_pairs(false, true).size() == 0);

  // Define nodes
  coords << 0, 0, 0;
  std::shared_ptr<mpm::NodeBase<Dim>> node0 =
      std::make_shared<mpm::Node<Dim, Dof, Nphases>>(0, coords);
  REQUIRE(mesh->add_node(node0) == true);
  for (int i=0; i<3; i++) 
    REQUIRE_NOTHROW(node0->assign_velocity_constraint(i, 0, true));

  coords << 2, 0, 0;
  std::shared_ptr<mpm::NodeBase<Dim>> node1 =
      std::make_shared<mpm::Node<Dim, Dof, Nphases>>(1, coords);
  REQUIRE(mesh->add_node(node1) == true);
  for (int i=0; i<3; i++) 
    REQUIRE_NOTHROW(node1->assign_velocity_constraint(i, 0, true));

  coords << 2, 2, 0;
  std::shared_ptr<mpm::NodeBase<Dim>> node2 =
      std::make_shared<mpm::Node<Dim, Dof, Nphases>>(2, coords);
  REQUIRE(mesh->add_node(node2) == true);
  for (int i=0; i<3; i++) 
    REQUIRE_NOTHROW(node2->assign_velocity_constraint(i, 0, true));

  coords << 0, 2, 0;
  std::shared_ptr<mpm::NodeBase<Dim>> node3 =
      std::make_shared<mpm::Node<Dim, Dof, Nphases>>(3, coords);
  REQUIRE(mesh->add_node(node3) == true);
  for (int i=0; i<3; i++) 
    REQUIRE_NOTHROW(node3->assign_velocity_constraint(i, 0, true));

  coords << 0, 0, 2;
  std::shared_ptr<mpm::NodeBase<Dim>> node4 =
      std::make_shared<mpm::Node<Dim, Dof, Nphases>>(4, coords);
  REQUIRE(mesh->add_node(node4) == true);
  for (int i=0; i<3; i++) 
    REQUIRE_NOTHROW(node4->assign_velocity_constraint(i, 0, true));

  coords << 2, 0, 2;
  std::shared_ptr<mpm::NodeBase<Dim>> node5 =
      std::make_shared<mpm::Node<Dim, Dof, Nphases>>(5, coords);
  REQUIRE(mesh->add_node(node5) == true);
  for (int i=0; i<3; i++) 
    REQUIRE_NOTHROW(node5->assign_velocity_constraint(i, 0, true));

  coords << 2, 2, 2;
  std::shared_ptr<mpm::NodeBase<Dim>> node6 =
      std::make_shared<mpm::Node<Dim, Dof, Nphases>>(6, coords);
  REQUIRE(mesh->add_node(node6) == true);
  // No boundary conditions for this node, so the velocity is not 0 and the particles should not go out of the mesh

  coords << 0, 2, 2;
  std::shared_ptr<mpm::NodeBase<Dim>> node7 =
      std::make_shared<mpm::Node<Dim, Dof, Nphases>>(7, coords);
  REQUIRE(mesh->add_node(node7) == true);
  for (int i=0; i<3; i++) 
    REQUIRE_NOTHROW(node7->assign_velocity_constraint(i, 0, true));


  // Create cell1
  auto cell1 = std::make_shared<mpm::Cell<Dim>>(id1, Nnodes, element);

  // Add nodes to cell
  cell1->add_node(0, node0);
  cell1->add_node(1, node1);
  cell1->add_node(2, node2);
  cell1->add_node(3, node3);
  cell1->add_node(4, node4);
  cell1->add_node(5, node5);
  cell1->add_node(6, node6);
  cell1->add_node(7, node7);

  REQUIRE(cell1->nnodes() == 8);

  REQUIRE(mesh->add_cell(cell1) == true);

  REQUIRE(cell1->initialise() == true);

  // Check nodal coordinates size
  REQUIRE(mesh->nodal_coordinates(true).size() == 8);
  // Check node pairs size
  REQUIRE(mesh->node_pairs().size() == 12);

  // Add particle 1 and check
  REQUIRE(mesh->add_particle(particle1) == true);
  // Add particle 2 and check
  REQUIRE(mesh->add_particle(particle2) == true);
  // Add particle 2 again and check
  REQUIRE_THROWS(mesh->add_particle(particle2, true, true));

  REQUIRE(particle1->assign_material(material) == true);
  REQUIRE(particle2->assign_material(material) == true);

  // Check mesh is active
  REQUIRE(mesh->status() == true);
  // Check number of particles in mesh
  REQUIRE(mesh->nparticles() == 2);

  REQUIRE_NOTHROW(mesh->locate_particles_mesh(true));

  REQUIRE_NOTHROW(particle1->assign_cell(cell1, true));
  REQUIRE_NOTHROW(particle2->assign_cell(cell1, true));

  // Assign volume
  REQUIRE_NOTHROW(particle1->assign_volume(4.0, true));
  REQUIRE_NOTHROW(particle2->assign_volume(3.0, true));

  // Compute particle masses
  REQUIRE_NOTHROW(particle1->compute_mass());
  REQUIRE_NOTHROW(particle2->compute_mass());

  SECTION("Check USF") {
    auto mpm_scheme = std::make_shared<mpm::MPMSchemeUSF<Dim>>(mesh, 0.01);
    // Phase
    unsigned phase = 0;
    // Step
    unsigned step = 5;
    // Gravity
    Eigen::Matrix<double, Dim, 1> gravity = {0., 0., 9.81};
    // Initialise
    REQUIRE_NOTHROW(mpm_scheme->initialise());

    // Mass momentum and compute velocity at nodes
    REQUIRE_NOTHROW(mpm_scheme->compute_nodal_kinematics("pic", phase, 0));

    // Update stress first
    REQUIRE_NOTHROW(mpm_scheme->precompute_stress_strain(phase, false));
    REQUIRE_NOTHROW(mpm_scheme->precompute_stress_strain(phase, true));

    // Compute forces
    REQUIRE_NOTHROW(mpm_scheme->compute_forces(gravity, phase, step, false, true));
    REQUIRE_NOTHROW(mpm_scheme->compute_forces(gravity, phase, step, true, true));

    // Particle kinematics
    REQUIRE_NOTHROW(
        mpm_scheme->compute_particle_kinematics("flip", 1., phase, "Cundall", 0.02, step, false));
    REQUIRE_NOTHROW(
        mpm_scheme->compute_particle_kinematics("pic", 0., phase, "Cundall", 0.02, step, false));
    REQUIRE_NOTHROW(
        mpm_scheme->compute_particle_kinematics("flip0.9", .9, phase, "Cundall", 0.02, step, false));
    REQUIRE_NOTHROW(
        mpm_scheme->compute_particle_kinematics("apic", 0., phase, "Cundall", 0.02, step, false));
    REQUIRE_NOTHROW(
        mpm_scheme->compute_particle_kinematics("aflip", 1., phase, "Cundall", 0.02, step, false));
    REQUIRE_NOTHROW(
        mpm_scheme->compute_particle_kinematics("nflip", 1., phase, "Cundall", 0.02, step, false));
    REQUIRE_NOTHROW(
        mpm_scheme->compute_particle_kinematics("anflip", 1., phase, "Cundall", 0.02, step, false));
    REQUIRE_NOTHROW(
        mpm_scheme->compute_particle_kinematics("anflip0.8", .8, phase, "Cundall", 0.02, step, false));
    REQUIRE_NOTHROW(
        mpm_scheme->compute_particle_kinematics("flip", 1., phase, "None", 0.02, step, false));
    REQUIRE_NOTHROW(
        mpm_scheme->compute_particle_kinematics("pic", 0., phase, "None", 0.02, step, false));
    REQUIRE_NOTHROW(
        mpm_scheme->compute_particle_kinematics("flip0.9", .9, phase, "None", 0.02, step, false));
    REQUIRE_NOTHROW(
        mpm_scheme->compute_particle_kinematics("apic", 0., phase, "None", 0.02, step, false));
    REQUIRE_NOTHROW(
        mpm_scheme->compute_particle_kinematics("aflip", 1., phase, "None", 0.02, step, false));
    REQUIRE_NOTHROW(
        mpm_scheme->compute_particle_kinematics("nflip", 1., phase, "None", 0.02, step, false));
    REQUIRE_NOTHROW(
        mpm_scheme->compute_particle_kinematics("anflip", 1., phase, "None", 0.02, step, false));
    REQUIRE_NOTHROW(
        mpm_scheme->compute_particle_kinematics("anflip0.8", .8, phase, "None", 0.02, step, false));

    // Update Stress Last
    REQUIRE_NOTHROW(mpm_scheme->postcompute_stress_strain(phase, true));
    REQUIRE_NOTHROW(mpm_scheme->postcompute_stress_strain(phase, false));

    // Locate particles
    REQUIRE_NOTHROW(mpm_scheme->locate_particles(true, true));
    REQUIRE_NOTHROW(mpm_scheme->locate_particles(false, true));
  }

  SECTION("Check USL") {
    auto mpm_scheme = std::make_shared<mpm::MPMSchemeUSL<Dim>>(mesh, 0.01);
    // Phase
    unsigned phase = 0;
    // Step
    unsigned step = 5;
    // Gravity
    Eigen::Matrix<double, Dim, 1> gravity = {0., 0., 9.81};
    // Initialise
    REQUIRE_NOTHROW(mpm_scheme->initialise());

    // Mass momentum and compute velocity at nodes
    REQUIRE_NOTHROW(mpm_scheme->compute_nodal_kinematics("pic", phase, 0));

    // Update stress first
    REQUIRE_NOTHROW(mpm_scheme->precompute_stress_strain(phase, false));
    REQUIRE_NOTHROW(mpm_scheme->precompute_stress_strain(phase, true));

    // Compute forces
    REQUIRE_NOTHROW(mpm_scheme->compute_forces(gravity, phase, step, false, true));
    REQUIRE_NOTHROW(mpm_scheme->compute_forces(gravity, phase, step, true, true));

    // Particle kinematics
    REQUIRE_NOTHROW(
        mpm_scheme->compute_particle_kinematics("flip", 1., phase, "Cundall", 0.02, step, false));
    REQUIRE_NOTHROW(
        mpm_scheme->compute_particle_kinematics("pic", 0., phase, "Cundall", 0.02, step, false));
    REQUIRE_NOTHROW(
        mpm_scheme->compute_particle_kinematics("flip0.9", .9, phase, "Cundall", 0.02, step, false));
    REQUIRE_NOTHROW(
        mpm_scheme->compute_particle_kinematics("apic", 0., phase, "Cundall", 0.02, step, false));
    REQUIRE_NOTHROW(
        mpm_scheme->compute_particle_kinematics("aflip", 1., phase, "Cundall", 0.02, step, false));
    REQUIRE_NOTHROW(
        mpm_scheme->compute_particle_kinematics("nflip", 1., phase, "Cundall", 0.02, step, false));
    REQUIRE_NOTHROW(
        mpm_scheme->compute_particle_kinematics("anflip", 1., phase, "Cundall", 0.02, step, false));
    REQUIRE_NOTHROW(
        mpm_scheme->compute_particle_kinematics("anflip0.8", .8, phase, "Cundall", 0.02, step, false));
    REQUIRE_NOTHROW(
        mpm_scheme->compute_particle_kinematics("flip", 1., phase, "None", 0.02, step, false));
    REQUIRE_NOTHROW(
        mpm_scheme->compute_particle_kinematics("pic", 0., phase, "None", 0.02, step, false));
    REQUIRE_NOTHROW(
        mpm_scheme->compute_particle_kinematics("flip0.9", .9, phase, "None", 0.02, step, false));
    REQUIRE_NOTHROW(
        mpm_scheme->compute_particle_kinematics("apic", 0., phase, "None", 0.02, step, false));
    REQUIRE_NOTHROW(
        mpm_scheme->compute_particle_kinematics("aflip", 1., phase, "None", 0.02, step, false));
    REQUIRE_NOTHROW(
        mpm_scheme->compute_particle_kinematics("nflip", 1., phase, "None", 0.02, step, false));
    REQUIRE_NOTHROW(
        mpm_scheme->compute_particle_kinematics("anflip", 1., phase, "None", 0.02, step, false));
    REQUIRE_NOTHROW(
        mpm_scheme->compute_particle_kinematics("anflip0.8", .8, phase, "None", 0.02, step, false));

    // Update Stress Last
    REQUIRE_NOTHROW(mpm_scheme->postcompute_stress_strain(phase, true));
    REQUIRE_NOTHROW(mpm_scheme->postcompute_stress_strain(phase, false));

    // Locate particles
    REQUIRE_NOTHROW(mpm_scheme->locate_particles(true, true));
    REQUIRE_NOTHROW(mpm_scheme->locate_particles(false, true));
  }

  auto mpm_scheme = std::make_shared<mpm::MPMSchemeUSF<Dim>>(mesh, 0.01);
  // Phase
  unsigned phase = 0;
  // Step
  unsigned step = 5;
  // Gravity
  Eigen::Matrix<double, Dim, 1> gravity = {-2., -1., -3}; // All directions are solicited

  // Initialise
  REQUIRE_NOTHROW(mpm_scheme->initialise());

  SECTION("Check FLIP motion integration strategy") {

    for (int i=0; i<500; i++) {
        // Initialise nodes, cells and shape functions
        mpm_scheme->initialise();

        // Mass momentum and compute velocity at nodes
        REQUIRE_NOTHROW(mpm_scheme->compute_nodal_kinematics("flip", phase, step+i));

        // Update stress first
        REQUIRE_NOTHROW(mpm_scheme->precompute_stress_strain(phase, false));

        // Compute forces
        REQUIRE_NOTHROW(mpm_scheme->compute_forces(gravity, phase, step+i, true, true));

        // Particle kinematics
        REQUIRE_NOTHROW(
            mpm_scheme->compute_particle_kinematics("flip", 1., phase, "None", 0.02, step+i, false));

        // Locate particles
        REQUIRE_NOTHROW(mpm_scheme->locate_particles(true, true));
    }

    // printMatrices("ppos", mpm_scheme->get_ppos());
    // printMatrices("pvel", mpm_scheme->get_pvel());

    // Check positions
    std::vector<Eigen::Matrix<double, 3, 1>> expected_poss = {
        Eigen::Matrix<double, 3, 1>(0.500002, 0.500011, 0.499992),
        Eigen::Matrix<double, 3, 1>(1.50004, 1.50029, 1.49979)
    };
    std::vector<Eigen::Matrix<double, 3, 1>> actual_poss = mpm_scheme->get_ppos();

    for (size_t i = 0; i < actual_poss.size(); ++i) {
        CAPTURE(actual_poss[i].transpose());
        CAPTURE(expected_poss[i].transpose());
        for (int dim = 0; dim < actual_poss[i].size(); ++dim) 
            REQUIRE(actual_poss[i](dim) == Approx(expected_poss[i](dim)));
    }

    // Check velocities
    std::vector<Eigen::Matrix<double, 3, 1>> expected_vels = {
        Eigen::Matrix<double, 3, 1>(0.000593457, 0.00110569, 8.12287e-05),
        Eigen::Matrix<double, 3, 1>(0.015458, 0.0292817, 0.00163423)
    };
    std::vector<Eigen::Matrix<double, 3, 1>> actual_vels = mpm_scheme->get_pvel();

    for (size_t i = 0; i < actual_vels.size(); ++i) {
        CAPTURE(actual_vels[i].transpose());
        CAPTURE(expected_vels[i].transpose());
        for (int dim = 0; dim < actual_vels[i].size(); ++dim) 
            REQUIRE(actual_vels[i](dim) == Approx(expected_vels[i](dim)));
    }
  }

   SECTION("Check PIC motion integration strategy") {
    
    for (int i=0; i<500; i++) {
        // Initialise nodes, cells and shape functions
        mpm_scheme->initialise();

        // Mass momentum and compute velocity at nodes
        REQUIRE_NOTHROW(mpm_scheme->compute_nodal_kinematics("pic", phase, step+i));

        // Update stress first
        REQUIRE_NOTHROW(mpm_scheme->precompute_stress_strain(phase, false));

        // Compute forces
        REQUIRE_NOTHROW(mpm_scheme->compute_forces(gravity, phase, step+i, true, true));

        // Particle kinematics
        REQUIRE_NOTHROW(
            mpm_scheme->compute_particle_kinematics("pic", 0., phase, "None", 0.02, step+i, false));

        // Locate particles
        REQUIRE_NOTHROW(mpm_scheme->locate_particles(true, true));
    }

    // printMatrices("ppos", mpm_scheme->get_ppos());
    // printMatrices("pvel", mpm_scheme->get_pvel());

    // Check positions
    std::vector<Eigen::Matrix<double, 3, 1>> expected_poss = {
        Eigen::Matrix<double, 3, 1>(0.499989, 0.500008, 0.499971),
        Eigen::Matrix<double, 3, 1>(1.49971, 1.50021, 1.49921)
    };
    std::vector<Eigen::Matrix<double, 3, 1>> actual_poss = mpm_scheme->get_ppos();

    for (size_t i = 0; i < actual_poss.size(); ++i) {
        CAPTURE(actual_poss[i].transpose());
        CAPTURE(expected_poss[i].transpose());
        // REQUIRE(actual_poss[i].isApprox(expected_poss[i], 1e-5));
        for (int dim = 0; dim < actual_poss[i].size(); ++dim) 
            REQUIRE(actual_poss[i](dim) == Approx(expected_poss[i](dim)));
    }

    // Check velocities
    std::vector<Eigen::Matrix<double, 3, 1>> expected_vels = {
        Eigen::Matrix<double, 3, 1>(1.62183e-17, 1.13818e-16, -1.13594e-16),
        Eigen::Matrix<double, 3, 1>(4.3767e-16, 3.07153e-15, -3.06549e-15)
    };
    std::vector<Eigen::Matrix<double, 3, 1>> actual_vels = mpm_scheme->get_pvel();

    for (size_t i = 0; i < actual_vels.size(); ++i) {
        CAPTURE(actual_vels[i].transpose());
        CAPTURE(expected_vels[i].transpose());
        for (int dim = 0; dim < actual_vels[i].size(); ++dim) 
            REQUIRE(actual_vels[i](dim) == Approx(expected_vels[i](dim)));
    }
  }

  SECTION("Check FLIP0.9 motion integration strategy") {
    
    for (int i=0; i<500; i++) {
        // Initialise nodes, cells and shape functions
        mpm_scheme->initialise();

        // Mass momentum and compute velocity at nodes
        REQUIRE_NOTHROW(mpm_scheme->compute_nodal_kinematics("flip0.9", phase, step+i));

        // Update stress first
        REQUIRE_NOTHROW(mpm_scheme->precompute_stress_strain(phase, false));

        // Compute forces
        REQUIRE_NOTHROW(mpm_scheme->compute_forces(gravity, phase, step+i, true, true));

        // Particle kinematics
        REQUIRE_NOTHROW(
            mpm_scheme->compute_particle_kinematics("flip0.9", .9, phase, "None", 0.02, step+i, false));

        // Locate particles
        REQUIRE_NOTHROW(mpm_scheme->locate_particles(true, true));
    }

    // printMatrices("ppos", mpm_scheme->get_ppos());
    // printMatrices("pvel", mpm_scheme->get_pvel());

    // Check positions
    std::vector<Eigen::Matrix<double, 3, 1>> expected_poss = {
        Eigen::Matrix<double, 3, 1>(0.499995, 0.500004, 0.499987),
        Eigen::Matrix<double, 3, 1>(1.49987, 1.5001, 1.49964)
    };
    std::vector<Eigen::Matrix<double, 3, 1>> actual_poss = mpm_scheme->get_ppos();

    for (size_t i = 0; i < actual_poss.size(); ++i) {
        CAPTURE(actual_poss[i].transpose());
        CAPTURE(expected_poss[i].transpose());
        // REQUIRE(actual_poss[i].isApprox(expected_poss[i], 1e-5));
        for (int dim = 0; dim < actual_poss[i].size(); ++dim) 
            REQUIRE(actual_poss[i](dim) == Approx(expected_poss[i](dim)));
    }

    // Check velocities
    std::vector<Eigen::Matrix<double, 3, 1>> expected_vels = {
        Eigen::Matrix<double, 3, 1>(-6.13318e-11, -1.28129e-11, -1.0985e-10),
        Eigen::Matrix<double, 3, 1>(-1.65557e-09, -3.45867e-10, -2.96525e-09)
    };
    std::vector<Eigen::Matrix<double, 3, 1>> actual_vels = mpm_scheme->get_pvel();

    for (size_t i = 0; i < actual_vels.size(); ++i) {
        CAPTURE(actual_vels[i].transpose());
        CAPTURE(expected_vels[i].transpose());
        for (int dim = 0; dim < actual_vels[i].size(); ++dim) 
            REQUIRE(actual_vels[i](dim) == Approx(expected_vels[i](dim)));
    }
  }

  SECTION("Check APIC motion integration strategy") {
    
    for (int i=0; i<200; i++) {
        // Initialise nodes, cells and shape functions
        mpm_scheme->initialise();

        // Mass momentum and compute velocity at nodes
        REQUIRE_NOTHROW(mpm_scheme->compute_nodal_kinematics("apic", phase, step+i));

        // Update stress first
        REQUIRE_NOTHROW(mpm_scheme->precompute_stress_strain(phase, false));

        // Compute forces
        REQUIRE_NOTHROW(mpm_scheme->compute_forces(gravity, phase, step+i, true, true));

        // Particle kinematics
        REQUIRE_NOTHROW(
            mpm_scheme->compute_particle_kinematics("apic", 0., phase, "None", 0.02, step+i, false));

        // Locate particles
        REQUIRE_NOTHROW(mpm_scheme->locate_particles(true, true));
    }

    // printMatrices("ppos", mpm_scheme->get_ppos());
    // printMatrices("pvel", mpm_scheme->get_pvel());

    // Check positions
    std::vector<Eigen::Matrix<double, 3, 1>> expected_poss = {
        Eigen::Matrix<double, 3, 1>(0.499995, 0.500004, 0.499986),
        Eigen::Matrix<double, 3, 1>(1.49986, 1.50011, 1.49961)
    };
    std::vector<Eigen::Matrix<double, 3, 1>> actual_poss = mpm_scheme->get_ppos();

    for (size_t i = 0; i < actual_poss.size(); ++i) {
        CAPTURE(actual_poss[i].transpose());
        CAPTURE(expected_poss[i].transpose());
        // REQUIRE(actual_poss[i].isApprox(expected_poss[i], 1e-5));
        for (int dim = 0; dim < actual_poss[i].size(); ++dim) 
            REQUIRE(actual_poss[i](dim) == Approx(expected_poss[i](dim)));
    }

    // Check velocities
    std::vector<Eigen::Matrix<double, 3, 1>> expected_vels = {
        Eigen::Matrix<double, 3, 1>(-2.19704e-13, -2.71005e-13, -1.68401e-13),
        Eigen::Matrix<double, 3, 1>(-5.93051e-12, -7.31528e-12, -4.54569e-12)
    };
    std::vector<Eigen::Matrix<double, 3, 1>> actual_vels = mpm_scheme->get_pvel();

    for (size_t i = 0; i < actual_vels.size(); ++i) {
        CAPTURE(actual_vels[i].transpose());
        CAPTURE(expected_vels[i].transpose());
        for (int dim = 0; dim < actual_vels[i].size(); ++dim) 
            REQUIRE(actual_vels[i](dim) == Approx(expected_vels[i](dim)));
    }
  }

  SECTION("Check AFLIP motion integration strategy") {
    
    for (int i=0; i<500; i++) {
        // Initialise nodes, cells and shape functions
        mpm_scheme->initialise();

        // Mass momentum and compute velocity at nodes
        REQUIRE_NOTHROW(mpm_scheme->compute_nodal_kinematics("aflip", phase, step+i));

        // Update stress first
        REQUIRE_NOTHROW(mpm_scheme->precompute_stress_strain(phase, false));

        // Compute forces
        REQUIRE_NOTHROW(mpm_scheme->compute_forces(gravity, phase, step+i, true, true));

        // Particle kinematics
        REQUIRE_NOTHROW(
            mpm_scheme->compute_particle_kinematics("aflip", 1., phase, "None", 0.02, step+i, false));

        // Locate particles
        REQUIRE_NOTHROW(mpm_scheme->locate_particles(true, true));
    }

    // printMatrices("ppos", mpm_scheme->get_ppos());
    // printMatrices("pvel", mpm_scheme->get_pvel());

    // Check positions
    std::vector<Eigen::Matrix<double, 3, 1>> expected_poss = {
        Eigen::Matrix<double, 3, 1>(0.499996, 0.500003, 0.499988),
        Eigen::Matrix<double, 3, 1>(1.49989, 1.50009, 1.49968)
    };
    std::vector<Eigen::Matrix<double, 3, 1>> actual_poss = mpm_scheme->get_ppos();

    for (size_t i = 0; i < actual_poss.size(); ++i) {
        CAPTURE(actual_poss[i].transpose());
        CAPTURE(expected_poss[i].transpose());
        // REQUIRE(actual_poss[i].isApprox(expected_poss[i], 1e-5));
        for (int dim = 0; dim < actual_poss[i].size(); ++dim) 
            REQUIRE(actual_poss[i](dim) == Approx(expected_poss[i](dim)));
    }

    // Check velocities
    std::vector<Eigen::Matrix<double, 3, 1>> expected_vels = {
        Eigen::Matrix<double, 3, 1>(4.73999e-07, 4.77884e-07, 4.70114e-07),
        Eigen::Matrix<double, 3, 1>(-2.34197e-08, -1.06481e-07, 5.96387e-08)
    };
    std::vector<Eigen::Matrix<double, 3, 1>> actual_vels = mpm_scheme->get_pvel();

    for (size_t i = 0; i < actual_vels.size(); ++i) {
        CAPTURE(actual_vels[i].transpose());
        CAPTURE(expected_vels[i].transpose());
        for (int dim = 0; dim < actual_vels[i].size(); ++dim) 
            REQUIRE(actual_vels[i](dim) == Approx(expected_vels[i](dim)));
    }
  }

  SECTION("Check NFLIP motion integration strategy") {
    
    for (int i=0; i<500; i++) {
        // Initialise nodes, cells and shape functions
        mpm_scheme->initialise();

        // Mass momentum and compute velocity at nodes
        REQUIRE_NOTHROW(mpm_scheme->compute_nodal_kinematics("nflip", phase, step+i));

        // Update stress first
        REQUIRE_NOTHROW(mpm_scheme->precompute_stress_strain(phase, false));

        // Compute forces
        REQUIRE_NOTHROW(mpm_scheme->compute_forces(gravity, phase, step+i, true, true));

        // Particle kinematics
        REQUIRE_NOTHROW(
            mpm_scheme->compute_particle_kinematics("nflip", 1., phase, "None", 0.02, step+i, false));

        // Locate particles
        REQUIRE_NOTHROW(mpm_scheme->locate_particles(true, true));
    }

    // printMatrices("ppos", mpm_scheme->get_ppos());
    // printMatrices("pvel", mpm_scheme->get_pvel());

    // Check positions
    std::vector<Eigen::Matrix<double, 3, 1>> expected_poss = {
        Eigen::Matrix<double, 3, 1>(0.500102, 0.500116, 0.500087),
        Eigen::Matrix<double, 3, 1>(1.49984, 1.50024, 1.49945)
    };
    std::vector<Eigen::Matrix<double, 3, 1>> actual_poss = mpm_scheme->get_ppos();

    for (size_t i = 0; i < actual_poss.size(); ++i) {
        CAPTURE(actual_poss[i].transpose());
        CAPTURE(expected_poss[i].transpose());
        // REQUIRE(actual_poss[i].isApprox(expected_poss[i], 1e-5));
        for (int dim = 0; dim < actual_poss[i].size(); ++dim) 
            REQUIRE(actual_poss[i](dim) == Approx(expected_poss[i](dim)));
    }

    // Check velocities
    std::vector<Eigen::Matrix<double, 3, 1>> expected_vels = {
        Eigen::Matrix<double, 3, 1>(0.000629321, 0.00113792, 0.000120715),
        Eigen::Matrix<double, 3, 1>(0.0158142, 0.0295283, 0.0021001)
    };
    std::vector<Eigen::Matrix<double, 3, 1>> actual_vels = mpm_scheme->get_pvel();

    for (size_t i = 0; i < actual_vels.size(); ++i) {
        CAPTURE(actual_vels[i].transpose());
        CAPTURE(expected_vels[i].transpose());
        for (int dim = 0; dim < actual_vels[i].size(); ++dim) 
            REQUIRE(actual_vels[i](dim) == Approx(expected_vels[i](dim)));
    }
  }

  SECTION("Check ANFLIP motion integration strategy") {
    
    for (int i=0; i<500; i++) {
        // Initialise nodes, cells and shape functions
        mpm_scheme->initialise();

        // Mass momentum and compute velocity at nodes
        REQUIRE_NOTHROW(mpm_scheme->compute_nodal_kinematics("anflip", phase, step+i));

        // Update stress first
        REQUIRE_NOTHROW(mpm_scheme->precompute_stress_strain(phase, false));

        // Compute forces
        REQUIRE_NOTHROW(mpm_scheme->compute_forces(gravity, phase, step+i, true, true));

        // Particle kinematics
        REQUIRE_NOTHROW(
            mpm_scheme->compute_particle_kinematics("anflip", 1., phase, "None", 0.02, step+i, false));

        // Locate particles
        REQUIRE_NOTHROW(mpm_scheme->locate_particles(true, true));
    }

    // printMatrices("ppos", mpm_scheme->get_ppos());
    // printMatrices("pvel", mpm_scheme->get_pvel());

    // Check positions
    std::vector<Eigen::Matrix<double, 3, 1>> expected_poss = {
        Eigen::Matrix<double, 3, 1>(0.499996, 0.500007, 0.499986),
        Eigen::Matrix<double, 3, 1>(1.49983, 1.50013, 1.49954)
    };
    std::vector<Eigen::Matrix<double, 3, 1>> actual_poss = mpm_scheme->get_ppos();

    for (size_t i = 0; i < actual_poss.size(); ++i) {
        CAPTURE(actual_poss[i].transpose());
        CAPTURE(expected_poss[i].transpose());
        // REQUIRE(actual_poss[i].isApprox(expected_poss[i], 1e-5));
        for (int dim = 0; dim < actual_poss[i].size(); ++dim) 
            REQUIRE(actual_poss[i](dim) == Approx(expected_poss[i](dim)));
    }

    // Check velocities
    std::vector<Eigen::Matrix<double, 3, 1>> expected_vels = {
        Eigen::Matrix<double, 3, 1>(5.33777e-07, 5.0355e-07, 5.64004e-07),
        Eigen::Matrix<double, 3, 1>(-2.64005e-08, -1.10989e-07, 5.81835e-08)
    };
    std::vector<Eigen::Matrix<double, 3, 1>> actual_vels = mpm_scheme->get_pvel();

    for (size_t i = 0; i < actual_vels.size(); ++i) {
        CAPTURE(actual_vels[i].transpose());
        CAPTURE(expected_vels[i].transpose());
        for (int dim = 0; dim < actual_vels[i].size(); ++dim) 
            REQUIRE(actual_vels[i](dim) == Approx(expected_vels[i](dim)));
    }
  }

  SECTION("Check ANFLIP0.8 motion integration strategy") {
    
    for (int i=0; i<500; i++) {
        // Initialise nodes, cells and shape functions
        mpm_scheme->initialise();

        // Mass momentum and compute velocity at nodes
        REQUIRE_NOTHROW(mpm_scheme->compute_nodal_kinematics("anflip0.8", phase, step+i));

        // Update stress first
        REQUIRE_NOTHROW(mpm_scheme->precompute_stress_strain(phase, false));

        // Compute forces
        REQUIRE_NOTHROW(mpm_scheme->compute_forces(gravity, phase, step+i, true, true));

        // Particle kinematics
        REQUIRE_NOTHROW(
            mpm_scheme->compute_particle_kinematics("anflip0.8", .8, phase, "None", 0.02, step+i, false));

        // Locate particles
        REQUIRE_NOTHROW(mpm_scheme->locate_particles(true, true));
    }

    // printMatrices("ppos", mpm_scheme->get_ppos());
    // printMatrices("pvel", mpm_scheme->get_pvel());

    // Check positions
    std::vector<Eigen::Matrix<double, 3, 1>> expected_poss = {
        Eigen::Matrix<double, 3, 1>(0.499994, 0.500004, 0.499984),
        Eigen::Matrix<double, 3, 1>(1.49984, 1.50012, 1.49956)
    };
    std::vector<Eigen::Matrix<double, 3, 1>> actual_poss = mpm_scheme->get_ppos();

    for (size_t i = 0; i < actual_poss.size(); ++i) {
        CAPTURE(actual_poss[i].transpose());
        CAPTURE(expected_poss[i].transpose());
        // REQUIRE(actual_poss[i].isApprox(expected_poss[i], 1e-5));
        for (int dim = 0; dim < actual_poss[i].size(); ++dim) 
            REQUIRE(actual_poss[i](dim) == Approx(expected_poss[i](dim)));
    }

    // Check velocities
    std::vector<Eigen::Matrix<double, 3, 1>> expected_vels = {
        Eigen::Matrix<double, 3, 1>(1.79216e-16, 6.14832e-13, -6.14551e-13),
        Eigen::Matrix<double, 3, 1>(4.83746e-15, 1.65957e-11, -1.65882e-11)
    };
    std::vector<Eigen::Matrix<double, 3, 1>> actual_vels = mpm_scheme->get_pvel();

    for (size_t i = 0; i < actual_vels.size(); ++i) {
        CAPTURE(actual_vels[i].transpose());
        CAPTURE(expected_vels[i].transpose());
        for (int dim = 0; dim < actual_vels[i].size(); ++dim) 
            REQUIRE(actual_vels[i](dim) == Approx(expected_vels[i](dim)));
    }
  }
}
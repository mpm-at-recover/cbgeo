#include <catch2/catch.hpp>

//! Alias for JSON
#include "json.hpp"
using Json = nlohmann::json;

#include "mpm_explicit.h"
#include "write_mesh_particles_unitcell.h"

const double absTolerance = 1E-8;

// Check MPM Explicit USF
TEST_CASE("MPM 2D Explicit USF implementation is checked in unitcells",
          "[MPM][2D][USF][Explicit][1Phase][unitcell]") {
  // Dimension
  const unsigned Dim = 2;

  // Write JSON file
  const std::string dname = "tests/mpm_explicit_usf_unitcell/";
  const std::string fname = "mpm-explicit-usf";
  const std::string analysis = "MPMExplicit2D";
  const std::string mpm_scheme = "usf";
  REQUIRE(mpm_test::write_json_unitcell(2, analysis, mpm_scheme, fname, dname) ==
          true);

  // Write Mesh
  REQUIRE(mpm_test::write_mesh_2d_unitcell(dname) == true);

  // Write Particles
  REQUIRE(mpm_test::write_particles_2d_unitcell(dname) == true);

  // Assign argc and argv to input arguments of MPM
  int argc = 5;
  std::string input_file = dname + "mpm-explicit-usf-2d-unitcell.json";
  std::string logfile = dname + "mpm-explicit-usf-2d-unitcell.log";
  // clang-format off
  char* argv[] = {(char*)"./mpm",
                  (char*)"-f",  (char*)"./",
                  // (char*)"-O", &logfile[0],
                  (char*)"-i",  &input_file[0]};
  // clang-format on

  SECTION("Check initialisation") {
    // Create an IO object
    std::unique_ptr<mpm::IO> io;
    REQUIRE_NOTHROW([&]() {
      io = std::make_unique<mpm::IO>(argc, argv);
    }());

    // Run explicit MPM
    std::unique_ptr<mpm::MPMExplicit<Dim>> mpm;
    REQUIRE_NOTHROW([&]() {
      mpm = std::make_unique<mpm::MPMExplicit<Dim>>(std::move(io));
    }());

    // Initialise materials
    REQUIRE_NOTHROW(mpm->initialise_materials());

    // Initialise mesh and particles
    REQUIRE_NOTHROW(mpm->initialise_mesh(true));
    REQUIRE_NOTHROW(mpm->initialise_particles(true));

    // Initialise external loading
    REQUIRE_NOTHROW(mpm->initialise_loads(true));

    // Renitialise materials
    REQUIRE_THROWS(mpm->initialise_materials());
  }

  SECTION("Check solver") {
    // Create an IO object
    std::unique_ptr<mpm::IO> io;
    REQUIRE_NOTHROW([&]() {
      io = std::make_unique<mpm::IO>(argc, argv);
    }());

    // Run explicit MPM
    std::unique_ptr<mpm::MPMExplicit<Dim>> mpm;
    REQUIRE_NOTHROW([&]() {
      mpm = std::make_unique<mpm::MPMExplicit<Dim>>(std::move(io));
    }());
    // Solve
    REQUIRE(mpm->solve() == true);
  }
}

// Check MPM Explicit
TEST_CASE("MPM 3D Explicit USF implementation is checked in unitcells",
          "[MPM][3D][Explicit][USF][1Phase][unitcell]") {
  // Dimension
  const unsigned Dim = 3;

  // Write JSON file
  const std::string dname = "tests/mpm_explicit_usf_unitcell/";
  const std::string fname = "mpm-explicit-usf";
  const std::string analysis = "MPMExplicit3D";
  const std::string mpm_scheme = "usf";
  REQUIRE(mpm_test::write_json_unitcell(3, analysis, mpm_scheme, fname, dname) ==
          true);

  // Write Mesh
  REQUIRE(mpm_test::write_mesh_3d_unitcell(dname) == true);

  // Write Particles
  REQUIRE(mpm_test::write_particles_3d_unitcell(dname) == true);

  // Assign argc and argv to input arguments of MPM
  int argc = 5;
  std::string input_file = dname + "mpm-explicit-usf-3d-unitcell.json";
  std::string logfile = dname + "mpm-explicit-usf-3d-unitcell.log";
  // clang-format off
  char* argv[] = {(char*)"./mpm",
                  (char*)"-f",  (char*)"./",
                  // (char*)"-O", &logfile[0],
                  (char*)"-i",  &input_file[0]};
  // clang-format on

  SECTION("Check initialisation") {
    // Create an IO object
    std::unique_ptr<mpm::IO> io;
    REQUIRE_NOTHROW([&]() {
      io = std::make_unique<mpm::IO>(argc, argv);
    }());

    // Run explicit MPM
    std::unique_ptr<mpm::MPMExplicit<Dim>> mpm;
    REQUIRE_NOTHROW([&]() {
      mpm = std::make_unique<mpm::MPMExplicit<Dim>>(std::move(io));
    }());

    // Initialise materials
    REQUIRE_NOTHROW(mpm->initialise_materials());

    // Initialise mesh and particles
    REQUIRE_NOTHROW(mpm->initialise_mesh(true));
    REQUIRE_NOTHROW(mpm->initialise_particles(true));

    // Renitialise materials
    REQUIRE_THROWS(mpm->initialise_materials());
  }

  SECTION("Check solver") {
    // Create an IO object
    std::unique_ptr<mpm::IO> io;
    REQUIRE_NOTHROW([&]() {
      io = std::make_unique<mpm::IO>(argc, argv);
    }());

    // Run explicit MPM
    std::unique_ptr<mpm::MPMExplicit<Dim>> mpm;
    REQUIRE_NOTHROW([&]() {
      mpm = std::make_unique<mpm::MPMExplicit<Dim>>(std::move(io));
    }());
    // Solve
    REQUIRE(mpm->solve() == true);
  }
}

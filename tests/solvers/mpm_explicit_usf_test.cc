#include <catch2/catch.hpp>

//! Alias for JSON
#include "json.hpp"
using Json = nlohmann::json;

#include "mpm_explicit.h"
#include "write_mesh_particles.h"

const double absTolerance = 1E-8;

// Check MPM Explicit
TEST_CASE("MPM 2D Explicit implementation is checked",
          "[MPM][2D][Explicit][USF][1Phase]") {
  // Dimension
  const unsigned Dim = 2;

  // Write JSON file
  const std::string dname = "tests/mpm_explicit_usf/";
  const std::string fname = "mpm-explicit-usf";
  const std::string analysis = "MPMExplicit2D";
  const std::string mpm_scheme = "usf";

  // Write JSON Entity Sets file
  REQUIRE(mpm_test::write_entity_set(dname) == true);

  // Write Mesh
  REQUIRE(mpm_test::write_mesh_2d(dname) == true);

  // Write Particles
  REQUIRE(mpm_test::write_particles_2d(dname) == true);

  // Assign argc and argv to input arguments of MPM
  int argc = 5;
  std::string input_file = dname + "mpm-explicit-usf-2d.json";
  std::string logfile = dname + "mpm-explicit-usf-2d.log";
  // clang-format off
  char* argv[] = {(char*)"./mpm",
                  (char*)"-f",  (char*)"./",
                  // (char*)"-O", &logfile[0],
                  (char*)"-i",  &input_file[0]};
  // clang-format on

  SECTION("Check initialisation") {
    bool resume = false;
    REQUIRE(mpm_test::write_json(2, resume, analysis, mpm_scheme, fname, dname) ==
            true);

    // Create an IO object
    std::unique_ptr<mpm::IO> io;
    REQUIRE_NOTHROW([&]() {
      io = std::make_unique<mpm::IO>(argc, argv);
    }());

    // Run explicit MPM
    std::unique_ptr<mpm::MPMExplicit<Dim>> mpm;
    REQUIRE_NOTHROW([&]() {
      mpm = std::make_unique<mpm::MPMExplicit<Dim>>(std::move(io));
    }());

    // Initialise materials
    REQUIRE_NOTHROW(mpm->initialise_materials());
    // Initialise mesh
    REQUIRE_NOTHROW(mpm->initialise_mesh(true));
    // Initialise particles
    REQUIRE_NOTHROW(mpm->initialise_particles(true));

    // Initialise external loading
    REQUIRE_NOTHROW(mpm->initialise_loads(true));

    // Renitialise materials
    REQUIRE_THROWS(mpm->initialise_materials());
  }

  SECTION("Check solver") {
    // Create an IO object
    std::unique_ptr<mpm::IO> io;
    REQUIRE_NOTHROW([&]() {
      io = std::make_unique<mpm::IO>(argc, argv);
    }());

    // Run explicit MPM
    std::unique_ptr<mpm::MPMExplicit<Dim>> mpm;
    REQUIRE_NOTHROW([&]() {
      mpm = std::make_unique<mpm::MPMExplicit<Dim>>(std::move(io));
    }());

    // Solve
    REQUIRE(mpm->solve() == true);
    // Test check point restart
    REQUIRE(mpm->checkpoint_resume() == false);
  }

  SECTION("Check resume") {
    // Write JSON file
    const std::string fname = "mpm-explicit-usf";
    const std::string analysis = "MPMExplicit2D";
    const std::string mpm_scheme = "usf";
    bool resume = true;
    REQUIRE(mpm_test::write_json(2, true, analysis, mpm_scheme, fname, dname) == true);

    // Create an IO object
    std::unique_ptr<mpm::IO> io;
    REQUIRE_NOTHROW([&]() {
      io = std::make_unique<mpm::IO>(argc, argv);
    }());

    // Run explicit MPM
    std::unique_ptr<mpm::MPMExplicit<Dim>> mpm;
    REQUIRE_NOTHROW([&]() {
      mpm = std::make_unique<mpm::MPMExplicit<Dim>>(std::move(io));
    }());

    // Initialise materials
    REQUIRE_NOTHROW(mpm->initialise_materials());
    // Initialise mesh
    REQUIRE_NOTHROW(mpm->initialise_mesh(true));

    // Test check point restart
    REQUIRE(mpm->checkpoint_resume() == true);
    {
      // Solve
      auto io = std::make_unique<mpm::IO>(argc, argv);
      // Run explicit MPM
      auto mpm_resume = std::make_unique<mpm::MPMExplicit<Dim>>(std::move(io));
      REQUIRE(mpm_resume->solve() == true);
    }
  }

  SECTION("Check pressure smoothing") {
    // Create an IO object
    std::unique_ptr<mpm::IO> io;
    REQUIRE_NOTHROW([&]() {
      io = std::make_unique<mpm::IO>(argc, argv);
    }());

    // Run explicit MPM
    std::unique_ptr<mpm::MPMExplicit<Dim>> mpm;
    REQUIRE_NOTHROW([&]() {
      mpm = std::make_unique<mpm::MPMExplicit<Dim>>(std::move(io));
    }());

    // Pressure smoothing
    REQUIRE_NOTHROW(mpm->pressure_smoothing(0));
  }
}

// Check MPM Explicit
TEST_CASE("MPM 3D Explicit implementation is checked",
          "[MPM][3D][Explicit][USF][1Phase]") {
  // Dimension
  const unsigned Dim = 3;

  // Write JSON file
  const std::string dname = "tests/mpm_explicit_usf/";
  const std::string fname = "mpm-explicit-usf";
  const std::string analysis = "MPMExplicit3D";
  const std::string mpm_scheme = "usf";

  // Write JSON Entity Sets file
  REQUIRE(mpm_test::write_entity_set(dname) == true);

  // Write Mesh
  REQUIRE(mpm_test::write_mesh_3d(dname) == true);

  // Write Particles
  REQUIRE(mpm_test::write_particles_3d(dname) == true);

  // Assign argc and argv to input arguments of MPM
  int argc = 5;
  std::string input_file = dname + "mpm-explicit-usf-3d.json";
  std::string logfile = dname + "mpm-explicit-usf-3d.log";
  // clang-format off
  char* argv[] = {(char*)"./mpm",
                  (char*)"-f",  (char*)"./",
                  // (char*)"-O", &logfile[0],
                  (char*)"-i",  &input_file[0]};
  // clang-format on

  SECTION("Check initialisation") {
    const bool resume = false;
    REQUIRE(mpm_test::write_json(3, resume, analysis, mpm_scheme, fname, dname) ==
            true);

    // Create an IO object
    std::unique_ptr<mpm::IO> io;
    REQUIRE_NOTHROW([&]() {
      io = std::make_unique<mpm::IO>(argc, argv);
    }());

    // Run explicit MPM
    std::unique_ptr<mpm::MPMExplicit<Dim>> mpm;
    REQUIRE_NOTHROW([&]() {
      mpm = std::make_unique<mpm::MPMExplicit<Dim>>(std::move(io));
    }());

    // Initialise materials
    REQUIRE_NOTHROW(mpm->initialise_materials());
    // Initialise mesh
    REQUIRE_NOTHROW(mpm->initialise_mesh(true));
    // Initialise particles
    REQUIRE_NOTHROW(mpm->initialise_particles(true));

    // Renitialise materials
    REQUIRE_THROWS(mpm->initialise_materials());
  }

  SECTION("Check solver") {
    // Create an IO object
    std::unique_ptr<mpm::IO> io;
    REQUIRE_NOTHROW([&]() {
      io = std::make_unique<mpm::IO>(argc, argv);
    }());

    // Run explicit MPM
    std::unique_ptr<mpm::MPMExplicit<Dim>> mpm;
    REQUIRE_NOTHROW([&]() {
      mpm = std::make_unique<mpm::MPMExplicit<Dim>>(std::move(io));
    }());

    // Solve
    REQUIRE(mpm->solve() == true);
    // Test check point restart
    REQUIRE(mpm->checkpoint_resume() == false);
  }

  SECTION("Check resume") {
    // Write JSON file
    const std::string fname = "mpm-explicit-usf";
    const std::string analysis = "MPMExplicit3D";
    const std::string mpm_scheme = "usf";
    bool resume = true;
    REQUIRE(mpm_test::write_json(3, resume, analysis, mpm_scheme, fname, dname) ==
            true);

    // Create an IO object
    std::unique_ptr<mpm::IO> io;
    REQUIRE_NOTHROW([&]() {
      io = std::make_unique<mpm::IO>(argc, argv);
    }());

    // Run explicit MPM
    std::unique_ptr<mpm::MPMExplicit<Dim>> mpm;
    REQUIRE_NOTHROW([&]() {
      mpm = std::make_unique<mpm::MPMExplicit<Dim>>(std::move(io));
    }());

    // Initialise materials
    REQUIRE_NOTHROW(mpm->initialise_materials());
    // Initialise mesh
    REQUIRE_NOTHROW(mpm->initialise_mesh(true));
    // Test check point restart
    REQUIRE(mpm->checkpoint_resume() == true);
    {
      // Solve
      auto io = std::make_unique<mpm::IO>(argc, argv);
      // Run explicit MPM
      auto mpm_resume = std::make_unique<mpm::MPMExplicit<Dim>>(std::move(io));
      REQUIRE(mpm_resume->solve() == true);
    }
  }

  SECTION("Check pressure smoothing") {
    // Create an IO object
    std::unique_ptr<mpm::IO> io;
    REQUIRE_NOTHROW([&]() {
      io = std::make_unique<mpm::IO>(argc, argv);
    }());

    // Run explicit MPM
    std::unique_ptr<mpm::MPMExplicit<Dim>> mpm;
    REQUIRE_NOTHROW([&]() {
      mpm = std::make_unique<mpm::MPMExplicit<Dim>>(std::move(io));
    }());

    // Pressure smoothing
    REQUIRE_NOTHROW(mpm->pressure_smoothing(0));
  }
}

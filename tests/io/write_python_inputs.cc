#include "write_python_inputs.h"

const double absTolerance = 1E-8;

namespace mpm_test {

// Write a python script that implements Hooke's law
void write_python_hooke(double youngs_modulus, double poisson_ratio, const std::string& file_name, std::string directory) {
  std::ofstream outFile(directory + file_name);
  if (outFile.is_open()) {
      outFile << "import numpy as np\n\n";
      outFile << "youngs_modulus = " << youngs_modulus << "\n";
      outFile << "poisson_ratio = " << poisson_ratio << "\n\n";
      outFile << "G = youngs_modulus / (2.0 * (1. + poisson_ratio))\n";
      outFile << "bulk_modulus_ = youngs_modulus / (3.0 * (1. - 2. * poisson_ratio))\n\n";
      outFile << "a1 = bulk_modulus_ + (4.0 / 3.0) * G\n";
      outFile << "a2 = bulk_modulus_ - (2.0 / 3.0) * G\n\n";
      outFile << "C = np.zeros((6,6))\n\n";
      outFile << "C[0,0] = a1\n";
      outFile << "C[0,1] = a2\n";
      outFile << "C[0,2] = a2\n";
      outFile << "C[1,0] = a2\n";
      outFile << "C[1,1] = a1\n";
      outFile << "C[1,2] = a2\n";
      outFile << "C[2,0] = a2\n";
      outFile << "C[2,1] = a2\n";
      outFile << "C[2,2] = a1\n";
      outFile << "C[3,3] = G\n";
      outFile << "C[4,4] = G\n";
      outFile << "C[5,5] = G\n\n";
      outFile << "def stress_computer(rid, de_xx, de_yy, de_zz, de_xy, de_yz, de_xz, mpm_iteration, *statevars):\n";
      outFile << "    print(de_xx)\n";
      outFile << "    de = np.array([de_xx, de_yy, de_zz, de_xy, de_yz, de_xz])\n";
      outFile << "    return tuple(C.dot(de)) + tuple(statevars)\n";

      outFile.close();
  } else {
      throw std::runtime_error("Python script " + file_name + " could not be written");
  }
}

// Write JSON Configuration file
bool write_python_json(unsigned dim, const std::string& analysis,
                const std::string& stress_update, Json material_props,
                const std::string& file_name, const std::string& directory) {
  // Make json object with input files
  // 2D
  std::string dimension = "2d";
  auto particle_type = "P2D";
  auto node_type = "N2D";
  auto cell_type = "ED2Q4";
  auto io_type = "Ascii2D";
  std::vector<double> gravity{{0., 0}};
  unsigned material_id = 0;

  std::string dir = mpm::createDirectory(directory);

  // 3D
  if (dim == 3) {
    dimension = "3d";
    particle_type = "P3D";
    node_type = "N3D";
    cell_type = "ED3H8";
    io_type = "Ascii3D";
    gravity.clear();
    gravity = {0., 0., 0};
  }

  Json json_file = {
      {"title", "Example JSON Input for MPM"},
      {"mesh",
       {{"mesh", dir + "mesh-python-" + dimension + ".txt"},
        {"io_type", io_type},
        {"check_duplicates", true},
        {"isoparametric", false},
        {"node_type", node_type},
        {"cell_type", cell_type}}},
      {"particles",
       {{{"group_id", 0},
         {"generator",
          {{"type", "file"},
           {"material_id", material_id},
           {"pset_id", 0},
           {"io_type", io_type},
           {"particle_type", particle_type},
           {"check_duplicates", true},
           {"location", dir + "particles-python-" + dimension + ".txt"}}}}}},
      {"materials",
       {{{"id", 0},
         {"type", material_props["type"]},
         {"density", material_props["density"]},
         {"script_path", material_props["script_path"]},
         {"function_name", material_props["function_name"]},
         {"log_memory_usage", material_props["log_memory_usage"]},
         {"log_memory_step", material_props["log_memory_step"]},
         {"children_pids_file", material_props["children_pids_file"]}}}},
      {"external_loading_conditions",
       {{"gravity", gravity}}},
      {"analysis",
       {{"type", analysis},
        {"stress_update", stress_update},
        {"locate_particles", true},
        {"dt", 0.001},
        {"uuid", file_name + "-" + dimension},
        {"nsteps", 10},
        {"damping", {{"type", "Cundall"}, {"damping_ratio", 0.02}}}}},
      {"post_processing",
       {{"path", dir + "results-python-" + dimension + "/"},
        {"output_steps", 5}}}};

  // Dump JSON as an input file to be read
  std::string fname = (file_name + "-" + dimension + ".json").c_str();
  std::ofstream file;
  file.open(mpm::createDirectory(directory) + fname, std::ios_base::out);
  file << json_file.dump(2);
  file.close();

  return true;
}

// Write Mesh file in 2D
bool write_python_mesh_2d(std::string directory) {
  // Dimension
  const unsigned dim = 2;

  // Vector of nodal coordinates
  std::vector<Eigen::Matrix<double, dim, 1>> coordinates;

  // Nodal coordinates
  Eigen::Matrix<double, dim, 1> node;

  // Cell 0
  // Node 0
  node << 0., 0.;
  coordinates.emplace_back(node);
  // Node 1
  node << 0.5, 0.;
  coordinates.emplace_back(node);
  // Node 2
  node << 0.5, 0.5;
  coordinates.emplace_back(node);
  // Node 3
  node << 0., 0.5;
  coordinates.emplace_back(node);

  // Cell 1
  // Node 4
  node << 1.0, 0.;
  coordinates.emplace_back(node);
  // Node 5
  node << 1.0, 0.5;
  coordinates.emplace_back(node);

  // Cell with node ids
  std::vector<std::vector<unsigned>> cells{// cell #0
                                           {0, 1, 2, 3},
                                           // cell #1
                                           {1, 4, 5, 2}};

  // Dump mesh file as an input file to be read
  std::ofstream file;
  file.open(mpm::createDirectory(directory) + "mesh-python-2d.txt");
  file << "! elementShape hexahedron\n";
  file << "! elementNumPoints 8\n";
  file << coordinates.size() << "\t" << cells.size() << "\n";

  // Write nodal coordinates
  for (const auto& coord : coordinates) {
    for (unsigned i = 0; i < coord.size(); ++i) file << coord[i] << "\t";
    file << "\n";
  }

  // Write cell node ids
  for (const auto& cell : cells) {
    for (auto nid : cell) file << nid << "\t";
    file << "\n";
  }
  file.close();

  return true;
}

// Write particles file in 2D
bool write_python_particles_2d(std::string directory) {
  const unsigned dim = 2;
  // Vector of particle coordinates
  std::vector<Eigen::Matrix<double, dim, 1>> coordinates;
  coordinates.clear();

  // Particle coordinates
  Eigen::Matrix<double, dim, 1> particle;

  // Cell 0
  // Particle 0
  particle << 0.25, 0.25;
  coordinates.emplace_back(particle);

  // Dump particles coordinates as an input file to be read
  std::ofstream file;
  file.open(mpm::createDirectory(directory) + "particles-python-2d.txt");
  file << coordinates.size() << "\n";
  // Write particle coordinates
  for (const auto& coord : coordinates) {
    for (unsigned i = 0; i < coord.size(); ++i) {
      file << coord[i] << "\t";
    }
    file << "\n";
  }

  file.close();
  return true;
}

// Write Mesh file in 2D
bool write_python_mesh_3d(std::string directory) {
  // Dimension
  const unsigned dim = 3;

  // Vector of nodal coordinates
  std::vector<Eigen::Matrix<double, dim, 1>> coordinates;

  // Nodal coordinates
  Eigen::Matrix<double, dim, 1> node;

  // Cell 0
  // Node 0
  node << 0., 0., 0.;
  coordinates.emplace_back(node);
  // Node 1
  node << 0.5, 0., 0.;
  coordinates.emplace_back(node);
  // Node 2
  node << 0.5, 0.5, 0.;
  coordinates.emplace_back(node);
  // Node 3
  node << 0., 0.5, 0.;
  coordinates.emplace_back(node);
  // Node 4
  node << 0., 0., 0.5;
  coordinates.emplace_back(node);
  // Node 5
  node << 0.5, 0., 0.5;
  coordinates.emplace_back(node);
  // Node 6
  node << 0.5, 0.5, 0.5;
  coordinates.emplace_back(node);
  // Node 7
  node << 0., 0.5, 0.5;
  coordinates.emplace_back(node);

  // Cell 1
  // Node 8
  node << 1.0, 0., 0.;
  coordinates.emplace_back(node);
  // Node 9
  node << 1.0, 0.5, 0.;
  coordinates.emplace_back(node);
  // Node 10
  node << 1.0, 0., 0.5;
  coordinates.emplace_back(node);
  // Node 11
  node << 1.0, 0.5, 0.5;
  coordinates.emplace_back(node);

  // Cell with node ids
  std::vector<std::vector<unsigned>> cells{// cell #0
                                           {0, 1, 2, 3, 4, 5, 6, 7},
                                           // cell #1
                                           {1, 8, 9, 2, 5, 10, 11, 6}};

  // Dump mesh file as an input file to be read
  std::ofstream file;
  file.open(mpm::createDirectory(directory) + "mesh-python-3d.txt");
  file << "! elementShape hexahedron\n";
  file << "! elementNumPoints 8\n";
  file << coordinates.size() << "\t" << cells.size() << "\n";

  // Write nodal coordinates
  for (const auto& coord : coordinates) {
    for (unsigned i = 0; i < coord.size(); ++i) file << coord[i] << "\t";
    file << "\n";
  }

  // Write cell node ids
  for (const auto& cell : cells) {
    for (auto nid : cell) file << nid << "\t";
    file << "\n";
  }

  file.close();

  return true;
}

// Write particles file in 2D
bool write_python_particles_3d(std::string directory) {
  const unsigned dim = 3;
  // Vector of particle coordinates
  std::vector<Eigen::Matrix<double, dim, 1>> coordinates;

  // Particle coordinates
  Eigen::Matrix<double, dim, 1> particle;

  // Cell 0
  // Particle 1
  particle << 0.25, 0.125, 0.125;
  coordinates.emplace_back(particle);
  
  // Dump particles coordinates as an input file to be read
  std::ofstream file;
  file.open(mpm::createDirectory(directory) + "particles-3d.txt");
  file << coordinates.size() << "\n";
  // Write particle coordinates
  for (const auto& coord : coordinates) {
    for (unsigned i = 0; i < coord.size(); ++i) {
      file << coord[i] << "\t";
    }
    file << "\n";
  }

  file.close();
  return true;
}

}  // namespace mpm_test

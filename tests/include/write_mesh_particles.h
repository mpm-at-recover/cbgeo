#include <fstream>

#include "mpm.h"

namespace mpm_test {

// Write JSON Configuration file
bool write_json(unsigned dim, bool resume, const std::string& analysis,
                const std::string& mpm_scheme, const std::string& file_name, const std::string& directory);

// Write JSON Entity Set
bool write_entity_set(std::string directory);

// Write Mesh file in 2D
bool write_mesh_2d(std::string directory);
// Write particles file in 2D
bool write_particles_2d(std::string directory);

// Write mesh file in 3D
bool write_mesh_3d(std::string directory);
// Write particles file in 3D
bool write_particles_3d(std::string directory);

}  // namespace mpm_test

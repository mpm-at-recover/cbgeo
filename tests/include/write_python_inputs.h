#include <fstream>

#include "mpm.h"

namespace mpm_test {

// Write a python script that implements Hooke's law
void write_python_hooke(double youngs_modulus, double poisson_ratio, const std::string& file_name, std::string directory);

// Write JSON Configuration file
bool write_python_json(unsigned dim, const std::string& analysis,
                const std::string& mpm_scheme, Json material_props, const std::string& file_name, const std::string& directory);

// Write Mesh file in 2D
bool write_python_mesh_2d(std::string directory);
// Write particles file in 2D
bool write_python_particles_2d(std::string directory);

// Write Mesh file in 3D
bool write_python_mesh_3d(std::string directory);
// Write particles file in 3D
bool write_python_particles_3d(std::string directory);


}  // namespace mpm_test

//! Constructor
template <unsigned Tdim>
mpm::MPMExplicit<Tdim>::MPMExplicit(const std::shared_ptr<IO>& io)
    : mpm::MPMBase<Tdim>(io) {
  //! Logger
  console_ = spdlog::get("MPMExplicit");
  //! Stress update
  if (this->stress_update_ == "usl") {
    mpm_scheme_ = std::make_shared<mpm::MPMSchemeUSL<Tdim>>(mesh_, dt_);
    timing::compute_stress_prefix = "post";
  }
  else if(this->stress_update_ == "usf") {
    mpm_scheme_ = std::make_shared<mpm::MPMSchemeUSF<Tdim>>(mesh_, dt_);
    timing::compute_stress_prefix = "pre";
  }
  else
    console_->error("{} as stress_update_ is not supported (only 'usf' and 'usl' are)",this->stress_update_);

  //! Interface scheme
  if (this->interface_)
    contact_ = std::make_shared<mpm::ContactFriction<Tdim>>(mesh_);
  else
    contact_ = std::make_shared<mpm::Contact<Tdim>>(mesh_);
}

//! MPM Explicit compute stress strain
template <unsigned Tdim>
void mpm::MPMExplicit<Tdim>::compute_stress_strain(unsigned phase) {
  // Iterate over each particle to calculate strain
  
  mesh_->iterate_over_particles(std::bind(
      &mpm::ParticleBase<Tdim>::compute_strain, std::placeholders::_1, dt_));
  

  // Iterate over each particle to update particle volume
  mesh_->iterate_over_particles(std::bind(
      &mpm::ParticleBase<Tdim>::update_volume, std::placeholders::_1));
  

  // Pressure smoothing
  if (pressure_smoothing_) this->pressure_smoothing(phase);
  

  // Iterate over each particle to compute stress
  mesh_->iterate_over_particles(std::bind(
      &mpm::ParticleBase<Tdim>::compute_stress, std::placeholders::_1));
  
}

//! MPM Explicit solver
template <unsigned Tdim>
bool mpm::MPMExplicit<Tdim>::solve() {
  // Shortcuts for std::chrono
  using namespace std::chrono;
  using seconds = duration<double>;

#if TIMING_DEPTH >= 0
  // Declare start_time and end_time
  high_resolution_clock::time_point start_time, end_time;
  start_time = high_resolution_clock::now();
#endif

  // Check the time before initializing the simulation
  auto sim_begin = steady_clock::now();


  bool status = true;

  console_->info("MPM analysis type {}", io_->analysis_type());

  // Initialise MPI rank and size
  int mpi_rank = 0;
  int mpi_size = 1;

#if USE_MPI
  // Get MPI rank
  MPI_Comm_rank(MPI_COMM_WORLD, &mpi_rank);
  // Get number of MPI ranks
  MPI_Comm_size(MPI_COMM_WORLD, &mpi_size);
#endif

  // Phase
  const unsigned phase = 0;

  // Test if checkpoint resume is needed
  bool resume = false;
  if (analysis_.find("resume") != analysis_.end())
    resume = analysis_["resume"]["resume"].template get<bool>();

  // Pressure smoothing
  pressure_smoothing_ = io_->analysis_bool("pressure_smoothing");

  // Interface
  interface_ = io_->analysis_bool("interface");

  // Initialise material
  this->initialise_materials();

  // Initialise mesh
  this->initialise_mesh();

  // Initialise particles
  if (!resume) this->initialise_particles();

  // Create nodal properties
  if (interface_) mesh_->create_nodal_properties();

  // Compute mass
  if (!resume)
    mesh_->iterate_over_particles(std::bind(
        &mpm::ParticleBase<Tdim>::compute_mass, std::placeholders::_1));

  // Check point resume
  if (resume) {
    this->checkpoint_resume();
    mesh_->resume_domain_cell_ranks();
#if USE_MPI
#ifdef USE_GRAPH_PARTITIONING
    MPI_Barrier(MPI_COMM_WORLD);
#endif
#endif
  } else {
    // Domain decompose
    bool initial_step = (resume == true) ? false : true;
    this->mpi_domain_decompose(initial_step);
  }

  //! Particle entity sets and velocity constraints
  if (resume) {
    this->particle_entity_sets(false);
    this->particle_velocity_constraints();
  }

  // Initialise loading conditions
  this->initialise_loads();

#if TIMING_DEPTH >= 0
  end_time = high_resolution_clock::now();
  timing::execution_times["initialise_simulation"] += duration_cast<seconds>(end_time - start_time).count();
  timing::execution_count["initialise_simulation"] += 1;
#endif

  auto solver_begin = steady_clock::now();
  // Main loop
  for (; step_ < nsteps_; ++step_) {
    if (mpi_rank == 0 && (step_ % (nsteps_/verbosity_))==0) {
#if TIMING_DEPTH >= 0
      start_time = high_resolution_clock::now();
#endif
      // Estimate the remaining time in seconds
      double elapsed_time = duration_cast<seconds>(steady_clock::now() - solver_begin).count();
      timing::compute_speed(elapsed_time, step_);
      std::string remaining_time = timing::format_duration((nsteps_-step_) / timing::speed);
      if (step_==0) remaining_time = "undefined";

      // Display advancement information
      console_->info("\n\tStep: {} of {} ({}%)\n\tAveraged speed since last info: {} iter/s\n\tEstimated remaining time: {}\n", step_, nsteps_, step_*100/nsteps_, timing::speed, remaining_time);
#if TIMING_DEPTH >= 0
      end_time = high_resolution_clock::now();
      timing::execution_times["verbosity"] += duration_cast<seconds>(end_time - start_time).count();
      timing::execution_count["verbosity"] += 1;
#endif
    }
#if USE_MPI
#ifdef USE_GRAPH_PARTITIONING
    // Run load balancer at a specified frequency
    if (step_ % nload_balance_steps_ == 0 && step_ != 0)
      this->mpi_domain_decompose(false);
#endif
#endif

    // Inject particles
#if TIMING_DEPTH >= 0
    start_time = high_resolution_clock::now();
#endif
    mesh_->inject_particles(step_ * dt_);
#if TIMING_DEPTH >= 0
    end_time = high_resolution_clock::now();
    timing::execution_times["inject_particles"] += duration_cast<seconds>(end_time - start_time).count();
    timing::execution_count["inject_particles"] += 1;
#endif

    // Initialise nodes, cells and shape functions
#if TIMING_DEPTH >= 0
    start_time = high_resolution_clock::now();
#endif
    mpm_scheme_->initialise();
#if TIMING_DEPTH >= 0
    end_time = high_resolution_clock::now();
    timing::execution_times["initialise_mpm_scheme"] += duration_cast<seconds>(end_time - start_time).count();
    timing::execution_count["initialise_mpm_scheme"] += 1;
#endif

    // Initialise nodal properties and append material ids to node
#if TIMING_DEPTH >= 0
    start_time = high_resolution_clock::now();
#endif
    contact_->initialise();
#if TIMING_DEPTH >= 0
    end_time = high_resolution_clock::now();
    timing::execution_times["initialise_contact"] += duration_cast<seconds>(end_time - start_time).count();
    timing::execution_count["initialise_contact"] += 1;
#endif

    // Mass momentum and compute velocity at nodes
#if TIMING_DEPTH >= 0
    start_time = high_resolution_clock::now();
#endif
    mpm_scheme_->compute_nodal_kinematics(velocity_update_, phase, step_);
#if TIMING_DEPTH >= 0
    end_time = high_resolution_clock::now();
    timing::execution_times["compute_nodal_kinematics"] += duration_cast<seconds>(end_time - start_time).count();
    timing::execution_count["compute_nodal_kinematics"] += 1;
#endif

    // Map material properties to nodes
#if TIMING_DEPTH >= 0
    start_time = high_resolution_clock::now();
#endif
    contact_->compute_contact_forces();
#if TIMING_DEPTH >= 0
    end_time = high_resolution_clock::now();
    timing::execution_times["compute_contact_forces"] += duration_cast<seconds>(end_time - start_time).count();
    timing::execution_count["compute_contact_forces"] += 1;
#endif

    // Possible update of stress=f(strain) depending on USF (function is effective) or USL (function is empty) choice
#if TIMING_DEPTH >= 0
    start_time = high_resolution_clock::now();
#endif
    mpm_scheme_->precompute_stress_strain(phase, pressure_smoothing_);
#if TIMING_DEPTH >= 0
    end_time = high_resolution_clock::now();
    timing::execution_times["precompute_stress_strain"] += duration_cast<seconds>(end_time - start_time).count();
    timing::execution_count["precompute_stress_strain"] += 1;
#endif

    // Compute forces
#if TIMING_DEPTH >= 0
    start_time = high_resolution_clock::now();
#endif
    mpm_scheme_->compute_forces(gravity_, phase, step_,
                                set_node_concentrated_force_);
#if TIMING_DEPTH >= 0
    end_time = high_resolution_clock::now();
    timing::execution_times["compute_forces"] += duration_cast<seconds>(end_time - start_time).count();
    timing::execution_count["compute_forces"] += 1;
#endif

    // Nodal acceleration and Particle kinematics
#if TIMING_DEPTH >= 0
    start_time = high_resolution_clock::now();
#endif
    mpm_scheme_->compute_particle_kinematics(velocity_update_, flip_coeff_, phase,
                                             "Cundall", damping_factor_, step_, update_defgrad_);
#if TIMING_DEPTH >= 0
    end_time = high_resolution_clock::now();
    timing::execution_times["compute_particle_kinematics"] += duration_cast<seconds>(end_time - start_time).count();
    timing::execution_count["compute_particle_kinematics"] += 1;
#endif
    
    // Possible update of stress=f(strain) depending on USF (function is empty) or USL (function is effective) choice
#if TIMING_DEPTH >= 0
    start_time = high_resolution_clock::now();
#endif
    mpm_scheme_->postcompute_stress_strain(phase, pressure_smoothing_);
#if TIMING_DEPTH >= 0
    end_time = high_resolution_clock::now();
    timing::execution_times["postcompute_stress_strain"] += duration_cast<seconds>(end_time - start_time).count();
    timing::execution_count["postcompute_stress_strain"] += 1;
#endif

    // Locate particles
#if TIMING_DEPTH >= 0
    start_time = high_resolution_clock::now();
#endif
    mpm_scheme_->locate_particles(this->locate_particles_);
#if TIMING_DEPTH >= 0
    end_time = high_resolution_clock::now();
    timing::execution_times["locate_particles"] += duration_cast<seconds>(end_time - start_time).count();
    timing::execution_count["locate_particles"] += 1;
#endif

#if USE_MPI
#ifdef USE_GRAPH_PARTITIONING
    mesh_->transfer_halo_particles();
    MPI_Barrier(MPI_COMM_WORLD);
#endif
#endif

    if (step_ % output_steps_ == 0) {
      // csv outputs
#if TIMING_DEPTH >= 0
    start_time = high_resolution_clock::now();
#endif
      this->write_csv(this->step_, this->nsteps_);
#if TIMING_DEPTH >= 0
    end_time = high_resolution_clock::now();
      timing::execution_times["write_csv"] += duration_cast<seconds>(end_time - start_time).count();
      timing::execution_count["write_csv"] += 1;
#endif
#if USE_VTK
      // VTK outputs
      this->write_vtk(this->step_, this->nsteps_);
#endif
    }
  }

  this->finalize_particles();
  auto solver_end = steady_clock::now();
  timing::total_time = duration_cast<seconds>(solver_end - sim_begin).count();
  timing::main_loop_time = duration_cast<seconds>(solver_end - solver_begin).count();
  timing::compute_speed(timing::main_loop_time, nsteps_, true);

  console_->info("Rank {}, Explicit {} solver raw duration: {} s\n\tTotal duration: {}\n\tAveraged speed: {} iter/s", mpi_rank,
              mpm_scheme_->scheme(),
              timing::total_time,
              timing::format_duration(timing::total_time),
              timing::speed);

// Timing information
std::string result_dir = io_->output_folder() + this->uuid_;
tbb_tools::nthreads = this->io_->nthreads();

    // Json file
std::string timing_json_file_path = result_dir + "/timing.json";
timing::generate_json_times(timing_json_file_path);

    // CSV file
std::string speed_csv_file_path = result_dir + "/speed.csv";
timing::write_performance_csv(speed_csv_file_path);

#if TIMING_DEPTH >= 0
    // Text file
  std::string timing_text_file_path = result_dir + "/timing.txt";
  std::string current_time = timing::current_date_time();
  std::string run_date_str = "Ran on " + current_time + " using " + std::to_string(tbb_tools::nthreads) + " threads, ";
  timing::print_detailed_times_to_file(timing_text_file_path, false, run_date_str + "sorted by execution sequence:");
  timing::print_detailed_times_to_file(timing_text_file_path, true, "\n" + run_date_str + "sorted by cost:");
#endif

  return status;
}

//! Constructor
template <unsigned Tdim>
mpm::MPMBase<Tdim>::MPMBase(const std::shared_ptr<IO>& io) : mpm::MPM(io) {
  //! Logger
  console_ = spdlog::get("MPMBase");

  // Create a mesh with global id 0
  const mpm::Index id = 0;

  // Set analysis step to start at 0
  step_ = 0;

  // Set mesh as isoparametric
  bool isoparametric = is_isoparametric();

  mesh_ = std::make_shared<mpm::Mesh<Tdim>>(id, isoparametric);

  // Create constraints
  constraints_ = std::make_shared<mpm::Constraints<Tdim>>(mesh_);

  // Empty all materials
  materials_.clear();

  // Variable list
  tsl::robin_map<std::string, VariableType> variables = {
      // Scalar variables
      {"mass", VariableType::Scalar},
      {"volume", VariableType::Scalar},
      {"mass_density", VariableType::Scalar},
      // Vector variables
      {"displacements", VariableType::Vector},
      {"velocities", VariableType::Vector},
      // Tensor variables
      {"strains", VariableType::Tensor},
      {"stresses", VariableType::Tensor}};

  try {
    analysis_ = io_->analysis();
    // Time-step size
    dt_ = analysis_["dt"].template get<double>();
    // Number of time steps
    nsteps_ = analysis_["nsteps"].template get<mpm::Index>();

    // Verbosity (number of line to be printed)
    if (analysis_.find("verbosity") != analysis_.end()) {
      verbosity_ = analysis_["verbosity"].template get<mpm::Index>();
      if (verbosity_>nsteps_) {
        console_->warn(
          "{} #{}: Verbosity higher than nsteps, using nsteps instead ",
          __FILE__, __LINE__);
        verbosity_ = nsteps_;
      }
    }
    else {
      console_->warn(
        "{} #{}: Verbosity is not specified, using nsteps as "
        "default",
        __FILE__, __LINE__);
        verbosity_ = nsteps_;
    }

    // nload balance
    if (analysis_.find("nload_balance_steps") != analysis_.end())
      nload_balance_steps_ =
          analysis_["nload_balance_steps"].template get<mpm::Index>();

    // Locate particles
    if (analysis_.find("locate_particles") != analysis_.end())
      locate_particles_ = analysis_["locate_particles"].template get<bool>();

    // Stress update method (USF/USL)
    try {
      if (analysis_.find("mpm_scheme") != analysis_.end())
        stress_update_ = analysis_["mpm_scheme"].template get<std::string>();
    } catch (std::exception& exception) {
      console_->warn(
          "{} #{}: {}. Stress update method is not specified, using USF as "
          "default",
          __FILE__, __LINE__, exception.what());
    }

    // Velocity update
    try {
      velocity_update_ = analysis_["velocity_update"].template get<std::string>();
    } catch (std::exception& exception) {
      console_->warn(
          "{} #{}: Velocity update parameter is not specified, using default "
          "as 'flip'",
          __FILE__, __LINE__, exception.what());
      velocity_update_ = "flip";
    }
    
    // Mixture coefficient (think of a *FLIP-PIC blend)
    if (velocity_update_.find("flip") != std::string::npos){
	    // nflip, flip, aflip, nflipX, flipX, aflipX, anflipX cases
	    if ((velocity_update_ != "flip")   && 
          (velocity_update_ != "nflip")  && 
          (velocity_update_ != "aflip")  && 
          (velocity_update_ != "anflip") && 
          (velocity_update_ != "tflip")) {
		    // sub-cases flipX, nflipX
		    std::size_t flip_pos = velocity_update_.find("flip");
		    if (flip_pos == 0) { // If velocity_update is 'flip x pic'
			    flip_coeff_ = std::stod(velocity_update_.substr (4));
		    }
		    else if ((flip_pos != std::string::npos) && (flip_pos == 1)) { // If velocity_update is 'nflip x pic' or 'aflip x pic' or 'tflip x pic'
			    flip_coeff_ = std::stod(velocity_update_.substr (5));
		    }
        else if ((flip_pos != std::string::npos) && (flip_pos == 2)) { // If velocity_update is 'anflip x pic'
			    flip_coeff_ = std::stod(velocity_update_.substr (6));
		    }
	    } // no need of else: default flip_coeff__ = 1 is fine for sub-cases flip, nflip, aflip and anflip
    }
    else // pic and apic cases
	    flip_coeff_ = 0;
    if (flip_coeff_ < 0 || flip_coeff_ > 1)
	    console_->error("error: obtained {} for flip_coeff_ (should be between 0 and 1)", flip_coeff_);

    // Damping
    try {
      if (analysis_.find("damping") != analysis_.end()) {
        if (!initialise_damping(analysis_.at("damping")))
          throw std::runtime_error("Damping parameters are not defined");
      }
    } catch (std::exception& exception) {
      console_->warn("{} #{}: Damping is not specified, using none as default",
                     __FILE__, __LINE__, exception.what());
    }

    // Math functions
    try {
      // Get materials properties
      auto math_functions = io_->json_object("math_functions");
      if (!math_functions.empty())
        this->initialise_math_functions(math_functions);
    } catch (std::exception& exception) {
      console_->warn("{} #{}: No math functions are defined", __FILE__,
                     __LINE__, exception.what());
    }

    post_process_ = io_->post_processing();
    // Output steps
    output_steps_ = post_process_["output_steps"].template get<mpm::Index>();

  } catch (std::domain_error& domain_error) {
    console_->error("{} {} Get analysis object: {}", __FILE__, __LINE__,
                    domain_error.what());
    abort();
  }

  // VTK particle variables
  // Initialise container with empty vector
  vtk_vars_.insert(
      std::make_pair(mpm::VariableType::Scalar, std::vector<std::string>()));
  vtk_vars_.insert(
      std::make_pair(mpm::VariableType::Vector, std::vector<std::string>()));
  vtk_vars_.insert(
      std::make_pair(mpm::VariableType::Tensor, std::vector<std::string>()));

  if ((post_process_.find("vtk") != post_process_.end()) &&
      post_process_.at("vtk").is_array() &&
      post_process_.at("vtk").size() > 0) {
    // Iterate over vtk
    for (unsigned i = 0; i < post_process_.at("vtk").size(); ++i) {
      std::string attribute =
          post_process_["vtk"][i].template get<std::string>();
      if (variables.find(attribute) != variables.end())
        vtk_vars_[variables.at(attribute)].emplace_back(attribute);
      else {
        console_->warn(
            "{} #{}: VTK variable '{}' was specified, but is not available "
            "in variable list",
            __FILE__, __LINE__, attribute);
      }
    }
  } else {
    console_->warn(
        "{} #{}: No VTK variables were specified, none will be generated",
        __FILE__, __LINE__);
  }

  // VTK state variables
  bool vtk_statevar = false;
  if ((post_process_.find("vtk_statevars") != post_process_.end()) &&
      post_process_.at("vtk_statevars").is_array() &&
      post_process_.at("vtk_statevars").size() > 0) {
    // Iterate over state_vars
    for (const auto& svars : post_process_["vtk_statevars"]) {
      // Phase id
      unsigned phase_id = 0;
      if (svars.contains("phase_id"))
        phase_id = svars.at("phase_id").template get<unsigned>();

      // State variables
      if (svars.at("statevars").is_array() &&
          svars.at("statevars").size() > 0) {
        // Insert vtk_statevars_
        const std::vector<std::string> state_var = svars["statevars"];
        vtk_statevars_.insert(std::make_pair(phase_id, state_var));
        vtk_statevar = true;
      } else {
        vtk_statevar = false;
        break;
      }
    }
  }
  if (!vtk_statevar)
    console_->warn(
        "{} #{}: No VTK state variable were specified, none will be generated",
        __FILE__, __LINE__);
}

// Initialise mesh
template <unsigned Tdim>
void mpm::MPMBase<Tdim>::initialise_mesh(bool testing) {
  // Initialise MPI rank and size
  int mpi_rank = 0;
  int mpi_size = 1;

#if USE_MPI
  MPI_Comm_rank(MPI_COMM_WORLD, &mpi_rank);
  // Get number of MPI ranks
  MPI_Comm_size(MPI_COMM_WORLD, &mpi_size);
#endif

  // Get mesh properties
  auto mesh_props = io_->json_object("mesh");
  // Get Mesh reader from JSON object
  const std::string io_type = mesh_props["io_type"].template get<std::string>();

  bool check_duplicates = true;
  try {
    check_duplicates = mesh_props["check_duplicates"].template get<bool>();
  } catch (std::exception& exception) {
    if (!testing) { 
      console_->warn(
        "{} #{}: Check duplicates, not specified setting default as true",
        __FILE__, __LINE__, exception.what());
      check_duplicates = true;
    }
    else throw;
  }

  // Set flags depending on which values are defined in the mesh properties
  bool has_boundary_cond = mesh_props.find("boundary_conditions") != mesh_props.end();
  bool has_euler_angles = false;
  if (has_boundary_cond) 
    has_euler_angles = mesh_props["boundary_conditions"].find("nodal_euler_angles") != mesh_props["boundary_conditions"].end();

  // Create a mesh reader
  auto mesh_io = Factory<mpm::IOMesh<Tdim>>::instance()->create(io_type);

  auto nodes_begin = std::chrono::steady_clock::now();
  // Global Index
  mpm::Index gid = 0;
  // Node type
  const auto node_type = mesh_props["node_type"].template get<std::string>();

  // Mesh file
  std::string mesh_file =
      io_->file_name(mesh_props["mesh"].template get<std::string>());

  // Create nodes from file
  bool node_status =
      mesh_->create_nodes(gid,                                  // global id
                          node_type,                            // node type
                          mesh_io->read_mesh_nodes(mesh_file),  // coordinates
                          check_duplicates);                    // check dups

  if (!node_status)
    throw std::runtime_error(
        "mpm::base::init_mesh(): Addition of nodes to mesh failed");

  auto nodes_end = std::chrono::steady_clock::now();
  console_->info("Rank {} Read nodes: {} ms", mpi_rank,
                 std::chrono::duration_cast<std::chrono::milliseconds>(
                     nodes_end - nodes_begin)
                     .count());

  // Read and assign node sets
  this->node_entity_sets(mesh_props, check_duplicates, testing);

  // Read nodal euler angles and assign rotation matrices
  if (has_euler_angles) this->node_euler_angles(mesh_props, mesh_io, testing);

  // Read and assign velocity constraints
  this->nodal_velocity_constraints(mesh_props, mesh_io, testing);

  // Read and assign friction constraints
  this->nodal_frictional_constraints(mesh_props, mesh_io, testing);

  // Initialise cell
  auto cells_begin = std::chrono::steady_clock::now();
  // Shape function name
  const auto cell_type = mesh_props["cell_type"].template get<std::string>();
  // Shape function
  std::shared_ptr<mpm::Element<Tdim>> element =
      Factory<mpm::Element<Tdim>>::instance()->create(cell_type);

  // Create cells from file
  bool cell_status =
      mesh_->create_cells(gid,                                  // global id
                          element,                              // element tyep
                          mesh_io->read_mesh_cells(mesh_file),  // Node ids
                          check_duplicates);                    // Check dups

  if (!cell_status)
    throw std::runtime_error(
        "mpm::base::init_mesh(): Addition of cells to mesh failed");

  // Compute cell neighbours
  mesh_->find_cell_neighbours(testing);

  // Read and assign cell sets
  this->cell_entity_sets(mesh_props, check_duplicates, testing);

  // Use Nonlocal basis
  if (cell_type.back() == 'B' || cell_type.back() == 'L') {
    this->initialise_nonlocal_mesh(mesh_props, testing);
  }

  // Save some shape functions in a csv file if the user specified a list of nodes in `save_nodes_shapefn`
  this->save_shapefn(mesh_props);

  auto cells_end = std::chrono::steady_clock::now();
  console_->info("Rank {} Read cells: {} ms", mpi_rank,
                 std::chrono::duration_cast<std::chrono::milliseconds>(
                     cells_end - cells_begin)
                     .count());
}

// Initialise particles
template <unsigned Tdim>
void mpm::MPMBase<Tdim>::initialise_particles(bool testing) {
  // Initialise MPI rank and size
  int mpi_rank = 0;
  int mpi_size = 1;
#if USE_MPI
  MPI_Comm_rank(MPI_COMM_WORLD, &mpi_rank);
  // Get number of MPI ranks
  MPI_Comm_size(MPI_COMM_WORLD, &mpi_size);
#endif

  // Get mesh properties
  auto mesh_props = io_->json_object("mesh");
  // Get Mesh reader from JSON object
  const std::string io_type = mesh_props["io_type"].template get<std::string>();

  // Check duplicates default set to true
  bool check_duplicates = true;
  if (mesh_props.find("check_duplicates") != mesh_props.end())
    check_duplicates = mesh_props["check_duplicates"].template get<bool>();

  auto particles_gen_begin = std::chrono::steady_clock::now();

  // Get particles properties
  auto json_particles = io_->json_object("particles");

  for (const auto& json_particle : json_particles) {
    // Generate particles
    bool gen_status =
        mesh_->generate_particles(io_, json_particle["generator"], testing);
    if (!gen_status)
      std::runtime_error(
          "mpm::base::init_particles() Generate particles failed");
  }

  auto particles_gen_end = std::chrono::steady_clock::now();
  console_->info("Rank {} Generate particles: {} ms", mpi_rank,
                 std::chrono::duration_cast<std::chrono::milliseconds>(
                     particles_gen_end - particles_gen_begin)
                     .count());

  auto particles_locate_begin = std::chrono::steady_clock::now();

  // Create a mesh reader
  auto particle_io = Factory<mpm::IOMesh<Tdim>>::instance()->create(io_type);

  // Read and assign particles cells
  this->particles_cells(mesh_props, particle_io, testing);

  // Locate particles in cell
  auto unlocatable_particles = mesh_->locate_particles_mesh(testing);

  if (!unlocatable_particles.empty())
    throw std::runtime_error(
        "mpm::base::init_particles() Particle outside the mesh domain");

  // Write particles and cells to file
  particle_io->write_particles_cells(
      io_->output_file("particles-cells", ".txt", uuid_, 0, 0).string(),
      mesh_->particles_cells(testing));

  auto particles_locate_end = std::chrono::steady_clock::now();
  console_->info("Rank {} Locate particles: {} ms", mpi_rank,
                 std::chrono::duration_cast<std::chrono::milliseconds>(
                     particles_locate_end - particles_locate_begin)
                     .count());

  auto particles_volume_begin = std::chrono::steady_clock::now();
  // Compute volume
  mesh_->iterate_over_particles(std::bind(
      &mpm::ParticleBase<Tdim>::compute_volume, std::placeholders::_1, testing));

  // Read and assign particles volumes
  this->particles_volumes(mesh_props, particle_io, testing);

  // Read and assign particles stresses
  this->particles_stresses(mesh_props, particle_io, testing);

  // Read and assign particles velocities
  this->particles_velocities(mesh_props, particle_io, testing);

  auto particles_volume_end = std::chrono::steady_clock::now();
  console_->info("Rank {} Read volume, velocity and stresses: {} ms", mpi_rank,
                 std::chrono::duration_cast<std::chrono::milliseconds>(
                     particles_volume_end - particles_volume_begin)
                     .count());

  // Particle entity sets
  this->particle_entity_sets(check_duplicates, testing);

  // Read and assign particles velocity constraints
  this->particle_velocity_constraints(testing);

  console_->info("Rank {} Create particle sets: {} ms", mpi_rank,
                 std::chrono::duration_cast<std::chrono::milliseconds>(
                     particles_volume_end - particles_volume_begin)
                     .count());

  // Material id update using particle sets
  try {
    if (io_->json_has("material_sets")) {
      auto material_sets = io_->json_object("material_sets");
      if (!material_sets.empty()) {
        for (const auto& material_set : material_sets) {
          unsigned material_id =
              material_set["material_id"].template get<unsigned>();
          unsigned phase_id = mpm::ParticlePhase::Solid;
          if (material_set.contains("phase_id"))
            phase_id = material_set["phase_id"].template get<unsigned>();
          unsigned pset_id = material_set["pset_id"].template get<unsigned>();
          // Update material_id for particles in each pset
          mesh_->iterate_over_particle_set(
              pset_id, std::bind(&mpm::ParticleBase<Tdim>::assign_material,
                                std::placeholders::_1,
                                materials_.at(material_id), phase_id, testing));
        }
      }
    }
  } catch (std::exception& exception) {
    if (!testing) console_->warn("{} #{}: Material sets are not specified", __FILE__,
                   __LINE__, exception.what());
    else throw;
  }
}

// Finalize particles
template <unsigned Tdim>
void mpm::MPMBase<Tdim>::finalize_particles() {
  try {
    auto material_sets = io_->json_object("material_sets");
    if (!material_sets.empty()) {
      for (const auto& material_set : material_sets) {
        unsigned material_id =
            material_set["material_id"].template get<unsigned>();
        unsigned phase_id = mpm::ParticlePhase::Solid;
        if (material_set.contains("phase_id"))
          phase_id = material_set["phase_id"].template get<unsigned>();
        unsigned pset_id = material_set["pset_id"].template get<unsigned>();
        // Update material_id for particles in each pset
        mesh_->iterate_over_particle_set(
            pset_id, std::bind(&mpm::ParticleBase<Tdim>::finalize_material,
                               std::placeholders::_1, phase_id));
      }
    }
  } catch (std::exception& exception) {
    console_->warn("{} #{}: Material sets not finalized", __FILE__,
                   __LINE__, exception.what());
  }
}

// Initialise materials
template <unsigned Tdim>
void mpm::MPMBase<Tdim>::initialise_materials() {
  // Get number of particles (only works if all particles are made of the same material)
  auto particles_props = io_->json_object("particles")[0]["generator"];
  unsigned n_particles;

  if (particles_props["type"].template get<std::string>() == "file") {
    std::ifstream pfile(particles_props["location"].template get<std::string>());
    std::string sLine;
    getline(pfile, sLine);
    try {
      n_particles = std::stoi(sLine);
    }
    catch (std::exception& exception) {
      console_->info("{} {} Particle file {} could not be read: {}", __FILE__,
                   __LINE__, particles_props["location"].template get<std::string>(), exception.what());
      throw std::runtime_error(
          "Particle file could not be read");
    }
  }
  else if (particles_props["type"].template get<std::string>() == "gauss") {
    int nppd = particles_props["nparticles_per_dir"].template get<int>();
    n_particles = pow(nppd, Tdim);
  }
  

  // Get materials properties
  auto materials = io_->json_object("materials");

  for (auto material_props : materials) {
    // Add particle number to material properties
    material_props += Json::object_t::value_type("n_particles", n_particles);

    // Add input file's path to material properties
    std::string input_file = io_->input_file_;
    material_props += Json::object_t::value_type("input_file_path", input_file);

    // Add nthreads to material properties
    unsigned nthreads = io_->nthreads();
    material_props += Json::object_t::value_type("nthreads", nthreads);

    // Add CPU offset to material properties
    unsigned cpu_offset = io_->cpu_offset();
    material_props += Json::object_t::value_type("cpu_offset", cpu_offset);

    // Get material type
    const std::string material_type =
        material_props["type"].template get<std::string>();

    // Get material id
    auto material_id = material_props["id"].template get<unsigned>();

    // Create a new material from JSON object
    auto mat =
        Factory<mpm::Material<Tdim>, unsigned, const Json&>::instance()->create(
            material_type, std::move(material_id), material_props);
    
    // Add material to list
    auto result = materials_.insert(std::make_pair(mat->id(), mat));

    // If insert material failed
    if (!result.second)
      throw std::runtime_error(
          "mpm::base::init_materials(): New material cannot be added, "
          "insertion failed");
  }
  // Copy materials to mesh
  mesh_->initialise_material_models(this->materials_);
}

//! Checkpoint resume
template <unsigned Tdim>
bool mpm::MPMBase<Tdim>::checkpoint_resume() {
  bool checkpoint = true;
  try {
    // TODO: Set phase
    const unsigned phase = 0;

    int mpi_rank = 0;
#if USE_MPI
    MPI_Comm_rank(MPI_COMM_WORLD, &mpi_rank);
#endif

    if (!analysis_["resume"]["resume"].template get<bool>())
      throw std::runtime_error("Resume analysis option is disabled!");

    // Get unique analysis id
    this->uuid_ = analysis_["resume"]["uuid"].template get<std::string>();
    // Get step
    this->step_ = analysis_["resume"]["step"].template get<mpm::Index>();

    // Input particle h5 file for resume
    std::string attribute = "particles";
    std::string extension = ".csv";

    auto particles_file =
        io_->output_file(attribute, extension, uuid_, step_, this->nsteps_)
            .string();

    // Load particle information from file
    mesh_->read_particles_csv(phase, particles_file);

    // Clear all particle ids
    mesh_->iterate_over_cells(
        std::bind(&mpm::Cell<Tdim>::clear_particle_ids, std::placeholders::_1));

    // Locate particles
    auto unlocatable_particles = mesh_->locate_particles_mesh();

    if (!unlocatable_particles.empty())
      throw std::runtime_error("Particle outside the mesh domain");

    // Increament step
    ++this->step_;
    console_->info("Checkpoint resume at step {} of {}", this->step_,
                   this->nsteps_);

  } catch (std::exception& exception) {
    console_->info("{} {} Resume failed, restarting analysis: {}", __FILE__,
                   __LINE__, exception.what());
    this->step_ = 0;
    checkpoint = false;
  }
  return checkpoint;
}

//! Write csv files
template <unsigned Tdim>
void mpm::MPMBase<Tdim>::write_csv(mpm::Index step, mpm::Index max_steps) {
  // Write input geometry to vtk file
  std::string attribute = "particles";
  std::string extension = ".csv";

  auto particles_file =
      io_->output_file(attribute, extension, uuid_, step, max_steps).string();

  const unsigned phase = 0;
  mesh_->write_particles_csv(phase, particles_file, velocity_update_);

  if (io_->analysis_bool("write_nodes_csv")) {
    attribute = "nodes";
    extension = ".csv";
    auto nodes_file =
        io_->output_file(attribute, extension, uuid_, step, max_steps).string();

    mesh_->write_nodes_csv(phase, nodes_file);
  }
}

#if USE_VTK
//! Write VTK files
template <unsigned Tdim>
void mpm::MPMBase<Tdim>::write_vtk(mpm::Index step, mpm::Index max_steps) {

  // VTK PolyData writer
  auto vtk_writer = std::make_unique<VtkWriter>(mesh_->particle_coordinates());

  // Write mesh on step 0
  // Get active node pairs use true
  if (step % nload_balance_steps_ == 0)
    vtk_writer->write_mesh(
        io_->output_file("mesh", ".vtp", uuid_, step, max_steps).string(),
        mesh_->nodal_coordinates(), mesh_->node_pairs(true));

  // Write input geometry to vtk file
  const std::string extension = ".vtp";
  const std::string attribute = "geometry";
  auto meshfile =
      io_->output_file(attribute, extension, uuid_, step, max_steps).string();
  vtk_writer->write_geometry(meshfile);

  // MPI parallel vtk file
  int mpi_rank = 0;
  int mpi_size = 1;
  bool write_mpi_rank = false;

#if USE_MPI
  // Get MPI rank
  MPI_Comm_rank(MPI_COMM_WORLD, &mpi_rank);
  // Get number of MPI ranks
  MPI_Comm_size(MPI_COMM_WORLD, &mpi_size);
#endif

  //! VTK scalar variables
  for (const auto& attribute : vtk_vars_.at(mpm::VariableType::Scalar)) {
    // Write scalar
    auto file =
        io_->output_file(attribute, extension, uuid_, step, max_steps).string();
    vtk_writer->write_scalar_point_data(
        file, mesh_->particles_scalar_data(attribute), attribute);

    // Write a parallel MPI VTK container file
#if USE_MPI
    if (mpi_rank == 0 && mpi_size > 1) {
      auto parallel_file = io_->output_file(attribute, ".pvtp", uuid_, step,
                                            max_steps, write_mpi_rank)
                               .string();

      vtk_writer->write_parallel_vtk(parallel_file, attribute, mpi_size, step,
                                     max_steps);
    }
#endif
  }

  //! VTK vector variables
  for (const auto& attribute : vtk_vars_.at(mpm::VariableType::Vector)) {
    // Write vector
    auto file =
        io_->output_file(attribute, extension, uuid_, step, max_steps).string();
    vtk_writer->write_vector_point_data(
        file, mesh_->particles_vector_data(attribute), attribute);

    // Write a parallel MPI VTK container file
#if USE_MPI
    if (mpi_rank == 0 && mpi_size > 1) {
      auto parallel_file = io_->output_file(attribute, ".pvtp", uuid_, step,
                                            max_steps, write_mpi_rank)
                               .string();

      vtk_writer->write_parallel_vtk(parallel_file, attribute, mpi_size, step,
                                     max_steps);
    }
#endif
  }

  //! VTK tensor variables
  for (const auto& attribute : vtk_vars_.at(mpm::VariableType::Tensor)) {
    // Write vector
    auto file =
        io_->output_file(attribute, extension, uuid_, step, max_steps).string();
    vtk_writer->write_tensor_point_data(
        file, mesh_->template particles_tensor_data<6>(attribute), attribute);

    // Write a parallel MPI VTK container file
#if USE_MPI
    if (mpi_rank == 0 && mpi_size > 1) {
      auto parallel_file = io_->output_file(attribute, ".pvtp", uuid_, step,
                                            max_steps, write_mpi_rank)
                               .string();

      vtk_writer->write_parallel_vtk(parallel_file, attribute, mpi_size, step,
                                     max_steps, 9);
    }
#endif
  }

  // VTK state variables
  for (auto const& vtk_statevar : vtk_statevars_) {
    unsigned phase_id = vtk_statevar.first;
    for (const auto& attribute : vtk_statevar.second) {
      std::string phase_attribute =
          "phase" + std::to_string(phase_id) + attribute;
      // Write state variables
      auto file =
          io_->output_file(phase_attribute, extension, uuid_, step, max_steps)
              .string();
      vtk_writer->write_scalar_point_data(
          file, mesh_->particles_statevars_data(attribute, phase_id),
          phase_attribute);
      // Write a parallel MPI VTK container file
#if USE_MPI
      if (mpi_rank == 0 && mpi_size > 1) {
        auto parallel_file = io_->output_file(phase_attribute, ".pvtp", uuid_,
                                              step, max_steps, write_mpi_rank)
                                 .string();
        unsigned ncomponents = 1;
        vtk_writer->write_parallel_vtk(parallel_file, phase_attribute, mpi_size,
                                       step, max_steps, ncomponents);
      }
#endif
    }
  }
}
#endif

//! Return if a mesh is isoparametric
template <unsigned Tdim>
bool mpm::MPMBase<Tdim>::is_isoparametric() {
  bool isoparametric = true;

  try {
    const auto mesh_props = io_->json_object("mesh");
    isoparametric = mesh_props.at("isoparametric").template get<bool>();
  } catch (std::exception& exception) {
    console_->warn(
        "{} {} Isoparametric status of mesh: {}\n Setting mesh as "
        "isoparametric.",
        __FILE__, __LINE__, exception.what());
    isoparametric = true;
  }
  return isoparametric;
}

//! Initialise loads
template <unsigned Tdim>
void mpm::MPMBase<Tdim>::initialise_loads(bool testing) {
  auto loads = io_->json_object("external_loading_conditions");
  // Initialise gravity loading
  if (loads.at("gravity").is_array() &&
      loads.at("gravity").size() == gravity_.size()) {
    for (unsigned i = 0; i < gravity_.size(); ++i) {
      gravity_[i] = loads.at("gravity").at(i);
    }
  } else {
    throw std::runtime_error("Specified gravity dimension is invalid");
  }

  // Create a file reader
  const std::string io_type =
      io_->json_object("mesh")["io_type"].template get<std::string>();
  auto reader = Factory<mpm::IOMesh<Tdim>>::instance()->create(io_type);

  // Read and assign particles surface tractions
  if (loads.find("particle_surface_traction") != loads.end()) {
    for (const auto& ptraction : loads["particle_surface_traction"]) {
      // Get the math function
      std::shared_ptr<FunctionBase> tfunction = nullptr;
      // If a math function is defined set to function or use scalar
      if (ptraction.find("math_function_id") != ptraction.end())
        tfunction = math_functions_.at(
            ptraction.at("math_function_id").template get<unsigned>());
      // Set id
      int pset_id = ptraction.at("pset_id").template get<int>();
      // Direction
      unsigned dir = ptraction.at("dir").template get<unsigned>();
      // Traction
      double traction = ptraction.at("traction").template get<double>();

      // Create particle surface tractions
      bool particles_tractions =
          mesh_->create_particles_tractions(tfunction, pset_id, dir, traction, testing);
      if (!particles_tractions)
        throw std::runtime_error(
            "Particles tractions are not properly assigned");
    }
  } else
    console_->warn("No particle surface traction is defined for the analysis");

  // Read and assign nodal concentrated forces
  if (loads.find("concentrated_nodal_forces") != loads.end()) {
    for (const auto& nforce : loads["concentrated_nodal_forces"]) {
      // Forces are specified in a file
      if (nforce.find("file") != nforce.end()) {
        std::string force_file = nforce.at("file").template get<std::string>();
        bool nodal_forces = mesh_->assign_nodal_concentrated_forces(
            reader->read_forces(io_->file_name(force_file, testing), testing), testing);
        if (!nodal_forces)
          throw std::runtime_error(
              "Nodal force file is invalid, forces are not properly "
              "assigned");
        set_node_concentrated_force_ = true;
      } else {
        // Get the math function
        std::shared_ptr<FunctionBase> ffunction = nullptr;
        if (nforce.find("math_function_id") != nforce.end())
          ffunction = math_functions_.at(
              nforce.at("math_function_id").template get<unsigned>());
        // Set id
        int nset_id = nforce.at("nset_id").template get<int>();
        // Direction
        unsigned dir = nforce.at("dir").template get<unsigned>();
        // Traction
        double force = nforce.at("force").template get<double>();

        // Read and assign nodal concentrated forces
        bool nodal_force = mesh_->assign_nodal_concentrated_forces(
            ffunction, nset_id, dir, force, testing);
        if (!nodal_force)
          throw std::runtime_error(
              "Concentrated nodal forces are not properly assigned");
        set_node_concentrated_force_ = true;
      }
    }
  } else
    console_->warn("No concentrated nodal force is defined for the analysis");
}

//! Initialise math functions
template <unsigned Tdim>
bool mpm::MPMBase<Tdim>::initialise_math_functions(const Json& math_functions) {
  bool status = true;
  try {
    // Get materials properties
    for (const auto& function_props : math_functions) {

      // Get math function id
      auto function_id = function_props["id"].template get<unsigned>();

      // Get function type
      const std::string function_type =
          function_props["type"].template get<std::string>();

      // Create a new function from JSON object
      auto function =
          Factory<mpm::FunctionBase, unsigned, const Json&>::instance()->create(
              function_type, std::move(function_id), function_props);

      // Add material to list
      auto insert_status =
          math_functions_.insert(std::make_pair(function->id(), function));

      // If insert material failed
      if (!insert_status.second) {
        status = false;
        throw std::runtime_error(
            "Invalid properties for new math function, fn insertion failed");
      }
    }
  } catch (std::exception& exception) {
    console_->error("#{}: Reading math functions: {}", __LINE__,
                    exception.what());
    status = false;
  }
  return status;
}

//! Node entity sets
template <unsigned Tdim>
void mpm::MPMBase<Tdim>::node_entity_sets(const Json& mesh_props,
                                          bool check_duplicates, bool testing) {
  try {
    if (mesh_props.find("entity_sets") != mesh_props.end()) {
      std::string entity_sets =
          mesh_props["entity_sets"].template get<std::string>();
      if (!io_->file_name(entity_sets, testing).empty()) {
        bool node_sets = mesh_->create_node_sets(
            (io_->entity_sets(io_->file_name(entity_sets, testing), "node_sets", testing)),
            check_duplicates, testing);
        if (!node_sets)
          throw std::runtime_error("Node sets are not properly assigned");
      }
    } else
      console_->warn("Entity set JSON not found");
  } catch (std::exception& exception) {
    if (!testing) console_->warn("#{}: Entity sets are undefined {} ", __LINE__,
                   exception.what());
    else throw;
  }
}

//! Node Euler angles
template <unsigned Tdim>
void mpm::MPMBase<Tdim>::node_euler_angles(
    const Json& mesh_props, const std::shared_ptr<mpm::IOMesh<Tdim>>& mesh_io, bool testing) {
  try {
    if (mesh_props.find("boundary_conditions") != mesh_props.end() &&
        mesh_props["boundary_conditions"].find("nodal_euler_angles") !=
            mesh_props["boundary_conditions"].end()) {
      std::string euler_angles =
          mesh_props["boundary_conditions"]["nodal_euler_angles"]
              .template get<std::string>();
      if (!io_->file_name(euler_angles, testing).empty()) {
        bool rotation_matrices = mesh_->compute_nodal_rotation_matrices(
            mesh_io->read_euler_angles(io_->file_name(euler_angles, testing)), testing);
        if (!rotation_matrices)
          throw std::runtime_error(
              "Euler angles are not properly assigned/computed");
      }
    } 
    else
      console_->warn("Euler angles JSON not found");
  } catch (std::exception& exception) {
    if (!testing) console_->warn("#{}: Euler angles are undefined {} ", __LINE__,
                   exception.what());
    else throw;
  }
}

// Nodal velocity constraints
template <unsigned Tdim>
void mpm::MPMBase<Tdim>::nodal_velocity_constraints(
    const Json& mesh_props, const std::shared_ptr<mpm::IOMesh<Tdim>>& mesh_io, bool testing) {
  try {
    // Read and assign velocity constraints
    if (mesh_props.find("boundary_conditions") != mesh_props.end() &&
        mesh_props["boundary_conditions"].find("velocity_constraints") !=
            mesh_props["boundary_conditions"].end()) {
      // Iterate over velocity constraints
      for (const auto& constraints :
           mesh_props["boundary_conditions"]["velocity_constraints"]) {
        // Velocity constraints are specified in a file
        if (constraints.find("file") != constraints.end()) {
          std::string velocity_constraints_file =
              constraints.at("file").template get<std::string>();
          bool velocity_constraints =
              constraints_->assign_nodal_velocity_constraints(
                  mesh_io->read_velocity_constraints(
                      io_->file_name(velocity_constraints_file, testing), testing), step_ * dt_, testing);
          if (!velocity_constraints)
            throw std::runtime_error(
                "Velocity constraints are not properly assigned");

        } else {
          // Get the math function
          std::shared_ptr<FunctionBase> mfunction = nullptr;
          // If a math function is defined set to function or use scalar
          if (constraints.find("math_function_id") != constraints.end())
            mfunction = math_functions_.at(
                constraints.at("math_function_id").template get<unsigned>());
          // Set id
          int nset_id = constraints.at("nset_id").template get<int>();
          // Direction
          unsigned dir = constraints.at("dir").template get<unsigned>();
          // Velocity
          double velocity = constraints.at("velocity").template get<double>();
          // Add velocity constraint to mesh
          auto velocity_constraint =
              std::make_shared<mpm::VelocityConstraint>(nset_id, mfunction, dir, velocity);
          bool velocity_constraints =
              constraints_->assign_nodal_velocity_constraint(
                  nset_id, velocity_constraint, step_ * dt_, testing);
          mesh_->create_nodal_velocity_constraint(nset_id, velocity_constraint, testing);
          if (!velocity_constraints)
            throw std::runtime_error(
                "Nodal velocity constraint is not properly assigned");
        }
      }
    } else
      console_->warn("Velocity constraints JSON not found");
  } catch (std::exception& exception) {
    if (!testing) console_->warn("#{}: Velocity constraints are undefined {} ", __LINE__,
                   exception.what());
    else throw;
  }
}



// Nodal frictional constraints
template <unsigned Tdim>
void mpm::MPMBase<Tdim>::nodal_frictional_constraints(
    const Json& mesh_props, const std::shared_ptr<mpm::IOMesh<Tdim>>& mesh_io, bool testing) {
  try {
    // Read and assign friction constraints
    if (mesh_props.find("boundary_conditions") != mesh_props.end() &&
        mesh_props["boundary_conditions"].find("friction_constraints") !=
            mesh_props["boundary_conditions"].end()) {
      // Iterate over velocity constraints
      for (const auto& constraints :
           mesh_props["boundary_conditions"]["friction_constraints"]) {
        // Friction constraints are specified in a file
        if (constraints.find("file") != constraints.end()) {
          std::string friction_constraints_file =
              constraints.at("file").template get<std::string>();
          bool friction_constraints =
              constraints_->assign_nodal_friction_constraints(
                  mesh_io->read_friction_constraints(
                      io_->file_name(friction_constraints_file, testing), testing), testing);
          if (!friction_constraints)
            throw std::runtime_error(
                "Friction constraints are not properly assigned");

        } else {

          // Set id
          int nset_id = constraints.at("nset_id").template get<int>();
          // Direction
          unsigned dir = constraints.at("dir").template get<unsigned>();
          // Sign n
          int sign_n = constraints.at("sign_n").template get<int>();
          // Friction
          double friction = constraints.at("friction").template get<double>();
          // Add friction constraint to mesh
          auto friction_constraint = std::make_shared<mpm::FrictionConstraint>(
              nset_id, dir, sign_n, friction);
          bool friction_constraints =
              constraints_->assign_nodal_frictional_constraint(
                  nset_id, friction_constraint, testing);
          if (!friction_constraints)
            throw std::runtime_error(
                "Nodal friction constraint is not properly assigned");
        }
      }
    } else
      console_->warn("Friction constraints JSON not found");

  } catch (std::exception& exception) {
    if (!testing) console_->warn("#{}: Friction conditions are undefined {} ", __LINE__,
                   exception.what());
    else throw;
  }
}

//! Cell entity sets
template <unsigned Tdim>
void mpm::MPMBase<Tdim>::cell_entity_sets(const Json& mesh_props,
                                          bool check_duplicates, bool testing) {
  try {
    if (mesh_props.find("entity_sets") != mesh_props.end()) {
      // Read and assign cell sets
      std::string entity_sets =
          mesh_props["entity_sets"].template get<std::string>();
      if (!io_->file_name(entity_sets, testing).empty()) {
        bool cell_sets = mesh_->create_cell_sets(
            (io_->entity_sets(io_->file_name(entity_sets, testing), "cell_sets", testing)),
            check_duplicates, testing);
        if (!cell_sets)
          throw std::runtime_error("Cell sets are not properly assigned");
      }
    } else
      console_->warn("Cell entity sets JSON not found");

  } catch (std::exception& exception) {
    if (!testing) console_->warn("#{}: Cell entity sets are undefined {} ", __LINE__,
                   exception.what());
    else throw;
  }
}

// Particles cells
template <unsigned Tdim>
void mpm::MPMBase<Tdim>::particles_cells(
    const Json& mesh_props,
    const std::shared_ptr<mpm::IOMesh<Tdim>>& particle_io, bool testing) {
  try {
    if (mesh_props.find("particle_cells") != mesh_props.end()) {
      std::string fparticles_cells =
          mesh_props["particle_cells"].template get<std::string>();

      if (!io_->file_name(fparticles_cells, testing).empty()) {
        bool particles_cells =
            mesh_->assign_particles_cells(particle_io->read_particles_cells(
                io_->file_name(fparticles_cells, testing), testing), testing);
        if (!particles_cells)
          throw std::runtime_error(
              "Particle cells are not properly assigned to particles");
      }
    } else
      console_->warn("Particle cells JSON not found, particles will be automatically located within the mesh");

  } catch (std::exception& exception) {
    if (!testing) console_->warn("#{}: Particle cells are undefined {} ", __LINE__,
                   exception.what());
    else throw;
  }
}

// Particles volumes
template <unsigned Tdim>
void mpm::MPMBase<Tdim>::particles_volumes(
    const Json& mesh_props,
    const std::shared_ptr<mpm::IOMesh<Tdim>>& particle_io, bool testing) {
  try {
    if (mesh_props.find("particles_volumes") != mesh_props.end()) {
      std::string fparticles_volumes =
          mesh_props["particles_volumes"].template get<std::string>();
      if (!io_->file_name(fparticles_volumes, testing).empty()) {
        bool particles_volumes =
            mesh_->assign_particles_volumes(particle_io->read_particles_volumes(
                io_->file_name(fparticles_volumes, testing), testing), testing);
        if (!particles_volumes)
          throw std::runtime_error(
              "Particles volumes are not properly assigned");
      }
    } else
      console_->warn("Particle volumes JSON not found, particles volume will be automatically determined");
  } catch (std::exception& exception) {
    if (!testing) console_->warn("#{}: Particle volumes are undefined {} ", __LINE__,
                   exception.what());
    else throw;
  }
}

// Particle velocity constraints
template <unsigned Tdim>
void mpm::MPMBase<Tdim>::particle_velocity_constraints(bool testing) {
  auto mesh_props = io_->json_object("mesh");
  // Create a file reader
  const std::string io_type =
      io_->json_object("mesh")["io_type"].template get<std::string>();
  auto reader = Factory<mpm::IOMesh<Tdim>>::instance()->create(io_type);

  try {
    if (mesh_props.find("boundary_conditions") != mesh_props.end() &&
        mesh_props["boundary_conditions"].find(
            "particles_velocity_constraints") !=
            mesh_props["boundary_conditions"].end()) {

      // Iterate over velocity constraints
      for (const auto& constraints :
           mesh_props["boundary_conditions"]
                     ["particles_velocity_constraints"]) {
        // Get the math function
          std::shared_ptr<FunctionBase> mfunction = nullptr;
          // If a math function is defined set to function or use scalar
          if (constraints.find("math_function_id") != constraints.end())
            mfunction = math_functions_.at(
                constraints.at("math_function_id").template get<unsigned>());
        // Set id
        int pset_id = constraints.at("pset_id").template get<int>();
        // Direction
        unsigned dir = constraints.at("dir").template get<unsigned>();
        // Velocity
        double velocity = constraints.at("velocity").template get<double>();
        // Add velocity constraint to mesh
        auto velocity_constraint =
            std::make_shared<mpm::VelocityConstraint>(pset_id, mfunction, dir, velocity);
        mesh_->create_particle_velocity_constraint(pset_id,
                                                   velocity_constraint, testing);
      }
    } else
      console_->warn("Particle velocity constraints JSON not found, no particle velocity constraints will be used");
  } catch (std::exception& exception) {
    if (!testing) console_->warn("#{}: Particle velocity constraints are undefined {} ",
                   __LINE__, exception.what());
    else throw;
  }
}

// Particles stresses
template <unsigned Tdim>
void mpm::MPMBase<Tdim>::particles_stresses(
    const Json& mesh_props,
    const std::shared_ptr<mpm::IOMesh<Tdim>>& particle_io, bool testing) {
  try {
    if (mesh_props.find("particles_stresses") != mesh_props.end()) {
      std::string fparticles_stresses =
          mesh_props["particles_stresses"].template get<std::string>();
      if (!io_->file_name(fparticles_stresses, testing).empty()) {

        // Get stresses of all particles
        const auto all_particles_stresses =
            particle_io->read_particles_stresses(
                io_->file_name(fparticles_stresses, testing), testing);

        // Read and assign particles stresses
        if (!mesh_->assign_particles_stresses(all_particles_stresses, testing))
          throw std::runtime_error(
              "Particles stresses are not properly assigned, particle stresses will be initialized to 0");
      }
    } else
      console_->warn("Particle stresses JSON not found");

  } catch (std::exception& exception) {
    if (!testing) console_->warn("#{}: Particle stresses are undefined {} ", __LINE__,
                   exception.what());
    else throw;
  }
}

// Particles velocities
template <unsigned Tdim>
void mpm::MPMBase<Tdim>::particles_velocities(
    const Json& mesh_props,
    const std::shared_ptr<mpm::IOMesh<Tdim>>& particle_io, bool testing) {
  try {
    if (mesh_props.find("particles_velocities") != mesh_props.end()) {
      std::string fparticles_velocities =
          mesh_props["particles_velocities"].template get<std::string>();
      if (!io_->file_name(fparticles_velocities, testing).empty()) {

        // Get velocities of all particles
        const auto all_particles_velocities =
            particle_io->read_particles_velocities(
                io_->file_name(fparticles_velocities, testing), testing);

        // Read and assign particles stresses
        if (!mesh_->assign_particles_velocities(all_particles_velocities, testing))
          throw std::runtime_error(
              "Particles velocities are not properly assigned");
      }
    } else
      console_->warn("Particle velocities JSON not found, particles velocities will be intiialized to 0");

  } catch (std::exception& exception) {
    if (!testing) console_->warn("#{}: Particle velocities are undefined {} ", __LINE__,
                   exception.what());
    else throw;
  }
}

//! Particle entity sets
template <unsigned Tdim>
void mpm::MPMBase<Tdim>::particle_entity_sets(bool check_duplicates, bool testing) {
  // Get mesh properties
  auto mesh_props = io_->json_object("mesh");
  // Read and assign particle sets
  try {
    if (mesh_props.find("entity_sets") != mesh_props.end()) {
      std::string entity_sets =
          mesh_props["entity_sets"].template get<std::string>();
      if (!io_->file_name(entity_sets, testing).empty()) {
        bool particle_sets = mesh_->create_particle_sets(
            (io_->entity_sets(io_->file_name(entity_sets, testing), "particle_sets", testing)),
            check_duplicates, testing);

        if (!particle_sets)
          throw std::runtime_error("Particle set creation failed");
      }
    } else
      console_->warn("Particle entity set JSON not found");

  } catch (std::exception& exception) {
    if (!testing) console_->warn("#{}: Particle sets are undefined {} ", __LINE__,
                   exception.what());
    else throw;
  }
}

// Initialise Damping
template <unsigned Tdim>
bool mpm::MPMBase<Tdim>::initialise_damping(const Json& damping_props) {

  // Read damping JSON object
  bool status = true;
  try {
    // Read damping type
    std::string type = damping_props.at("type").template get<std::string>();
    if (type == "Cundall") damping_type_ = mpm::Damping::Cundall;

    // Read damping factor
    damping_factor_ = damping_props.at("damping_factor").template get<double>();

  } catch (std::exception& exception) {
    console_->warn("#{}: Damping parameters are undefined {} ", __LINE__,
                   exception.what());
    status = false;
  }

  return status;
}

//! Domain decomposition
template <unsigned Tdim>
void mpm::MPMBase<Tdim>::mpi_domain_decompose(bool initial_step) {
#if USE_MPI
  // Initialise MPI rank and size
  int mpi_rank = 0;
  int mpi_size = 1;

  // Get MPI rank
  MPI_Comm_rank(MPI_COMM_WORLD, &mpi_rank);
  // Get number of MPI ranks
  MPI_Comm_size(MPI_COMM_WORLD, &mpi_size);

  if (mpi_size > 1 && mesh_->ncells() > 1) {

    // Initialize MPI
    MPI_Comm comm;
    MPI_Comm_dup(MPI_COMM_WORLD, &comm);

    auto mpi_domain_begin = std::chrono::steady_clock::now();
    console_->info("Rank {}, Domain decomposition started\n", mpi_rank);

#ifdef USE_GRAPH_PARTITIONING
    // Create graph object if empty
    if (initial_step || graph_ == nullptr)
      graph_ = std::make_shared<Graph<Tdim>>(mesh_->cells());

    // Find number of particles in each cell across MPI ranks
    mesh_->find_nglobal_particles_cells();

    // Construct a weighted DAG
    graph_->construct_graph(mpi_size, mpi_rank);

    // Graph partitioning mode
    int mode = 4;  // FAST
    // Create graph partition
    graph_->create_partitions(&comm, mode);
    // Collect the partitions
    auto exchange_cells = graph_->collect_partitions(mpi_size, mpi_rank, &comm);

    // Identify shared nodes across MPI domains
    mesh_->find_domain_shared_nodes();
    // Identify ghost boundary cells
    mesh_->find_ghost_boundary_cells();

    // Delete all the particles which is not in local task parititon
    if (initial_step) mesh_->remove_all_nonrank_particles();
    // Transfer non-rank particles to appropriate cells
    else
      mesh_->transfer_nonrank_particles(exchange_cells);

#endif
    auto mpi_domain_end = std::chrono::steady_clock::now();
    console_->info("Rank {}, Domain decomposition: {} ms", mpi_rank,
                   std::chrono::duration_cast<std::chrono::milliseconds>(
                       mpi_domain_end - mpi_domain_begin)
                       .count());
  }
#endif  // MPI
}

//! MPM pressure smoothing
template <unsigned Tdim>
void mpm::MPMBase<Tdim>::pressure_smoothing(unsigned phase) {
  // Assign pressure to nodes
  mesh_->iterate_over_particles(
      std::bind(&mpm::ParticleBase<Tdim>::map_pressure_to_nodes,
                std::placeholders::_1, phase));

#if USE_MPI
  int mpi_size = 1;

  // Get number of MPI ranks
  MPI_Comm_size(MPI_COMM_WORLD, &mpi_size);

  // Run if there is more than a single MPI task
  if (mpi_size > 1) {
    // MPI all reduce nodal pressure
    mesh_->template nodal_halo_exchange<double, 1>(
        std::bind(&mpm::NodeBase<Tdim>::pressure, std::placeholders::_1, phase),
        std::bind(&mpm::NodeBase<Tdim>::assign_pressure, std::placeholders::_1,
                  phase, std::placeholders::_2));
  }
#endif

  // Smooth pressure over particles
  mesh_->iterate_over_particles(
      std::bind(&mpm::ParticleBase<Tdim>::compute_pressure_smoothing,
                std::placeholders::_1, phase));
}

//! Initialise nonlocal mesh
template <unsigned Tdim>
void mpm::MPMBase<Tdim>::initialise_nonlocal_mesh(const Json& mesh_props, bool testing) {
  //! Shape function name
  const auto cell_type = mesh_props["cell_type"].template get<std::string>();
  try {
    // Initialise additional properties
    tsl::robin_map<std::string, double> nonlocal_properties;

    // Parameters for B-Spline elements
    if (cell_type.back() == 'B') {
      // Cell and node neighbourhood for quadratic B-Spline
      cell_neighbourhood_ = 1;
      node_neighbourhood_ = 3;

      // Initialise nonlocal node
      mesh_->iterate_over_nodes(
          std::bind(&mpm::NodeBase<Tdim>::initialise_nonlocal_node,
                    std::placeholders::_1));

      //! Read nodal type from entity sets
      if (mesh_props.find("nonlocal_mesh_properties") != mesh_props.end()) {
        const auto sf_type = mesh_props["nonlocal_mesh_properties"]["type"]
                                 .template get<std::string>();
        assert(sf_type == "BSPLINE");

        // Iterate over node type
        for (const auto& node_type :
             mesh_props["nonlocal_mesh_properties"]["node_types"]) {
          // Set id
          int nset_id = node_type.at("nset_id").template get<int>();
          // Direction
          unsigned dir = node_type.at("dir").template get<unsigned>();
          // Type
          unsigned type = node_type.at("type").template get<unsigned>();
          // Assign nodal nonlocal type
          mesh_->assign_nodal_nonlocal_type(nset_id, dir, type, testing);
        }
      }
    }
    // Parameters for LME elements
    else if (cell_type.back() == 'L') {
      //! Read nodal type from entity sets
      if (mesh_props.find("nonlocal_mesh_properties") != mesh_props.end()) {
        const auto sf_type = mesh_props["nonlocal_mesh_properties"]["type"]
                                 .template get<std::string>();
        assert(sf_type == "LME");

        // Gamma parameter
        const double gamma = mesh_props["nonlocal_mesh_properties"]["gamma"]
                                 .template get<double>();

        // Support tolerance
        double tol0 = 1.e-6;
        if (mesh_props["nonlocal_mesh_properties"].contains(
                "support_tolerance"))
          tol0 = mesh_props["nonlocal_mesh_properties"]["support_tolerance"]
                     .template get<double>();

        // Average mesh size
        double h;
        if (mesh_props["nonlocal_mesh_properties"].contains("mesh_size"))
          h = mesh_props["nonlocal_mesh_properties"]["mesh_size"]
                  .template get<double>();
        else
          h = mesh_->compute_average_cell_size();

        // Anisotropy parameter
        bool anisotropy = false;
        if (mesh_props["nonlocal_mesh_properties"].contains("anisotropy")) {
          anisotropy = mesh_props["nonlocal_mesh_properties"]["anisotropy"]
                           .template get<bool>();
          update_defgrad_ = true;
        }
        nonlocal_properties.insert(
            std::pair<std::string, bool>("anisotropy", anisotropy));

        // Calculate beta
        const double beta = gamma / (h * h);
        nonlocal_properties.insert(
            std::pair<std::string, double>("beta", beta));

        // Calculate support radius automatically
        const double r = std::sqrt(-std::log(tol0) / gamma) * h;
        nonlocal_properties.insert(
            std::pair<std::string, double>("support_radius", r));

        // Cell and node neighbourhood for LME
        cell_neighbourhood_ = static_cast<unsigned>(floor(r / h));
        node_neighbourhood_ = 1 + 2 * cell_neighbourhood_;
      }
    } else {
      throw std::runtime_error(
          "Unable to initialise nonlocal mesh for cell type: " + cell_type);
    }

    //! Update number of nodes in cell
    mesh_->upgrade_cells_to_nonlocal(cell_type, cell_neighbourhood_,
                                     nonlocal_properties);

  } catch (std::exception& exception) {
    if (!testing) console_->warn("{} #{}: initialising nonlocal mesh failed! ", __FILE__,
                   __LINE__, exception.what());
    else throw;
  }
}

//! Save shape functions to CSV files
template <unsigned Tdim>
void mpm::MPMBase<Tdim>::save_shapefn(const Json& mesh_props) {
  if (io_->nthreads()>1) {
    console_->warn("{} #{}: The shape function saving features only works for sequential simulations, no shape function will be saved", __FILE__, __LINE__);
    return;
  }
  std::vector<mpm::Index> save_nodes_shapefn;
  if (mesh_props.find("save_nodes_shapefn") != mesh_props.end()) {
    save_nodes_shapefn = mesh_props.at("save_nodes_shapefn").get<std::vector<mpm::Index>>();
    if (save_nodes_shapefn.empty()) {
      console_->warn("{} #{}: `save_nodes_shapefn` key exists but is empty, no shape function will be saved into CSV files", __FILE__, __LINE__);
    }
    else {

      int n_save_nodes = save_nodes_shapefn.size();

      // Create a grid 
      unsigned resolution;
      if (mesh_props.find("shapefn_resolution") != mesh_props.end()) resolution = mesh_props.at("shapefn_resolution").get<unsigned>();
      else {
        console_->warn("{} #{}: `save_nodes_shapefn` is used but `shapefn_resolution` is not defined, using 25 as default", __FILE__, __LINE__);
        resolution = 25;
      }
      bool save_grad;
      if (mesh_props.find("save_shapefn_grad") != mesh_props.end()) save_grad = mesh_props.at("save_shapefn_grad").get<bool>();
      else {
        console_->warn("{} #{}: `save_nodes_shapefn` is used but `save_shapefn_grad` is not defined, using false as default (gradient of the shape functions not saved)", __FILE__, __LINE__);
        save_grad = false;
      }
      Eigen::Matrix<double, Tdim*2, 1> extremum = mesh_->get_extremum();
      std::vector<std::vector<double>> grid;
      for (int i_dim=0; i_dim<Tdim; i_dim++) {
        std::vector<double> pts;
        double dx = (extremum(2*i_dim+1) - extremum(2*i_dim)) / (resolution-1);
        for (int i_x=0; i_x<resolution-1; ++i_x) {
          pts.push_back(extremum(2*i_dim) + dx*i_x);
        }
        pts.push_back(extremum(2*i_dim+1));
        grid.push_back(pts);
      }

      // Flatten the grid into a vector of coordinates
      std::vector<std::vector<double>> grid_pts_tmp = {{}};
      for (const auto& pts : grid) {
        std::vector<std::vector<double>> r;
        for (const auto& x : grid_pts_tmp) {
            for (const auto pt : pts) {
                r.push_back(x);
                r.back().push_back(pt);
            }
        }
        grid_pts_tmp = std::move(r);
      }
      std::vector<Eigen::Matrix<double, Tdim, 1>> grid_pts;
      for (std::vector<double>  pt: grid_pts_tmp) {
        Eigen::Matrix<double, Tdim, 1> pt_eig = Eigen::Map<Eigen::Matrix<double, Tdim, 1>, Eigen::Unaligned>(pt.data(), pt.size());
        grid_pts.push_back(pt_eig);
      }

      // Create a "ghost" particle to go through the mesh and get the shape functions values
      std::string particle_type = (Tdim == 2) ? "P2D" : "P3D";
      std::vector<unsigned> mat_ids{mesh_->first_mat_id()};
      mpm::Index pset_id = std::numeric_limits<unsigned>::max();
      mpm::Index ghost_id = mesh_->nparticles();


      // Create the CSV file path
      std::string results_path = io_->output_folder();
      std::stringstream file_path_strm;
      file_path_strm << results_path << "shapefn.csv";
      std::string file_path = file_path_strm.str();

      // Create the header of the CSV file
      std::stringstream header_strm;
      std::vector<std::string> dim_labels = {"x", "y", "z"};
      for (int i_dim=0; i_dim<Tdim; i_dim++) header_strm << dim_labels[i_dim] << "\t";
      for (std::vector<mpm::Index>::iterator node_id_ptr = save_nodes_shapefn.begin(); node_id_ptr < save_nodes_shapefn.end()-1; node_id_ptr++) {
        header_strm << "node_" << *node_id_ptr << "\t";
      }
      header_strm << "node_" << save_nodes_shapefn.back() << "\n";
      
      // Open the CSV file and write the header
      std::ofstream file, gradfile;
      file.open(file_path);
      file << header_strm.str();

      if (save_grad) {
        std::stringstream gradfile_path_strm;
        gradfile_path_strm << results_path << "gradshapefn.csv";
        gradfile.open(gradfile_path_strm.str());
        gradfile << header_strm.str();
      }

      // Loop over all grid points to get and write shape functions values
      for (int i_pt=0; i_pt<grid_pts.size(); i_pt++) {
        Eigen::Matrix<double, Tdim, 1> pt = grid_pts[i_pt];
        std::vector<Eigen::Matrix<double, Tdim, 1>> coords = {pt};
        mesh_->create_particles(particle_type, coords, mat_ids, pset_id, false);
        std::map<mpm::Index, double> mapped_shapefn = mesh_->get_mapped_shapefn(ghost_id);

        std::map<mpm::Index, double> mapped_gradshapefn;
        if (save_grad) mapped_gradshapefn = mesh_->get_mapped_gradshapefn(ghost_id);

        // Map the value of the shape functions to the nodes of interest
        std::map<mpm::Index, double> shapefn_csv_map, gradshapefn_csv_map;
        for (mpm::Index csv_node_id: save_nodes_shapefn) shapefn_csv_map[csv_node_id] = 0;

        for (auto iter = cbegin(mapped_shapefn); iter != cend(mapped_shapefn); ++iter) {
          if (std::find(save_nodes_shapefn.begin(), save_nodes_shapefn.end(), iter->first) != save_nodes_shapefn.end()) shapefn_csv_map[iter->first] = iter->second;
        }

        if (save_grad) {
          for (mpm::Index csv_node_id: save_nodes_shapefn) gradshapefn_csv_map[csv_node_id] = 0;
          for (auto iter = cbegin(mapped_gradshapefn); iter != cend(mapped_gradshapefn); ++iter) {
            if (std::find(save_nodes_shapefn.begin(), save_nodes_shapefn.end(), iter->first) != save_nodes_shapefn.end()) gradshapefn_csv_map[iter->first] = iter->second;
          }
        }

        // Write the line in the CSV file
          // Position
        for (int i_dim=0; i_dim<Tdim; i_dim++) file << std::scientific << pt(i_dim) << "\t";
        if (save_grad) {
          for (int i_dim=0; i_dim<Tdim; i_dim++) gradfile << std::scientific << pt(i_dim) << "\t";
        }
          // Shape function for all saved nodes0
        for (std::vector<mpm::Index>::iterator node_id_ptr = save_nodes_shapefn.begin(); node_id_ptr < save_nodes_shapefn.end()-1; node_id_ptr++) {
          file << std::scientific << shapefn_csv_map[*node_id_ptr] << "\t";
          if (save_grad) gradfile << std::scientific << gradshapefn_csv_map[*node_id_ptr] << "\t";
        }
          
        file << std::scientific << shapefn_csv_map[save_nodes_shapefn.back()] << "\n";
        if (save_grad) gradfile << std::scientific << gradshapefn_csv_map[save_nodes_shapefn.back()] << "\n";
        
        // Remove ghost particle and particle set
        mesh_->remove_particle_by_id(ghost_id);
        mesh_->remove_pset(pset_id);
      }

      // Close the CSV file
      file.close();
      if (save_grad) gradfile.close();
    }
  }
}
#ifndef MPM_MPM_SCHEME_H_
#define MPM_MPM_SCHEME_H_

#ifdef USE_GRAPH_PARTITIONING
#include "graph.h"
#endif

#include "mesh.h"
#include "timing.h"
#include "misc.h"

namespace mpm {

//! MPMScheme class
//! \brief Mpmscheme base class to support different stress update schemes
//! \tparam Tdim Dimension
template <unsigned Tdim>
class MPMScheme {
 public:
  //! Default constructor with mesh class
  MPMScheme(const std::shared_ptr<mpm::Mesh<Tdim>>& mesh, double dt);

  //! Intialize
  virtual inline void initialise();

  //! Compute nodal kinematics - map mass and momentum to nodes
  //! \param[in] phase Phase to smooth pressure
  virtual inline void compute_nodal_kinematics(std::string velocity_update, unsigned phase, unsigned step);

  //! Compute stress and strain
  //! \param[in] phase Phase to smooth pressure
  //! \param[in] pressure_smoothing Enable or disable pressure smoothing
  virtual inline void compute_stress_strain(unsigned phase,
                                            bool pressure_smoothing);

  //! Precompute stress and strain (empty call)
  //! \param[in] phase Phase to smooth pressure
  //! \param[in] pressure_smoothing Enable or disable pressure smoothing
  virtual inline void precompute_stress_strain(unsigned phase,
                                               bool pressure_smoothing) = 0;

  //! Postcompute stress and strain (empty call)
  //! \param[in] phase Phase to smooth postssure
  //! \param[in] postssure_smoothing Enable or disable postssure smoothing
  virtual inline void postcompute_stress_strain(unsigned phase,
                                                bool pressure_smoothing) = 0;

  //! Pressure smoothing
  //! \param[in] phase Phase to smooth pressure
  //! \param[in] pressure_smoothing Enable or disable pressure smoothing
  virtual inline void pressure_smoothing(unsigned phase);

  //! Compute forces
  //! \param[in] gravity Acceleration due to gravity
  //! \param[in] step Number of step in solver
  //! \param[in] concentrated_nodal_forces Boolean for if a concentrated force
  //! is applied or not
  virtual inline void compute_forces(
      const Eigen::Matrix<double, Tdim, 1>& gravity, unsigned phase,
      unsigned step, bool concentrated_nodal_forces, bool testing = false);

  //! Compute acceleration velocity position
  //! \param[in] velocity_update Velocity or acceleration update string
  //! \param[in] flip_coeff Mixture coefficient (think of a PIC-FLIP blend)
  //! \param[in] phase Phase of particle
  //! \param[in] damping_type Type of damping
  //! \param[in] damping_factor Value of critical damping
  virtual inline void compute_particle_kinematics(
      std::string velocity_update, double flip_coeff, unsigned phase,
      const std::string& damping_type, double damping_factor, unsigned step, bool update_defgrad);

  //! Compute particle location
  //! \param[in] locate_particles Flag to enable locate particles, if set to
  //! false, unlocated particles will be removed
  virtual inline void locate_particles(bool locate_particles, bool testing = false);

  //! Stress update scheme
  //! \retval scheme Stress update scheme
  virtual inline std::string scheme() const = 0;

  //! Get particle positions
  //! \retval ppos Vector of Eigen arrays, containing the positions of the particles
  virtual inline std::vector<Eigen::Matrix<double, Tdim, 1> > get_ppos();

  //! Get particle velocities
  //! \retval ppos Vector of Eigen arrays, containing the velocities of the particles
  virtual inline std::vector<Eigen::Matrix<double, Tdim, 1> > get_pvel();

  //! Get the maximum, over all material points, of the norm of the velocity gradient increase with respect to its previous value
  virtual inline double get_max_rel_norm_dgV(); 

 protected:
  //! Mesh object
  std::shared_ptr<mpm::Mesh<Tdim>> mesh_;
  //! Time increment
  double dt_;
  //! MPI Size
  int mpi_size_ = 1;
  //! MPI rank
  int mpi_rank_ = 0;
};  // MPMScheme class
}  // namespace mpm

#include "mpm_scheme.tcc"

#endif  // MPM_MPM_SCHEME_H_

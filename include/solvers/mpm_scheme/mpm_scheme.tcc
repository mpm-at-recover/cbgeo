//! Constructor of stress update with mesh
template <unsigned Tdim>
mpm::MPMScheme<Tdim>::MPMScheme(const std::shared_ptr<mpm::Mesh<Tdim>>& mesh,
                                double dt) {
  // Assign mesh
  mesh_ = mesh;
  // Assign time increment
  dt_ = dt;
#if USE_MPI
  // Get MPI rank
  MPI_Comm_rank(MPI_COMM_WORLD, &mpi_rank_);
  // Get number of MPI ranks
  MPI_Comm_size(MPI_COMM_WORLD, &mpi_size_);
#endif
}

//! Initialize nodes, cells and shape functions
template <unsigned Tdim>
inline void mpm::MPMScheme<Tdim>::initialise() {
#if TIMING_DEPTH >= 1
  // Shortcuts for std::chrono
  using namespace std::chrono;
  using seconds = duration<double>;

  // Declare start_time and end_time
  high_resolution_clock::time_point start_time, end_time;
#endif


  // Initialise nodes
#if TIMING_DEPTH >= 1
  start_time = high_resolution_clock::now();
#endif
  mesh_->iterate_over_nodes(
      std::bind(&mpm::NodeBase<Tdim>::initialise, std::placeholders::_1));
#if TIMING_DEPTH >= 1
  end_time = high_resolution_clock::now();
  timing::execution_times["reset_nodes"] += duration_cast<seconds>(end_time - start_time).count();
  timing::execution_count["reset_nodes"] += 1;
#endif

  // Activate nodes if some particles are in the range of their sape function
#if TIMING_DEPTH >= 1
  start_time = high_resolution_clock::now();
#endif
  mesh_->iterate_over_cells(
      std::bind(&mpm::Cell<Tdim>::activate_nodes, std::placeholders::_1));
#if TIMING_DEPTH >= 1
  end_time = high_resolution_clock::now();
  timing::execution_times["activate_nodes"] += duration_cast<seconds>(end_time - start_time).count();
  timing::execution_count["activate_nodes"] += 1;
#endif

  // Iterate over each particle to compute shapefn
#if TIMING_DEPTH >= 1
  start_time = high_resolution_clock::now();
#endif
  mesh_->iterate_over_particles(std::bind(
      &mpm::ParticleBase<Tdim>::compute_shapefn, std::placeholders::_1));
#if TIMING_DEPTH >= 1
  end_time = high_resolution_clock::now();
  timing::execution_times["compute_shapefn"] += duration_cast<seconds>(end_time - start_time).count();
  timing::execution_count["compute_shapefn"] += 1;
#endif
}

//! Compute nodal kinematics - map mass and momentum to nodes
template <unsigned Tdim>
inline void mpm::MPMScheme<Tdim>::compute_nodal_kinematics(std::string velocity_update, unsigned phase, unsigned step) {
#if TIMING_DEPTH >= 1
  // Shortcuts for std::chrono
  using namespace std::chrono;
  using seconds = duration<double>;

  // Declare start_time and end_time
  high_resolution_clock::time_point start_time, end_time;
#endif

  // Compute D matrix (for affine augmented velocity update, "apic", "aflip" and "anflip")
#if TIMING_DEPTH >= 1
  start_time = high_resolution_clock::now();
#endif
  if (velocity_update.rfind("a", 0)==0) {
    mesh_->iterate_over_particles(
      std::bind(&mpm::ParticleBase<Tdim>::compute_D,
                std::placeholders::_1));

    // Assign mass and momentum to nodes
    mesh_->iterate_over_particles(
        std::bind(&mpm::ParticleBase<Tdim>::map_mass_momentum_to_nodes_affine_augmented,
                  std::placeholders::_1, false));
  }
  else if (velocity_update.rfind("t", 0)==0) {
    // Assign mass and momentum to nodes
    mesh_->iterate_over_particles(
        std::bind(&mpm::ParticleBase<Tdim>::map_mass_momentum_to_nodes_taylor,
                  std::placeholders::_1));
  }
  else {
    // Assign mass and momentum to nodes
    mesh_->iterate_over_particles(
        std::bind(&mpm::ParticleBase<Tdim>::map_mass_momentum_to_nodes,
                  std::placeholders::_1));

  }
#if TIMING_DEPTH >= 1
  end_time = high_resolution_clock::now();
  timing::execution_times["map_mass_momentum_to_nodes"] += duration_cast<seconds>(end_time - start_time).count();
  timing::execution_count["map_mass_momentum_to_nodes"] += 1;
#endif

#if USE_MPI
  // Run if there is more than a single MPI task
  if (mpi_size_ > 1) {
    // MPI all reduce nodal mass
    mesh_->template nodal_halo_exchange<double, 1>(
        std::bind(&mpm::NodeBase<Tdim>::mass, std::placeholders::_1, phase),
        std::bind(&mpm::NodeBase<Tdim>::update_mass, std::placeholders::_1,
                  false, phase, std::placeholders::_2));
    // MPI all reduce nodal momentum
    mesh_->template nodal_halo_exchange<Eigen::Matrix<double, Tdim, 1>, Tdim>(
        std::bind(&mpm::NodeBase<Tdim>::momentum, std::placeholders::_1, phase),
        std::bind(&mpm::NodeBase<Tdim>::update_momentum, std::placeholders::_1,
                  false, phase, std::placeholders::_2));
  }
#endif

  // Compute nodal velocity
#if TIMING_DEPTH >= 1
  start_time = high_resolution_clock::now();
#endif
  mesh_->iterate_over_nodes_predicate(
      std::bind(&mpm::NodeBase<Tdim>::compute_velocity, std::placeholders::_1),
      std::bind(&mpm::NodeBase<Tdim>::status, std::placeholders::_1));

  if (step==0 && ( // if it is the initial iteration and we use the affine augmentation or a Taylor motion integration strategy
    velocity_update.rfind("a", 0)==0 || 
    velocity_update.rfind("t", 0)==0
  )) {
    int max_pass = 5000; // Maximum number of passes
    int i_pass = 1; // Counter of the number of passes
    double max_diff = std::numeric_limits<double>::epsilon(); // 1E-20; // Convergence criterion on the maximum (over all material points) of ||(gV_-prev_gV)|| / ||prev_gV|| (with ||.|| the Frobenius norm)
    double current_diff = Eigen::NumTraits<double>::infinity(); // current value of the difference
    while (current_diff>max_diff && i_pass<=max_pass) { // the case where the initial velocity gradient is zero sets current_diff to -1, which fails the test at the first pass, therefore no need to have a criterion on the absolute difference 
      // Compute the velocity gradient
      mesh_->iterate_over_particles(
          std::bind(&mpm::ParticleBase<Tdim>::compute_gV,
                    std::placeholders::_1, phase, true));
      
      current_diff = this->get_max_rel_norm_dgV();

      // Reset nodal data
      this->initialise();

      // Recompute the nodal velocity, with the updated velocity gradient
        // Mass and momentum transportation (different depending on the motion integration strategy)
      if (velocity_update.rfind("t", 0)==0) {
        mesh_->iterate_over_particles(
          std::bind(&mpm::ParticleBase<Tdim>::map_mass_momentum_to_nodes_taylor,
                    std::placeholders::_1));
      }
      else if (velocity_update.rfind("a", 0)==0) {
        mesh_->iterate_over_particles(
          std::bind(&mpm::ParticleBase<Tdim>::map_mass_momentum_to_nodes_affine_augmented,
                    std::placeholders::_1, true));
      }
        // Nodal velocity computation using mass and momentum 
      mesh_->iterate_over_nodes_predicate(
      std::bind(&mpm::NodeBase<Tdim>::compute_velocity, std::placeholders::_1),
      std::bind(&mpm::NodeBase<Tdim>::status, std::placeholders::_1));

      i_pass++;
    }

    if (i_pass==max_pass+1 && current_diff>max_diff) {// If the passes counter reached its maximum value but the convergence criterion wasn't met
      throw std::runtime_error("The initial velocity gradient iterative computation didn't converge (" 
      + misc_utl::to_scientific(current_diff) + " > " + misc_utl::to_scientific(max_diff) + ", " 
      + std::to_string(max_pass) + " iterations performed)");
    }
  }

#if TIMING_DEPTH >= 1
  end_time = high_resolution_clock::now();
  timing::execution_times["compute_nodal_velocity"] += duration_cast<seconds>(end_time - start_time).count();
  timing::execution_count["compute_nodal_velocity"] += 1;
#endif
  
  // Apply nodal velocity constraints
#if TIMING_DEPTH >= 1
  start_time = high_resolution_clock::now();
#endif
  mesh_->apply_nodal_velocity_constraints(step * dt_);
#if TIMING_DEPTH >= 1
  end_time = high_resolution_clock::now();
  timing::execution_times["apply_nodal_velocity_constraints"] += duration_cast<seconds>(end_time - start_time).count();
  timing::execution_count["apply_nodal_velocity_constraints"] += 1;
#endif
}

//! Initialize nodes, cells and shape functions
template <unsigned Tdim>
inline void mpm::MPMScheme<Tdim>::compute_stress_strain(
    unsigned phase, bool pressure_smoothing) {
#if TIMING_DEPTH >= 1
  // Shortcuts for std::chrono
  using namespace std::chrono;
  using seconds = duration<double>;

  // Declare start_time and end_time
  high_resolution_clock::time_point start_time, end_time;
#endif

  // Iterate over each particle to calculate strain
#if TIMING_DEPTH >= 1
  start_time = high_resolution_clock::now();
#endif
  mesh_->iterate_over_particles(std::bind(
      &mpm::ParticleBase<Tdim>::compute_strain, std::placeholders::_1, dt_));
#if TIMING_DEPTH >= 1
  end_time = high_resolution_clock::now();
  timing::execution_times["compute_strain"] += duration_cast<seconds>(end_time - start_time).count();
  timing::execution_count["compute_strain"] += 1;
#endif

  // Iterate over each particle to update particle volume
#if TIMING_DEPTH >= 1
  start_time = high_resolution_clock::now();
#endif
  mesh_->iterate_over_particles(std::bind(
      &mpm::ParticleBase<Tdim>::update_volume, std::placeholders::_1));
#if TIMING_DEPTH >= 1
  end_time = high_resolution_clock::now();
  timing::execution_times["update_volume"] += duration_cast<seconds>(end_time - start_time).count();
  timing::execution_count["update_volume"] += 1;
#endif

  // Pressure smoothing
#if TIMING_DEPTH >= 1
  start_time = high_resolution_clock::now();
#endif
  if (pressure_smoothing) this->pressure_smoothing(phase);
#if TIMING_DEPTH >= 1
  end_time = high_resolution_clock::now();
  timing::execution_times["pressure_smoothing"] += duration_cast<seconds>(end_time - start_time).count();
  timing::execution_count["pressure_smoothing"] += 1;
#endif

  // Iterate over each particle to compute stress
#if TIMING_DEPTH >= 1
  start_time = high_resolution_clock::now();
#endif
  mesh_->iterate_over_particles(std::bind(
      &mpm::ParticleBase<Tdim>::compute_stress, std::placeholders::_1));
#if TIMING_DEPTH >= 1
  end_time = high_resolution_clock::now();
  timing::execution_times["compute_stress"] += duration_cast<seconds>(end_time - start_time).count();
  timing::execution_count["compute_stress"] += 1;
#endif
}

//! Pressure smoothing
template <unsigned Tdim>
inline void mpm::MPMScheme<Tdim>::pressure_smoothing(unsigned phase) {
  // Assign pressure to nodes
  mesh_->iterate_over_particles(
      std::bind(&mpm::ParticleBase<Tdim>::map_pressure_to_nodes,
                std::placeholders::_1, phase));

#if USE_MPI
  // Run if there is more than a single MPI task
  if (mpi_size_ > 1)
    // MPI all reduce nodal pressure
    mesh_->template nodal_halo_exchange<double, 1>(
        std::bind(&mpm::NodeBase<Tdim>::pressure, std::placeholders::_1, phase),
        std::bind(&mpm::NodeBase<Tdim>::assign_pressure, std::placeholders::_1,
                  phase, std::placeholders::_2));
#endif

  // Smooth pressure over particles
  mesh_->iterate_over_particles(
      std::bind(&mpm::ParticleBase<Tdim>::compute_pressure_smoothing,
                std::placeholders::_1, phase));
}

// Compute forces
template <unsigned Tdim>
inline void mpm::MPMScheme<Tdim>::compute_forces(
    const Eigen::Matrix<double, Tdim, 1>& gravity, unsigned phase,
    unsigned step, bool concentrated_nodal_forces, bool testing) {
#if TIMING_DEPTH >= 1
  // Shortcuts for std::chrono
  using namespace std::chrono;
  using seconds = duration<double>;

  // Declare start_time and end_time
  high_resolution_clock::time_point start_time, end_time;
#endif

  // Iterate over each particle to compute nodal body force
#if TIMING_DEPTH >= 1
  start_time = high_resolution_clock::now();
#endif
  mesh_->iterate_over_particles(
      std::bind(&mpm::ParticleBase<Tdim>::map_body_force,
                std::placeholders::_1, gravity));
#if TIMING_DEPTH >= 1
  end_time = high_resolution_clock::now();
  timing::execution_times["map_body_force"] += duration_cast<seconds>(end_time - start_time).count();
  timing::execution_count["map_body_force"] += 1;
#endif

  // Apply particle traction and map to nodes
#if TIMING_DEPTH >= 1
  start_time = high_resolution_clock::now();
#endif
  mesh_->apply_traction_on_particles(step * dt_, testing);
#if TIMING_DEPTH >= 1
  end_time = high_resolution_clock::now();
  timing::execution_times["apply_traction_on_particles"] += duration_cast<seconds>(end_time - start_time).count();
  timing::execution_count["apply_traction_on_particles"] += 1;
#endif

  // Iterate over each node to add concentrated node force to external
  // force
#if TIMING_DEPTH >= 1
  start_time = high_resolution_clock::now();
#endif
  if (concentrated_nodal_forces)
    mesh_->iterate_over_nodes(
        std::bind(&mpm::NodeBase<Tdim>::apply_concentrated_force,
                  std::placeholders::_1, phase, (step * dt_)));
#if TIMING_DEPTH >= 1
  end_time = high_resolution_clock::now();
  timing::execution_times["apply_concentrated_force"] += duration_cast<seconds>(end_time - start_time).count();
  timing::execution_count["apply_concentrated_force"] += 1;
#endif
  // Spawn a task for internal force
  // Iterate over each particle to compute nodal internal force
#if TIMING_DEPTH >= 1
  start_time = high_resolution_clock::now();
#endif
  mesh_->iterate_over_particles(std::bind(
      &mpm::ParticleBase<Tdim>::map_internal_force, std::placeholders::_1));
#if TIMING_DEPTH >= 1
  end_time = high_resolution_clock::now();
  timing::execution_times["map_internal_force"] += duration_cast<seconds>(end_time - start_time).count();
  timing::execution_count["map_internal_force"] += 1;
#endif


#if USE_MPI
  // Run if there is more than a single MPI task
  if (mpi_size_ > 1) {
    // MPI all reduce external force
    mesh_->template nodal_halo_exchange<Eigen::Matrix<double, Tdim, 1>, Tdim>(
        std::bind(&mpm::NodeBase<Tdim>::external_force, std::placeholders::_1,
                  phase),
        std::bind(&mpm::NodeBase<Tdim>::update_external_force,
                  std::placeholders::_1, false, phase, std::placeholders::_2));
    // MPI all reduce internal force
    mesh_->template nodal_halo_exchange<Eigen::Matrix<double, Tdim, 1>, Tdim>(
        std::bind(&mpm::NodeBase<Tdim>::internal_force, std::placeholders::_1,
                  phase),
        std::bind(&mpm::NodeBase<Tdim>::update_internal_force,
                  std::placeholders::_1, false, phase, std::placeholders::_2));
  }
#endif
}

// Compute particle kinematics after measuring nodal acceleration
template <unsigned Tdim>
inline void mpm::MPMScheme<Tdim>::compute_particle_kinematics(
    std::string velocity_update, double flip_coeff, unsigned phase, 
    const std::string& damping_type, double damping_factor, unsigned step, bool update_defgrad) {

#if TIMING_DEPTH >= 1
  // Shortcuts for std::chrono
  using namespace std::chrono;
  using seconds = duration<double>;

  // Declare start_time and end_time
  high_resolution_clock::time_point start_time, end_time;
#endif

  // Check if damping has been specified and accordingly Iterate over
  // active nodes to compute nodal acceleration and updated nodal velocity
#if TIMING_DEPTH >= 1
  start_time = high_resolution_clock::now();
#endif

  damping_factor = damping_type == "Cundall" ? damping_factor : 0;
  mesh_->iterate_over_nodes_predicate(
      std::bind(&mpm::NodeBase<Tdim>::compute_acceleration_velocity,
                std::placeholders::_1, phase, dt_, damping_factor),
      std::bind(&mpm::NodeBase<Tdim>::status, std::placeholders::_1));

#if TIMING_DEPTH >= 1
  end_time = high_resolution_clock::now();
  timing::execution_times["compute_acceleration_velocity"] += duration_cast<seconds>(end_time - start_time).count();
  timing::execution_count["compute_acceleration_velocity"] += 1;
#endif

// Compute B matrix (for affine augmented velocity update, "apic", "aflip" and "anflip")
#if TIMING_DEPTH >= 1
  start_time = high_resolution_clock::now();
#endif
  if (velocity_update.rfind("a", 0)==0) {
    mesh_->iterate_over_particles(
        std::bind(&mpm::ParticleBase<Tdim>::compute_B,
                  std::placeholders::_1, phase));
  }

  // Compute velocity gradient at each material point (for Taylor* velocity updates, "tpic", "tflip")
  if (velocity_update.rfind("t", 0)==0) {
    mesh_->iterate_over_particles(
        std::bind(&mpm::ParticleBase<Tdim>::compute_gV,
                  std::placeholders::_1, phase, false));
  }
#if TIMING_DEPTH >= 1
  end_time = high_resolution_clock::now();
  timing::execution_times["motion_integration_specific_op"] += duration_cast<seconds>(end_time - start_time).count();
  timing::execution_count["motion_integration_specific_op"] += 1;
#endif

  // Iterate over each particle to compute their velocity
#if TIMING_DEPTH >= 1
  start_time = high_resolution_clock::now();
#endif
  mesh_->iterate_over_particles(
      std::bind(&mpm::ParticleBase<Tdim>::compute_particle_velocity,
                std::placeholders::_1, dt_, flip_coeff));
#if TIMING_DEPTH >= 1
  end_time = high_resolution_clock::now();
  timing::execution_times["compute_particle_velocity"] += duration_cast<seconds>(end_time - start_time).count();
  timing::execution_count["compute_particle_velocity"] += 1;
#endif

  // Apply particle velocity constraints
#if TIMING_DEPTH >= 1
  start_time = high_resolution_clock::now();
#endif
  mesh_->apply_particle_velocity_constraints(step * dt_);
#if TIMING_DEPTH >= 1
  end_time = high_resolution_clock::now();
  timing::execution_times["apply_particle_velocity_constraints"] += duration_cast<seconds>(end_time - start_time).count();
  timing::execution_count["apply_particle_velocity_constraints"] += 1;
#endif

  // Iterate over each particle to compute updated position
#if TIMING_DEPTH >= 1
  start_time = high_resolution_clock::now();
#endif
  mesh_->iterate_over_particles(
      std::bind(&mpm::ParticleBase<Tdim>::compute_updated_position,
                std::placeholders::_1, dt_, velocity_update));
#if TIMING_DEPTH >= 1
  end_time = high_resolution_clock::now();
  timing::execution_times["compute_updated_position"] += duration_cast<seconds>(end_time - start_time).count();
  timing::execution_count["compute_updated_position"] += 1;
#endif
}

// Locate particles
template <unsigned Tdim>
inline void mpm::MPMScheme<Tdim>::locate_particles(bool locate_particles, bool testing) {

  auto unlocatable_particles = mesh_->locate_particles_mesh(testing);

  if (!unlocatable_particles.empty() && locate_particles)
    throw std::runtime_error("Particle outside the mesh domain");
  // If unable to locate particles remove particles
  if (!unlocatable_particles.empty() && !locate_particles)
    for (const auto& remove_particle : unlocatable_particles)
      mesh_->remove_particle(remove_particle);
}

// Get particle positions
template <unsigned Tdim>
inline std::vector<Eigen::Matrix<double, Tdim, 1> > mpm::MPMScheme<Tdim>::get_ppos() {
  std::vector<Eigen::Matrix<double, Tdim, 1> > ppos;
  for (int ip = 0; ip < this->mesh_->nparticles(); ip++) {
    std::shared_ptr<mpm::ParticleBase<Tdim> > part = this->mesh_->get_particle(ip);
    ppos.emplace_back(part->coordinates());
  }
  return ppos;
}

// Get particle velocities
template <unsigned Tdim>
inline std::vector<Eigen::Matrix<double, Tdim, 1> > mpm::MPMScheme<Tdim>::get_pvel() {
  std::vector<Eigen::Matrix<double, Tdim, 1> > pvel;
  for (int ip = 0; ip < this->mesh_->nparticles(); ip++) {
    std::shared_ptr<mpm::ParticleBase<Tdim> > part = this->mesh_->get_particle(ip);
    pvel.emplace_back(part->velocity());
  }
  return pvel;
}

// Get the maximum, over all material points, of the norm of the velocity gradient increase with respect to its previous value
template <unsigned Tdim>
inline double mpm::MPMScheme<Tdim>::get_max_rel_norm_dgV() {
  double max_val = -1; // the values will be positive (they are norms), all will be more than -1 
  for (int ip = 0; ip < this->mesh_->nparticles(); ip++) {
    std::shared_ptr<mpm::ParticleBase<Tdim> > part = this->mesh_->get_particle(ip);
    double norm_dgV = part->get_rel_norm_dgV();
    if (norm_dgV > max_val) max_val = norm_dgV;
  }
  return max_val;
}
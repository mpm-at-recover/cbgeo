#include "element.h"
#include "cell.h"
#include "material.h"
#include "mpm.h"
#include "node.h"
#include "particle_base.h"
#include "io_mesh.h"
#include "quadrature.h"
#include "function_base.h"

void registerEverything() {
    mpm::registerElements();
    mpm::registerCells();
    mpm::registerMaterials();
    mpm::registerMPM();
    mpm::registerNodes();
    mpm::registerParticles();
    mpm::registerIO();
    mpm::registerQuadratures();
    mpm::registerFunctions();
}
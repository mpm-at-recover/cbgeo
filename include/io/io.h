#ifndef MPM_IO_H_
#define MPM_IO_H_

#include <fstream>
#include <memory>
#include <string>

#include <boost/filesystem.hpp>

#include "tclap/CmdLine.h"

#include "tsl/robin_map.h"
//! Alias for JSON
#include "json.hpp"
using Json = nlohmann::json;
// Speed log
#include "spdlog/spdlog.h"

#if USE_MPI
#include "mpi.h"
#endif

#include "data_types.h"
#include "version_management.h"

namespace fs = boost::filesystem;

namespace mpm {

//! Create a directory if it doesn't exist, and return its path 
std::string createDirectory(const std::string& path);

//! \brief Input/Output handler
class IO {
 public:
  //! Input file name (public only for python_model)
  std::string input_file_{"mpm.json"};

  //! Constructor with argc and argv
  //! \param[in] argc Number of input arguments
  //! \param[in] argv Input arguments
  IO(int argc, char** argv);

  //! Return number of threads
  unsigned nthreads() const;

  //! Return the CPU offset
  unsigned cpu_offset() const;

  //! Return the CPU offset
  std::string logfile() const;

  //! Return input file name of mesh/submesh/soil particles
  //! or an empty string if specified file for the key is not found
  //! \param[in] key Input key in JSON for the filename of
  //! mesh/submesh/soilparticles
  std::string file_name(const std::string& key, bool testing = false);

  //! Check if a file is present and valid
  //! \param[in] file_name Name of the file to check if it is present
  bool check_file(const std::string& file_name, bool testing = false);

  //! Return analysis
  std::string analysis_type() const;

  //! Return json analysis object
  Json analysis() const;

  //! Return json object
  Json json_object(const std::string& key) const;

  //! Return if the json file has the specified key
  bool json_has(const std::string& key) { return json_.find(key) != json_.end(); }


  //! Return post processing object
  Json post_processing() const;

  //! Return JSON analysis boolean
  bool analysis_bool(const std::string& key) const;

  //! Return the entity sets from the input set JSON file
  //! \param[in] filename File name
  //! \param[in] sets_type type of sets
  //! \retval entity_sets map of entity sets
  tsl::robin_map<mpm::Index, std::vector<mpm::Index>> entity_sets(
      const std::string& filename, const std::string& sets_type, bool testing = false);

  //! Return the output folder for the analysis
  std::string output_folder() const;

  //! Working directory
  std::string working_dir() const { return working_dir_; }

  //! Create output VTK file names (eg. velocity0000*.vtk)
  //! Generates a file based on attribute, current step and maxsteps
  //! \param[in] attribute Attribute being written (eg., velocity / stress)
  //! \param[in] file_extension File Extension (*.vtk or *.vtp)
  //! \param[in] step Current step
  //! \param[in] max_steps Total number of steps to be solved
  //! \param[in] parallel Write output as parallel file system
  //! \return file_name File name with the correct attribute and a VTK extension
  boost::filesystem::path output_file(const std::string& attribute,
                                      const std::string& file_extension,
                                      const std::string& analysis_id,
                                      unsigned step, unsigned max_steps,
                                      bool parallel = true);

 private:
  //! Number of parallel threads
  unsigned nthreads_{0};
  //! CPU offset for PythonModel
  unsigned cpu_offset_{0};
  //! Working directory
  std::string working_dir_;
  //! Input JSON object
  Json json_;
  //! Working directory
  std::string logfile_;
  //! Logger
  std::shared_ptr<spdlog::logger> console_;
  ;
};

}  // namespace mpm

#endif  // MPM_IO_H_

//! Compute B-Spline Basis Function using their parametric expression centered at 0
//! for single direction
double bspline::kernel(
    double point_coord, double nodal_coord, unsigned node_type,
    unsigned poly_order, double spacing_length, unsigned index) {
  double h = spacing_length;
  double x = point_coord-nodal_coord;
  double x2, h2, x3, h3;
  x2 = x*x;
  h2 = h*h;
  if (poly_order>2) {
    x3 = x2*x;
    h3 = h2*h;
  }
  if (node_type==0){ // regular i.e. internal node, type 3 for Nguyen2023 p. 106-107
    if (poly_order==2) { // Quadratic B-splines (from Steffen et al 2008)
      if ((-1.5*h <= x) && (x <= -.5*h)) return .5*x2/h2 + 1.5*x/h + 9./8.;
      else if ((-.5*h < x) && (x <= .5*h)) return -x2/h2 + .75;
      else if ((.5*h < x) && (x <= 1.5*h)) return .5*x2/h2 - 1.5*x/h + 9./8.;
      else return 0;
    }
    else if (poly_order==3) { // Cubic B-splines, e.g. Eq. (3.20) p. 107 Nguyen2023
      if ((-2*h <= x) && (x <= -h)) return 1./6.*x3/h3 + x2/h2 + 2*x/h + 4./3.;
      else if ((-h < x) && (x <= 0)) return -1./2.*x3/h3 - x2/h2 + 2./3.;
      else if ((0 < x) && (x <= h)) return 1./2.*x3/h3 - x2/h2 + 2./3.;
      else if ((h < x) && (x <= 2*h)) return -1./6.*x3/h3 + x2/h2 - 2*x/h + 4./3.;
      else return 0;
    }
  }

  else if (node_type==1){ // lower boundary node, type 1 for Nguyen2023 p. 106
    if (poly_order==2) { // Quadratic B-splines (from Steffen et al 2008)
      if ((0 <= x) && (x <= .5*h)) return 1 - 4./3.*x2/h2;
      else if ((.5*h < x) && (x <= 1.5*h)) return 2./3. * x2/h2 - 2*x/h + 3./2.;
      else return 0;
    }
    else if (poly_order==3) { // Cubic B-splines, Eq. (3.18) from Nguyen2023 p. 106
      if ((0 <= x) && (x <= h)) return 1./6.*x3/h3 - x/h + 1;
      else if ((h < x) && (x <= 2*h)) return -1./6.*x3/h3 + x2/h2 - 2*x/h + 4./3.;
      else return 0;
    }
  }

  else if (node_type==2){ // close to lower boundary, type 2 for Nguyen2023 p. 106
    if (poly_order==2) { // Quadratic B-splines (from Steffen et al 2008)
      if ((-h <= x) && (x <= -.5*h)) return 4./3.*(x2/h2 + 2*x/h+1);
      else if ((-.5*h < x) && (x <= .5*h)) return 1./6.*(-7*x2/h2 + x/h + 17./4.);
      else if ((.5*h < x) && (x <= 1.5*h)) return 1./2. * x2/h2 - 3./2.*x/h + 9./8.;
      else return 0;
    }
    else if (poly_order==3) { // Cubic B-splines, Eq. (3.19) from Nguyen2023 p. 106
      if ((-h <= x) && (x <= 0)) return -1./3.*x3/h3 - x2/h2 + 2./3.;
      else if ((0 < x) && (x <= h)) return 1./2.*x3/h3 - x2/h2 + 2./3.;
      else if ((h < x) && (x <= 2*h)) return -1./6.*x3/h3 + x2/h2 - 2*x/h + 4./3.;
      else return 0;
    }
  }

  else if (node_type==3) return bspline::kernel(nodal_coord-x, nodal_coord, 2, poly_order, h); // close to upper boundary, fall-back to symmetric case node_type 2
  else if (node_type==4) return bspline::kernel(nodal_coord-x, nodal_coord, 1, poly_order, h); // upper boundary, fall-back to symmetric node_type 1

  return 0;
}

//! Compute 1D B-Spline Basis Function Gradient
double bspline::gradient(
    double point_coord, double nodal_coord, unsigned node_type,
    unsigned poly_order, double spacing_length, unsigned index) {
  double h = spacing_length;
  double x = point_coord-nodal_coord;
  double x2, h2, h3;
  h2 = h*h;
  if (poly_order>2) {
    x2 = x*x;
    h3 = h2*h;
  }
  if (node_type==0){
    if (poly_order==2) { // Quadratic B-splines
      if ((-1.5*h <= x) && (x <= -.5*h)) return x/h2 + 1.5/h;
      else if ((-.5*h < x) && (x <= .5*h)) return -2*x/h2;
      else if ((.5*h < x) && (x <= 1.5*h)) return x/h2 - 1.5/h;
      else return 0;
    }
    else if (poly_order==3) { // Cubic B-splines
      if ((-2*h <= x) && (x <= -h)) return 1./2.*x2/h3 + 2*x/h2 + 2/h;
      else if ((-h < x) && (x <= 0)) return -3./2.*x2/h3 - 2*x/h2;
      else if ((0 < x) && (x <= h)) return 3./2.*x2/h3 - 2*x/h2;
      else if ((h < x) && (x <= 2*h)) return -1./2.*x2/h3 + 2*x/h2 - 2/h;
      else return 0;
    }
  }

  else if (node_type==1){
    if (poly_order==2) { // Quadratic B-splines
      if ((0 <= x) && (x <= .5*h)) return - 8./3.*x/h2;
      else if ((.5*h < x) && (x <= 1.5*h)) return 4./3. * x/h2 - 2/h;
      else return 0;
    }
    else if (poly_order==3) { // Cubic B-splines
      if ((0 <= x) && (x <= h)) return 1./2.*x2/h3 - 1./h;
      else if ((h < x) && (x <= 2*h)) return -1./2.*x2/h3 + 2.*x/h2 - 2/h;
      else return 0;
    }
  }

  else if (node_type==2){
    if (poly_order==2) { // Quadratic B-splines
      if ((-h <= x) && (x <= -.5*h)) return 8./3.*(x/h2 + 1/h);
      else if ((-.5*h < x) && (x <= .5*h)) return 1./6.*(-14*x/h2 + 1);
      else if ((.5*h < x) && (x <= 1.5*h)) return x/h2 - 3./2./h;
      else return 0;
    }
    else if (poly_order==3) { // Cubic B-splines
      if ((-h <= x) && (x <= 0)) return -x2/h3 - 2*x/h2;
      else if ((0 < x) && (x <= h)) return 3./2.*x2/h3 - 2*x/h2;
      else if ((h < x) && (x <= 2*h)) return -1./2.*x2/h3 + 2*x/h2 - 2./h;
      else return 0;
    }
  }

  else if (node_type==3) return -bspline::gradient(nodal_coord-x, nodal_coord, 2, poly_order, h);
  else if (node_type==4) return -bspline::gradient(nodal_coord-x, nodal_coord, 1, poly_order, h);

  return 0;
}
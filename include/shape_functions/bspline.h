#ifndef BSPLINE_H
#define BSPLINE_H

#include <stdlib.h>

namespace bspline {
    //! Compute 1D Boundary Modified B-Spline Basis Function
    //! \param[in] point_coord point coordinate in one direction
    //! \param[in] nodal_coord nodal coordinate in one direction
    //! dimension
    //! \param[in] node_type Node type associated with direction
    //! \param[in] poly_order Polynomial degree: p in N_i,p notation (of Piegl1995)
    //! \param[in] spacing_length length of a cell (only works for uniform meshes)
    //! \param[in] index Index: i in N_i,p notation (for an internal node, or boundary term)
    double kernel(double point_coord, double nodal_coord, unsigned node_type,
                  unsigned poly_order, double spacing_length, unsigned index = 0);

    //! Compute 1D Boundary Modified B-Spline Basis Function Gradient
    //! \param[in] point_coord point coordinate in one direction
    //! \param[in] nodal_coord nodal coordinate in one direction dimension
    //! \param[in] node_type Node type associated with direction
    //! \param[in] poly_order Polynomial degree: p in N_i,p notation (of Piegl1995)
    //! \param[in] spacing_length length of a cell (only works for uniform meshes)
    //! \param[in] index Index: i in N_i,p notation (for an internal node, or boundary term)
    double gradient(double point_coord, double nodal_coord, unsigned node_type,
                    unsigned poly_order, double spacing_length, unsigned index = 0);
}

#include "bspline.cpp"

#endif // BSPLINE_H
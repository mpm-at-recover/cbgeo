#ifndef PARTICLE_DATA_H_
#define PARTICLE_DATA_H_

#include "data_types.h"

namespace mpm {
// Define a struct of particle
typedef struct ParticleData {
  // Index
  mpm::Index id;
  // Mass
  double mass;
  // Volume
  double volume;
  // Pressure
  double pressure;
  // Coordinates
  double coord_x, coord_y, coord_z;
  // Displacement
  double displacement_x, displacement_y, displacement_z;
  // Natural particle size
  double nsize_x, nsize_y, nsize_z;
  // Velocity
  double velocity_x, velocity_y, velocity_z;
  // Stresses
  double stress_xx, stress_yy, stress_zz;
  double tau_xy, tau_yz, tau_xz;
  // Strains
  double strain_xx, strain_yy, strain_zz;
  double gamma_xy, gamma_yz, gamma_xz;
  // Volumetric strain centroid
  double epsilon_v;
  // Index
  mpm::Index cell_id;
  // Status
  bool status;
  // Material id
  unsigned material_id;
  // Number of state variables
  unsigned nstate_vars;
  // B matrix
  double B_xx, B_xy, B_xz;
  double B_yx, B_yy, B_yz;
  double B_zx, B_zy, B_zz;
  // D matrix
  double D_xx, D_xy, D_xz;
  double       D_yy, D_yz;
  double             D_zz;
  // Velocity gradient
  double gV_xx, gV_xy, gV_xz;
  double gV_yx, gV_yy, gV_yz;
  double gV_zx, gV_zy, gV_zz;
  // State variables (init to zero)
  std::vector<double> svars;

} ParticleData;

}  // namespace mpm

#endif  // PARTICLE_DATA_H_

namespace vmanage {

    inline std::string git_summary() {
        std::string git_suffix_ = GitMetadata::AnyUncommittedChanges() ? " (dirty)" : "";
        return GitMetadata::Describe() + git_suffix_;
    }

    // Helper function to execute a system command and capture the output
    inline std::string exec(const char* cmd) {
        std::array<char, 128> buffer;
        std::string result;
        std::shared_ptr<FILE> pipe(popen(cmd, "r"), pclose);
        if (!pipe) return "Not available";
        while (fgets(buffer.data(), buffer.size(), pipe.get()) != nullptr) {
            result += buffer.data();
        }
        result.erase(result.find_last_not_of("\n\r") + 1); // Trim trailing newline
        return result;
    }

    inline std::string extractVersion(const std::string& output) {
        std::regex versionRegex(R"(\d+(\.\d+)+)");  // Matches version number, e.g., "11.4.0"
        std::smatch match;
        if (std::regex_search(output, match, versionRegex)) {
            return match.str(0);  // Return the first matched version number
        }
        return "Not available";
    }

    // Define functions to print each dependency version

    inline std::string getGCCVersion() {
        if (std::system("gcc --version > /dev/null 2>&1") == 0) {
            std::string versionOutput = vmanage::exec("gcc --version | head -n 1");
            return extractVersion(versionOutput);
        }
        return "Not available";
    }

    inline std::string getGPlusPlusVersion() {
        if (std::system("g++ --version > /dev/null 2>&1") == 0) {
            std::string versionOutput = vmanage::exec("g++ --version | head -n 1");
            return extractVersion(versionOutput);
        }
        return "Not available";
    }

    inline std::string getGitVersion() {
        if (std::system("git --version > /dev/null 2>&1") == 0) {
            std::string versionOutput = vmanage::exec("git --version");
            return extractVersion(versionOutput);
        }
        return "Not available";
    }

    inline std::string getCMakeVersion() {
        if (std::system("cmake --version > /dev/null 2>&1") == 0) {
            std::string versionOutput = vmanage::exec("cmake --version | head -n 1");
            return extractVersion(versionOutput);
        }
        return "Not available";
    }

    inline std::string getBoostVersion() {
        #ifdef BOOST_LIB_VERSION
            return BOOST_LIB_VERSION;
        #else
            return "Not available";
        #endif
    }

    inline std::string getEigenVersion() {
        #ifdef EIGEN_MAJOR_VERSION
            return std::to_string(EIGEN_MAJOR_VERSION) + "." + std::to_string(EIGEN_MINOR_VERSION);
        #else
            return "Not available";
        #endif
    }

    inline std::string getTBBVersion() {
        #ifdef TBB_VERSION_MAJOR
            return std::to_string(TBB_VERSION_MAJOR) + "." +
                std::to_string(TBB_VERSION_MINOR) + "." +
                std::to_string(TBB_VERSION_PATCH);
        #else
            return "Not available";
        #endif
    }

    inline std::string getPythonVersion() {
        #if USE_PYTHON
            Py_Initialize();
            std::string versionOutput = Py_GetVersion();
            Py_Finalize();
            return extractVersion(versionOutput);
        #else
            return "Disabled";
        #endif
    }

    inline std::string getVTKVersion() {
        #if USE_VTK
            // Check that vtkVersion methods are available
            return std::to_string(vtkVersion::GetVTKMajorVersion()) + "." +
                std::to_string(vtkVersion::GetVTKMinorVersion()) + "." +
                std::to_string(vtkVersion::GetVTKBuildVersion());
        #else
            return "Disabled";
        #endif
    }

    inline std::string getQtVersion() {
        #if USE_VTK
            return std::to_string(QT_VERSION_MAJOR) + "." +
                std::to_string(QT_VERSION_MINOR) + "." +
                std::to_string(QT_VERSION_PATCH);
        #else
            return "VTK Disabled";
        #endif
    }

    inline std::string getMPIVersion() {
        #if USE_MPI
            int version, subversion;
            MPI_Get_version(&version, &subversion);
            return std::to_string(version) + "." + std::to_string(subversion);
        #else
            return "Disabled";
        #endif
    }

    // Define a function to print all dependencies in table format
    inline void printDependencyVersions() {
        // Print current program version
        std::cout << EXECUTABLE_NAME << "\t" << git_summary() << std::endl;

        // Column titles
        std::string name_column = "Dependency";
        std::string version_column = "Version";

        // Create a vector of dependencies and their version fetchers
        std::vector<std::pair<std::string, std::string>> dependencies = {
            {"GCC", getGCCVersion()},
            {"G++", getGPlusPlusVersion()},
            {"Git", getGitVersion()},
            {"CMake", getCMakeVersion()},
            {"Boost", getBoostVersion()},
            {"Eigen", getEigenVersion()},
            {"TBB", getTBBVersion()},
            {"Python", getPythonVersion()},
            {"VTK", getVTKVersion()},
            {"Qt", getQtVersion()},
            {"MPI", getMPIVersion()}
        };

        // Find the longest name
        int width = static_cast<int>(name_column.length()) + 2;  // Start with the width
        for (const auto& dep : dependencies) {
            width = std::max(width, static_cast<int>(dep.first.length()) + 1);
        }

        // Find the longest version string
        int max_len_version = static_cast<int>(version_column.length());
        for (const auto& dep : dependencies) {
            max_len_version = std::max(max_len_version, static_cast<int>(dep.second.length()));
        }

        std::string separator(width + max_len_version, '-');

        // Print the table
            // Table Header
        std::cout << std::setw(width) << separator << std::endl;
        std::cout << std::left << std::setw(width) << name_column << version_column << std::endl;
        std::cout << std::setw(width) << separator << std::endl;

            // Dependencies
        for (const auto& dep : dependencies) {
            std::cout << std::setw(width) << dep.first << dep.second << std::endl;
        }

        std::cout << std::setw(width) << separator << std::endl;
    }
}
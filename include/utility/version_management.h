#ifndef VERSION_MANAGEMENT_H
#define VERSION_MANAGEMENT_H

#include <iostream>
#include <iomanip>
#include <string>
#include <cstdlib>
#include <cstdio>
#include <regex>

#include <boost/version.hpp>
#include <tbb/version.h>

#if USE_PYTHON
#include <Python.h>
#endif

#if USE_VTK
#include <vtkVersion.h>
#include <QtGlobal>
#endif

#if USE_MPI
#include <mpi.h>
#endif

#include "git.h"

namespace vmanage {
    // Current version
    
    std::string git_summary();

    std::string exec(const char* cmd);
    std::string extractVersion(const std::string& output);

    std::string getGCCVersion();
    std::string getGPlusPlusVersion();
    std::string getGitVersion();
    std::string getCMakeVersion();
    std::string getBoostVersion();
    std::string getEigenVersion();
    std::string getTBBVersion();
    std::string getPythonVersion();
    std::string getVTKVersion();
    std::string getQtVersion();
    std::string getMPIVersion();

    void printDependencyVersions();
    
}

#include "version_management.cpp"

#endif // VERSION_MANAGEMENT_H
#ifndef TIMING_H
#define TIMING_H

#include <unordered_map>
#include <string>
#include <iomanip>
#include <fstream>

#include "json.hpp"
#include "tbb_tools.h"

using json = nlohmann::json;

namespace timing {
    // Declare the execution_times map (containing timing for each measured function)
    extern std::unordered_map<std::string, double> execution_times;

    // Declare the execution_count map (containing the number of time each measured function was called)
    extern std::unordered_map<std::string, int> execution_count;

    // Declare the total time variable
    extern double total_time;

    // Declare the main loop time variable
    extern double main_loop_time;

    // Declare the speed variable (in iteration/second)
    extern double speed;

    // Declare the vector which stores the speed values (the number of elements depends on the verbosity parameter)
    extern std::vector<double> speed_history;

    // Declare the vector which stores the elapsed time values (the number of elements depends on the verbosity parameter)
    extern std::vector<double> etime_history;

    // Declare the vector which stores the steps at which the speed was saved 
    extern std::vector<int> step_history;

    // Declare the variable containing the prefix for the choosen mpm_scheme model ("post" for USL or "pre" for USF) 
    extern std::string compute_stress_prefix;

    // Define the vector containing the execution order of the keys in map (Depth 0, has the order of the execution sequence)
    inline std::vector<std::string> exec_order = {"initialise_simulation", "verbosity", "inject_particles", "initialise_mpm_scheme", "initialise_contact", "compute_nodal_kinematics", "compute_contact_forces", "precompute_stress_strain", "compute_forces", "compute_particle_kinematics", "postcompute_stress_strain", "locate_particles", "write_csv"};

    // Define the map indicating which function is called by which other function (each second member of this map has the order of the execution sequence)
    inline std::unordered_map<std::string, std::vector<std::string>> sub_functions = {
        // Depth 1
#if TIMING_DEPTH >= 1
        {"initialise_mpm_scheme", {"reset_nodes", "activate_nodes", "compute_shapefn"}},
        {"compute_forces", {"map_body_force", "apply_traction_on_particles", "apply_concentrated_force", "map_internal_force"}},
        {"compute_nodal_kinematics", {"map_mass_momentum_to_nodes", "compute_nodal_velocity", "apply_nodal_velocity_constraints"}},
        {"compute_stress_strain", {"compute_strain", "update_volume", "pressure_smoothing","compute_stress"}},
        {"compute_particle_kinematics", {"compute_acceleration_velocity", "motion_integration_specific_op", "compute_particle_velocity", "apply_particle_velocity_constraints", "compute_updated_position"}}
#endif
        // Depth 2
#if TIMING_DEPTH >= 2
        ,{"compute_shapefn", {"shapefn_themselves", "gradient"}},
        {"map_internal_force", {"compute_internal_force", "update_nodal_internal_force"}}
#endif
    };

    // Declare the function that computes the speed
    void compute_speed(double current_time, int current_step, bool over_all_steps=false);

    // Declare the function that format durations into a human-readable form
    std::string format_duration(double duration_in_seconds);

    // Function to format a single line of output
    std::string format_output(const std::string& name, double time, double rel_time, int count, int name_width, int time_width, int rel_time_width, int count_width, int precision, int indentation);

    // Function to print header to file
    std::string format_header(int name_width, int time_width, int rel_time_width, int countWidth);

    // Function to print the total lines to file
    std::string format_total_lines(const std::string& name, double time, double rel_time, int name_width, int time_width, int rel_time_width, int count_width, int precision);

    // Declare the function that prints the detailed durations into a text file, for easy reading
    void print_detailed_times_to_file(const std::string& filename, bool sort_by_time=true, const std::string& context_line="", int precision=2);

    // Declare the function that prints the detailed durations into a Json file, for easy loading
    void generate_json_times(const std::string& filename);

    // Declare the function that prints the speed history into a CSV file, for tracking performances
    void write_performance_csv(const std::string& filename);

    // Declare a function that returns the formatted current time
    std::string current_date_time();
}

#include "timing.cpp"

#endif // TIMING_H
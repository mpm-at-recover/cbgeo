#ifndef TBB_TOOLS_H
#define TBB_TOOLS_H

#include <tbb/tbb.h>
#include <tbb/task_arena.h>
#include <tbb/parallel_for_each.h>
#include <tbb/queuing_mutex.h>

namespace tbb_tools {
    // Declare the nthreads variable, for easy access
    extern int nthreads;

    // Declare the task arena variable
    extern tbb::task_arena arena;

    // Declare function to initialise the task arena
    void initialise_task_arena(int nthreads);
}

#include "tbb_tools.cpp"

#endif // TBB_TOOLS_H
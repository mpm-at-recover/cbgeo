#ifndef MISC_H
#define MISC_H

#include <iostream>
#include <string>
#include <vector>

namespace misc_utl {
    // Declare function that splits a string
    std::vector<std::string> split_string(const std::string& s, char delimiter);

    // Declare function to format double values in scientific notation
    std::string to_scientific(double value);
}

#include "misc.cpp"

#endif // MISC_H
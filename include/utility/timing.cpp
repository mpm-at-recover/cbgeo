namespace timing {
    // Define the execution_times map
    inline std::unordered_map<std::string, double> execution_times;

    // Define the execution_count map
    inline std::unordered_map<std::string, int> execution_count;

    // Define the total time variable
    inline double total_time;

    // Define the main loop time variable
    inline double main_loop_time;

    // Define the speed variable (in iteration/second)
    inline double speed;

    // Define the vector which stores the speed values (the number of elements depends on the verbosity parameter)
    inline std::vector<double> speed_history;

    // Define the vector which stores the elapsed time values (the number of elements depends on the verbosity parameter)
    inline std::vector<double> etime_history;

    // Define the vector which stores the steps at which the other quantities were saved 
    inline std::vector<int> step_history;

    // Define the variable containing the prefix for the choosen mpm_scheme model ("post" for USL or "pre" for USF) 
    inline std::string compute_stress_prefix;

    // Define the function that computes the execution speed
    inline void compute_speed(double current_time, int current_step, bool over_all_steps) {
        // Put input data into history lists 
        timing::etime_history.push_back(current_time);
        step_history.push_back(current_step);

        if (step_history.size()>1) {
            double dt = timing::etime_history.end()[-1] - timing::etime_history.end()[-2];
            double dstep = timing::step_history.end()[-1] - timing::step_history.end()[-2];
            timing::speed = dstep / dt;
        }
        else timing::speed = 0;
        speed_history.push_back(timing::speed);

        if (over_all_steps) timing::speed = current_step / current_time; // overwrites the speed set above with the average
    }

    // Define the function that format durations into a human-readable form
    inline std::string format_duration(double duration_in_seconds) {
        if (duration_in_seconds < 1e-6) {
            return std::to_string(duration_in_seconds * 1e9) + " ns";
        } else if (duration_in_seconds < 1e-3) {
            return std::to_string(duration_in_seconds * 1e6) + " us";
        } else if (duration_in_seconds < 1) {
            return std::to_string(duration_in_seconds * 1e3) + " ms";
        } else if (duration_in_seconds < 60) {
            return std::to_string(duration_in_seconds) + " s";
        } else if (duration_in_seconds < 3600) {
            int minutes = duration_in_seconds / 60;
            double seconds = duration_in_seconds - minutes * 60;
            return std::to_string(minutes) + " min " + std::to_string(seconds) + " s";
        } else if (duration_in_seconds < 86400) {
            int hours = duration_in_seconds / 3600;
            int minutes = (duration_in_seconds - hours * 3600) / 60;
            double seconds = duration_in_seconds - hours * 3600 - minutes * 60;
            return std::to_string(hours) + " h " + std::to_string(minutes) + " min " + std::to_string(seconds) + " s";
        } else {
            int days = duration_in_seconds / 86400;
            int hours = (duration_in_seconds - days * 86400) / 3600;
            int minutes = (duration_in_seconds - days * 86400 - hours * 3600) / 60;
            double seconds = duration_in_seconds - days * 86400 - hours * 3600 - minutes * 60;
            return std::to_string(days) + " d " + std::to_string(hours) + " h " + std::to_string(minutes) + " min " + std::to_string(seconds) + " s";
        }
    }

    // Function to format a single line of output
    inline std::string format_output(const std::string& name, double time, double rel_time, int count, int name_width, int time_width, int rel_time_width, int count_width, int precision, int indentation) {
        std::stringstream main_ss, time_ss, rel_time_ss;
        time_ss << std::fixed << std::setprecision(precision) << time;
        rel_time_ss << std::fixed << std::setprecision(precision) << rel_time;

        std::string time_str = time_ss.str() + "s";
        std::string rel_time_str = rel_time_ss.str() + "%";

        main_ss << std::setw(name_width) << std::left << name << '|'
        << std::string(indentation, ' ') << std::setw(count_width-indentation) << std::left << count << '|'
        << std::string(indentation, ' ') << std::setw(time_width-indentation) << std::left << time_str << '|'
        << std::string(indentation, ' ') << std::setw(rel_time_width-indentation) << std::left << rel_time_str << std::endl;
        return main_ss.str();
    }

    // Function to print header to file
    inline std::string format_header(int name_width, int time_width, int rel_time_width, int count_width) {
        std::function<std::string(int, int)> left_space = [&](int title_len, int available_width) {
            return std::string((available_width-title_len)/2, ' ');
        };

        std::stringstream ss;
        ss << std::setw(name_width + time_width + rel_time_width + count_width) << std::setfill('-') << "" << std::setfill(' ') << std::endl;
        ss << std::setw(name_width) << std::left << left_space(4, name_width) + "Name" << '|'
        << std::setw(count_width) << std::left << left_space(5, count_width) + "Count" << '|'
        << std::setw(time_width) << std::left << left_space(4, time_width) + "Time" << '|'
        << std::setw(rel_time_width) << std::left << left_space(9, rel_time_width) + "Rel. time" << std::endl;
        ss << std::setw(name_width + time_width + rel_time_width + count_width) << std::setfill('-') << "" << std::setfill(' ') << std::endl;
        return ss.str();
    }

    // Function to print the total lines to file
    inline std::string format_total_lines(const std::string& name, double time, double rel_time, int name_width, int time_width, int rel_time_width, int count_width, int precision) {
        std::stringstream ss;

        std::stringstream main_ss, time_ss, rel_time_ss;
        time_ss << std::fixed << std::setprecision(precision) << time;
        rel_time_ss << std::fixed << std::setprecision(precision) << rel_time;

        std::string time_str = time_ss.str() + "s";
        std::string rel_time_str = rel_time_ss.str() + "%";


        main_ss << std::setw(name_width + time_width + rel_time_width + count_width) << std::setfill('-') << "" << std::setfill(' ') << std::endl;
        main_ss << std::setw(name_width) << std::left << name << "|"
        << std::setw(count_width+1) << std::right << "|"
        << std::setw(time_width) << std::left << time_str << '|'
        << std::setw(rel_time_width) << std::left << rel_time_str << std::endl;
        return main_ss.str();
    }

    inline void print_detailed_times_to_file(const std::string& filename, 
                                  bool sort_by_time, 
                                  const std::string& context_line,
                                  int precision) {
        // Open the output file
        std::ofstream outputFile(filename, std::ios::app);
        if (!outputFile.is_open()) {
            std::cerr << "Error: Unable to open file " << filename << std::endl;
            return;
        }

        // Append the context line:
        outputFile << context_line << std::endl;

        // Compute the width of the columns for better alignment
            // Useful variables
        const int maximum_depth = TIMING_DEPTH; // Maximum depth of the probing
        int max_indent_space = maximum_depth*4; // Maximum space taken by the indentation level

            // Determine the number of characters necessary for the name column
        int max_name_length = 0;
        for (const auto& pair : timing::execution_count) {
            int curr_len = pair.first.length();
            // Check if the current value is greater than the current maximum
            if (curr_len > max_name_length) {
                // Update the maximum value
                max_name_length = curr_len;
            }
        }

            // Determine the number of characters necessary for the count column
        int max_count = std::numeric_limits<int>::min();
        for (const auto& pair : timing::execution_count) {
            // Check if the current value is greater than the current maximum
            if (pair.second > max_count) {
                // Update the maximum value
                max_count = pair.second;
            }
        }
        int max_count_digit = std::to_string(max_count).length();

            // Determine the number of characters necessary for the time column
        double max_time = -std::numeric_limits<double>::infinity();
        for (const auto& pair : timing::execution_times) {
            // Round the value to the specified precision
            double rounded_value = std::round(pair.second * std::pow(10, precision)) / std::pow(10, precision);
            if (rounded_value > max_time) {
                // Update the maximum value
                max_time = rounded_value;
            }
        }
        int max_time_digit = std::to_string(max_time).length();

            // Finally, define the widths
        int nameWidth = std::max(max_name_length + max_indent_space, 4);
        int countWidth = std::max(max_count_digit + max_indent_space, 5);
        int timeWidth = std::max(max_time_digit + max_indent_space + 1, 4);
        int relTimeWidth = std::max(8 + precision + max_indent_space, 9);

        // Sort the exec_order based on the specified order or by time
        std::vector<std::string> ordered_functions = timing::exec_order;
        if (sort_by_time) {
            std::sort(ordered_functions.begin(), ordered_functions.end(), 
                    [&](const std::string& a, const std::string& b) {
                        return timing::execution_times[a] > timing::execution_times[b];
                    });
        }

        // Adapt the name of the compute_stress_strain function to the mpm_scheme used
        auto it = timing::sub_functions.find("compute_stress_strain");
        if (it != timing::sub_functions.end()) {
            // Extract the value associated with the key
            std::vector<std::string> sub_funcs = std::move(it->second);

            // Erase the old element and insert the new one with the updated key
            timing::sub_functions.emplace(timing::compute_stress_prefix + "compute_stress_strain", std::move(sub_funcs));
            timing::sub_functions.erase(it);
        }

        // Print header to file
        outputFile << format_header(nameWidth, timeWidth, relTimeWidth, countWidth);

        // Get the total time
        double totalTime = timing::total_time;

        // Define a recursive function to print function times and sub-functions
        std::function<void(const std::string&, int)> print_function_times = [&](const std::string& func_name, int depth) {
            double exec_time = timing::execution_times[func_name];
            double exec_count = timing::execution_count[func_name];
            double rel_time = (exec_time / totalTime) * 100.;

            // Print the function time with appropriate indentation
            std::string indented_name = std::string(depth * 4, ' ') + func_name;
            outputFile << format_output(indented_name, exec_time, rel_time, exec_count, nameWidth, timeWidth, relTimeWidth, countWidth, precision, depth * 4);

            // Check if the function has sub-functions
            if (timing::sub_functions.find(func_name) != timing::sub_functions.end()) {
                // Sort sub-functions based on the specified order or by time
                std::vector<std::pair<std::string, double>> subFunctionTimes;
                for (const auto& sub_func : timing::sub_functions[func_name]) {
                    subFunctionTimes.push_back({sub_func, timing::execution_times[sub_func]});
                }
                if (sort_by_time) {
                    std::sort(subFunctionTimes.begin(), subFunctionTimes.end(), 
                            [](const auto& a, const auto& b) {
                                return a.second > b.second; // Sort in descending order of time
                            });
                }

                // Print sub-functions with appropriate indentation
                for (const auto& sub_pair : subFunctionTimes) {
                    print_function_times(sub_pair.first, depth + 1);
                }
            }
        };

        double total_rel_time = 0.0; // Total relative time excluding sub-functions
        // Loop over exec_order to extract function times and print them recursively
        for (const auto& func_name : ordered_functions) {
            print_function_times(func_name, 0);
            total_rel_time += (timing::execution_times[func_name] / totalTime) * 100.;
        }

        // Print total of above line to file
        outputFile << format_total_lines("Total of above", (total_rel_time * totalTime) / 100.0, total_rel_time, nameWidth, timeWidth, relTimeWidth, countWidth, precision);
        
        // Print total line to file
        outputFile << format_total_lines("Total", totalTime, 100.0, nameWidth, timeWidth, relTimeWidth, countWidth, precision);

        // Bottom line of the table
        outputFile << std::setw(nameWidth + timeWidth + relTimeWidth + countWidth) << std::setfill('-') << "" << std::setfill(' ') << std::endl;

        // Close the output file
        outputFile.close();
    }

    inline void generate_json_times(const std::string& filename) {
        // Open the output file
        std::ofstream outputFile(filename);
        if (!outputFile.is_open()) {
            std::cerr << "Error: Unable to open file " << filename << std::endl;
            return;
        }

        // Compute the width of the columns for better alignment
        // Useful variables
        const int maximum_depth = TIMING_DEPTH; // Maximum depth of the probing

        // Get the total time
        double totalTime = timing::total_time;

        // Define a recursive function to fill the json object with function times and sub-functions
        std::function<void(json&, const std::string&, int)> fill_json_object = [&](json& func_object, const std::string& func_name, int depth) {
            double exec_time = timing::execution_times[func_name];
            double exec_count = timing::execution_count[func_name];
            double rel_time = (exec_time / totalTime) * 100.;

            // Add the current function time to the json object
            func_object["total"] = exec_time;

            // Check if the function has sub-functions
            if (timing::sub_functions.find(func_name) != timing::sub_functions.end()) {
                // Create a json object for sub-functions
                json sub_functions_object;

                // Fill the json object for sub-functions recursively
                for (const auto& sub_func : timing::sub_functions[func_name]) {
                    fill_json_object(sub_functions_object[sub_func], sub_func, depth + 1);
                }

                // Add the json object for sub-functions to the current function's json object
                func_object["sub_functions"] = sub_functions_object;
            }
        };

        // Create a top-level json object
        json top_level_object;

#if TIMING_DEPTH >= 0
        // Create a json object to hold the timing data
        json detailed_timing_object;

        // Fill the json object with function times and sub-functions recursively
        for (const auto& func_name : timing::exec_order) {
            fill_json_object(detailed_timing_object[func_name], func_name, 0);
        }

        // Add the detailed timing data to the top-level object
        top_level_object["detailed_timing"] = detailed_timing_object;
#endif

        // Add the other information to the top-level object
        top_level_object["average_speed"] = timing::speed;
        top_level_object["total_time"] = timing::total_time;
        top_level_object["main_loop_time"] = timing::main_loop_time;
        top_level_object["nthreads"] = tbb_tools::nthreads;

        // Write the JSON data to the output file
        outputFile << std::setw(4) << top_level_object << std::endl;

        // Close the output file
        outputFile.close();
    }

    inline void write_performance_csv(const std::string& filename) {
        // Open the output file
        std::ofstream outputFile(filename);
        if (!outputFile.is_open()) {
            std::cerr << "Error: Unable to open file " << filename << std::endl;
            return;
        }

        // Write the headers
        outputFile << "Step\tSpeed (iter/s)" << std::endl;

        // Write the data
        for (size_t i = 0; i < timing::speed_history.size(); ++i) {
            outputFile << timing::step_history[i] << "\t" << timing::speed_history[i] << std::endl;
        }

        // Close the output file
        outputFile.close();
    }

    // Define a function that returns the formatted current time
    inline std::string current_date_time() {
        auto now = std::chrono::system_clock::now();
        auto milliseconds = std::chrono::duration_cast<std::chrono::milliseconds>(now.time_since_epoch()) % 1000;
        time_t now_time = std::chrono::system_clock::to_time_t(now);
        tm time_info = *std::localtime(&now_time);
        char buffer[20];
        strftime(buffer, 20, "%Y-%m-%d %H:%M:%S", &time_info);
        return std::string(buffer) + '.' + std::to_string(milliseconds.count());
    }
}
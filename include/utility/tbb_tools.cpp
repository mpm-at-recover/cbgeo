namespace tbb_tools {
    // Define the nthreads variable, for easy access
    inline int nthreads;

    // Define the task arena variable
    inline tbb::task_arena arena;

    // Define function to initialise the task arena
    inline void initialise_task_arena(int nthreads) {
        tbb_tools::nthreads = nthreads;
        tbb_tools::arena.initialize(nthreads);
    }
}
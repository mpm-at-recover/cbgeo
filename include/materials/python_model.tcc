//! Read material properties
template <unsigned Tdim>
mpm::PythonModel<Tdim>::PythonModel(unsigned id, const Json& material_properties)
    : Material<Tdim>(id, material_properties) {
  try {
    density_ = material_properties.at("density").template get<double>();
    script_name_ = material_properties.at("script_path").template get<std::string>();
    function_name_ = material_properties.at("function_name").template get<std::string>();
    n_particles_ = material_properties.at("n_particles").template get<unsigned>();
    input_file_ = material_properties.at("input_file_path").template get<std::string>();
    n_threads_ = material_properties.at("nthreads").template get<unsigned>();
    cpu_offset_ = material_properties.at("cpu_offset").template get<unsigned>();
    log_mem_ = material_properties.at("log_memory_usage").template get<bool>();
    log_mem_step_ = material_properties.at("log_memory_step").template get<int>();
    children_pids_file_ = material_properties.at("children_pids_file").template get<std::string>();

    // Get RVE directory path
    rve_dir_ = input_file_.substr(0, input_file_.find_last_of("/")) + "/rve_data/";

    // Initial state variables
    bool last_svar_flag = true;
    int i = 0;
    while (last_svar_flag){
      // if an initial state variable can be read, append to init_svars_ its initial value
      try { 
        std::string var_name = "svars_";
        var_name += std::to_string(i);
        init_svars_.push_back(material_properties.at(var_name).template get<double>());
        i++;
      }
      // if an initial state variable can't be read, it means that there is no state variable left
      catch (Json::exception& except) {
        n_state_vars_ = i;
        last_svar_flag = false;
      }
    }

    properties_ = material_properties;

    // Make an unique id to name queues
    FILE *fp;
    fp = popen("cat /proc/sys/kernel/random/uuid", "r");
    if (fp != NULL) {
        if (fgets(uuid, 37, fp) == NULL)
            console_->error("Unique material's id could not be generated\n");
        pclose(fp);
    }
    
    // Get executable directory
    char path[PATH_MAX];
    char dest[PATH_MAX];
    memset(dest,0,sizeof(dest)); // readlink does not null terminate!
    pid_t pid = getpid();
    sprintf(path, "/proc/%d/exe", pid);
    if (readlink(path, dest, PATH_MAX) == -1) {
      perror("readlink");
    }
    std::string executable_path(dest);
    std::string executable_dir = executable_path.substr(0, executable_path.size()-3);

    // Construct python_model's path
    std::string python_exec = executable_path.append("-python");

    // Create a vector of CPU cores' id, to assign each material point to a core
    std::vector<int> core_ids;
    for (int i=0; i<n_particles_; i++) {
      core_ids.push_back(i%n_threads_+cpu_offset_);
    }

    // Construct input arguments
    char* argv[7];
    argv[0] = const_cast<char*> (python_exec.c_str());
    argv[1] = const_cast<char*> (input_file_.c_str());
    argv[2] = uuid;
    argv[6] = NULL;

    // Construct environment variables
    const char* home_tmp = std::getenv("HOME");
    std::string home(std::string("HOME=") + home_tmp);

    char* envp[5];
    envp[0] = (char*) home.c_str();
    char const *envp_1 = "OMP_NUM_THREADS=1";
    envp[1] = (char*) envp_1;
    char const *envp_2 = "DISPLAY=";
    envp[2] = (char*) envp_2;
    char envp_3[1024]; // Adjust the size as needed
    snprintf(envp_3, 1023, "LD_LIBRARY_PATH=%s", getenv("LD_LIBRARY_PATH"));
    envp[3] = (char*) envp_3;
    envp[4] = NULL;
    
    // Fork process so there is one subprocess per particle
    std::ofstream pids_file;
    if (!children_pids_file_.empty()) {
      std::string filePath = children_pids_file_;
      pids_file.open(filePath, std::ios::app);
    }
    int n;
    for (unsigned part_id = 0; part_id < n_particles_; part_id++) {
      // Convert part_id to char* so it can be passed to argv[4]
      char mp_id_str[10 + sizeof(char)]; 
      std::sprintf(mp_id_str, "%u", part_id);
      argv[4] = mp_id_str;

      // Convert core_id to char* so it can be passed to argv[5]
      char core_id_str[10 + sizeof(char)]; 
      std::sprintf(core_id_str, "%d", core_ids[part_id]);
      argv[5] = core_id_str;

      // Create named pipe (FIFO) for communication between children and parent
      this->create_fifo(part_id);
      std::string fifo_str = fifos_str[part_id];
      char fifo_name[fifo_str.length()+1];
      strcpy(fifo_name, fifo_str.c_str());

      argv[3] = fifo_name;

      // Create child process
      n = fork();
      if (n==0) {
        // Execute python_model to create material point's RVE
        execve(argv[0], argv, envp);
        exit(0);
      }
      else {
        // Add child's pid to pids vector and initialize mpm step counter
        processes_.push_back(n);
        if (!children_pids_file_.empty()) pids_file << std::to_string(n) << std::endl;
        mpm_step_.push_back(0);
      }
    }
  if (!children_pids_file_.empty()) pids_file.close();
  // If material wasn't correctly created, send an error to the console
  } catch (Json::exception& except) {
    console_->error("Material parameter not set: {} {}\n", except.what(),
                    except.id);
  }
}

//! Initialise state variables
template <unsigned Tdim>
mpm::dense_map mpm::PythonModel<Tdim>::initialise_state_variables() {
  mpm::dense_map state_vars;
  for (int i=0; i<n_state_vars_; ++i) {
    std::string var_name = "svars_";
    var_name += std::to_string(i);
    state_vars.insert(std::make_pair(var_name, init_svars_[i]));
  }
  return state_vars;
}

//! Finalize material
template <unsigned Tdim>
void mpm::PythonModel<Tdim>::finalize_material() {
  // Remove FIFOs from /tmp
  for (auto & fifo_str : fifos_str) {
    char fifo_chr[fifo_str.length()+1];
    strcpy(fifo_chr, fifo_str.c_str());
    std::remove(fifo_chr);
  }

  // Kill children (can't believe it's OK to say this)
  for (auto & pid : processes_) {
    kill(pid, SIGTERM);
  }

}

//! Get state variables names
template <unsigned Tdim>
std::vector<std::string> mpm::PythonModel<Tdim>::state_variables() const {
  std::vector<std::string> state_vars;
  for (int i = 0; i < n_state_vars_; ++i) {
    std::stringstream var_name;
    var_name << "svars_" << i;
    state_vars.push_back(var_name.str());
  }
  return state_vars;
}

//! Return updated stress after computing a dstress increment
template <unsigned Tdim>
Eigen::Matrix<double, 6, 1> mpm::PythonModel<Tdim>::compute_stress(
    const Vector6d& stress, const Vector6d& dstrain,
    const ParticleBase<Tdim>* ptr, mpm::dense_map* state_vars) {
      int ppid = getpid();

    char fifo_name[fifos_str[ptr->id()].length()+1];
    strcpy(fifo_name, fifos_str[ptr->id()].c_str());

    // Contruct input string
    std::ostringstream streamObj;
    for (unsigned i=0; i<6; i++) {
      streamObj << stress[i] << " ";
    }
    for (unsigned i=0; i<6; i++) {
      streamObj << dstrain[i] << " ";
    }
    for (unsigned i=0; i<n_state_vars_; i++) {
      std::string var_name = "svars_";
      var_name += std::to_string(i);
      streamObj << (*state_vars).at(var_name) << " ";
    }

    std::string input_str = streamObj.str();

    // Send input string
    this->send_message(ptr->id(), input_str);

    // Wait for output
    std::string output_str = receive_message(ptr->id());

    // Construct dstress output
    Eigen::Matrix<double, 6, 1> out_stress;
    std::vector<std::string> splited_output = str_split(output_str, ' ');
    for (unsigned i=0; i < n_state_vars_+6; i++) {
      if (i>5) { // the current index corresponds to a state variable 
        std::string var_name = "svars_";
        var_name += std::to_string(i-6);
        std::istringstream in1b(splited_output[i]);
        (*state_vars).at(var_name) = atof(in1b.str().c_str());
      }
      else { // the current index corresponds to a stress component 
        std::istringstream in1a(splited_output[i]);
        out_stress[i] = atof(in1a.str().c_str());
      }
    }

    if (log_mem_ && (mpm_step_[ptr->id()]% log_mem_step_ == 0)){
      std::string procPath = "/proc/" + std::to_string(processes_[ptr->id()]) + "/status";
      std::ifstream statusFile(procPath);
      if (!statusFile) console_->error("{} #{}: {}\n", __FILE__, __LINE__, "Failed to open the process file");

      std::string line;
      while (std::getline(statusFile, line)) {
          if (line.find("VmRSS:") != std::string::npos) {
              std::istringstream iss(line);
              std::string label;
              int ramUsage;
              std::string unit;

              // Extract the label ("VmRSS:") and the RAM usage value (integer) with unit
              if (iss >> label >> ramUsage >> unit) {
                  // Map unit suffixes to their corresponding multipliers (e.g., KB, MB, GB)
                  std::map<std::string, int> unitMultipliers = {
                      {"kB", 1},
                      {"MB", 1024},
                      {"GB", 1024 * 1024},
                      // Add more units as needed
                  };

                  // Convert the value to bytes (always)
                  if (unitMultipliers.find(unit) != unitMultipliers.end()) {
                      ramUsage *= unitMultipliers[unit];
                  }
                  else console_->warn("Memory unit in the process status file not recognized, assuming kB for export. Please go and code sthg few lines above if that is not the case");

                  
                  // Make sure the output directory exists
                  std::string out_dir = rve_dir_ + "RVE_" + std::to_string(ptr->id());
                  boost::filesystem::create_directories(out_dir);

                  std::string outfile_path = out_dir + "/memory_usage_bytes.csv";

                  // Get the current time
                  time_t currentTime = std::time(nullptr);
                  struct tm* timeInfo = std::localtime(&currentTime);

                  // Format the time as HH:MM:SS
                  char timeStr[9]; // HH:MM:SS + '\0'
                  std::strftime(timeStr, sizeof(timeStr), "%T", timeInfo);

                  // Append the RAM usage value and time to a text file
                  std::ofstream outputFile(outfile_path, std::ios::app);
                  if (outputFile) {
                      outputFile << timeStr << "\t" << ramUsage << std::endl;
                      outputFile.close();
                  } else {
                      console_->error("{} #{}: {}\n", __FILE__, __LINE__, "Failed to write to the memory log file");
                  }
              } else {
                console_->error("{} #{}: {}\n", __FILE__, __LINE__, "Failed to extract RAM usage value");
              }
              break;
          }
      }

    }
  mpm_step_[ptr->id()]++;
  return out_stress;
}

//! Create a message queue
template <unsigned Tdim>
void mpm::PythonModel<Tdim>::create_fifo(unsigned mp_id) {
  // Add material point's id to unique id
  char mp_id_str[10 + sizeof(char)]; 
  std::sprintf(mp_id_str, "%u", mp_id);
  std::string tmp_fifo_str = "/tmp/cbgeo-" + std::string(uuid) + "-" + mp_id_str;
  fifos_str.push_back(tmp_fifo_str);
  char fifo_chr[tmp_fifo_str.length()+1];
  strcpy(fifo_chr, tmp_fifo_str.c_str());

  // Create the fifo
  mkfifo(fifo_chr, 0666);
}

//! Send a message to a child
template <unsigned Tdim>
void mpm::PythonModel<Tdim>::send_message(unsigned mp_id, std::string message_str) {
  // Convert message to char*
  char message[900];
  sprintf(message, "%s", message_str.c_str());

  // Convert FIFO's name to char*
  char fifo_chr[100];
  sprintf(fifo_chr, "%s", fifos_str[mp_id].c_str());

  // Open FIFO in write mode
  int fd = open(fifo_chr, O_WRONLY);
  if (fd<0) {
    std::stringstream message;
    message << "Cannot find named pipe " << fifo_chr;
    console_->error("{} #{}: {} {}\n", __FILE__, __LINE__, message.str());
  }

  // Write message and close the FIFO
  // sleep(5);
  int res = write(fd, message, strlen(message)+1);
  if (res<0) console_->error("{} #{}: {}\n", __FILE__, __LINE__, "Cannot send message");
  close(fd);
}

//! Receive a message from a child
template <unsigned Tdim>
std::string mpm::PythonModel<Tdim>::receive_message(unsigned mp_id) {
  // Convert FIFO's name into char*
  char fifo_name[fifos_str[mp_id].length()+1];
  strcpy(fifo_name, fifos_str[mp_id].c_str());

  // Open FIFO in read mode
  int fd = open(fifo_name, O_RDONLY);
  if (fd<0) {
    std::stringstream message;
    message << "Cannot find named pipe " << fifo_name;
    console_->error("{} #{}: {} {}\n", __FILE__, __LINE__, message.str());
  } 

  // Create poll to wait for response (reduce CPU usage)
  struct pollfd *pfd = (struct pollfd *) calloc(1, sizeof(struct pollfd));
  pfd->fd = fd;
  pfd->events = POLLIN;
  pfd->revents = POLLOUT;

  // Get message and close FIFO
  char message[900];
  poll(pfd, 1, -1);
  int sz = read(fd, message, sizeof(message));
  if (sz<0) console_->error("{} #{}: {}\n", __FILE__, __LINE__, "Cannot receive message");
  free(pfd);
  close(fd);

  // Convert message into a string
  std::string message_str(message);

  return message_str;
}

//! Split string output received from children
template <unsigned Tdim>
std::vector<std::string> mpm::PythonModel<Tdim>::str_split(const std::string &s, char delim) {
    std::vector<std::string> result;
    std::stringstream ss (s);
    std::string item;
    while (getline (ss, item, delim)) {
        result.push_back (item);
    }
    return result;
}
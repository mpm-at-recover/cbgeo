#if USE_PYTHON

#define PY_SSIZE_T_CLEAN
#include <Python.h>

#include <limits>
#include <wchar.h>

#include "Eigen/Dense"

#include "material.h"

#include <sstream>

namespace pymat {

  // Global dense hash map type
  using dense_map = tsl::robin_map<std::string, double>;
  using Vector6d = Eigen::Matrix<double, 6, 1>;
  using Index = unsigned long long;
  using Json = nlohmann::json;

  //! Initialize the python session
  void init_session(const Json& material_properties);

  //! Initialise state variables
  dense_map initialise_state_variables();

  //! Return updated stress after computing a dstress increment
  Vector6d compute_stress(Vector6d stress, Vector6d dstrain, dense_map* state_vars, Index mp_id);

  //! Finalize python session
  void finalize_session();

  //! Format Python exception
  std::string get_python_exception();

   //! Logger
   std::unique_ptr<spdlog::logger> console_;
   //! Script name
   std::string script_name_;
   //! Function name
   std::string function_name_;
   //! Number of state variables
   int n_state_vars_;
   //! Initial values of state variables
   std::vector<double> init_svars_;
   //! Python module variable
   PyObject *pModule_;
   //! Python function variable
   PyObject *pFunc_;
   //! Python input arguments variable
   PyObject *pArgs_;
   //! Python return value variable
   PyObject *pValue_;
}  // namespace pymat

#include "_python_model_mat.tcc"

#endif // USE_PYTHON
void pymat::init_session(const Json& material_properties) {
  // Create a logger instance
  pymat::console_ = std::make_unique<spdlog::logger>(material_properties.at("logger_string").template get<std::string>(), mpm::stdout_sink);
  try {
    pymat::script_name_ = material_properties.at("script_path").template get<std::string>();
    pymat::function_name_ = material_properties.at("function_name").template get<std::string>();

    // Add simulation's directory as prefix to script_name_
    std::string input_file_path = material_properties.at("input_file_path").template get<std::string>();
      // Get input file name, thanks https://stackoverflow.com/a/14266139/16796697
    std::string input_file_name = input_file_path;
    std::string delimiter = "/";
    size_t pos = 0;
    while ((pos = input_file_name.find(delimiter)) != std::string::npos) {
      input_file_name.erase(0, pos + delimiter.length());
    }
    
      // Get simulation's directory
    pos = 0;
    std::string input_file_path_bis = input_file_path;
    std::string sim_directory;
    while ((pos = input_file_path_bis.find(input_file_name)) != std::string::npos) {
      sim_directory = input_file_path_bis.substr(0, pos);
      input_file_path_bis.erase(0, pos + input_file_name.length());
    }

      // Edit script_name_
    std::string script_name_original = pymat::script_name_;
    std::string sim_directory_tmp = sim_directory;
    sim_directory_tmp.append(pymat::script_name_);
    pymat::script_name_ = sim_directory_tmp;

    // Initial state variables
    bool last_svar_flag = true;
    int i = 0;
    while (last_svar_flag){
      // if an initial state variable can be read, append to init_svars_ its initial value
      try { 
        std::string var_name = "svars_";
        var_name += std::to_string(i);
        init_svars_.push_back(material_properties.at(var_name).template get<double>());
        i++;
      }
      // if an initial state variable can't be read, it means that there is no state variable left
      catch (Json::exception& except) {
        pymat::n_state_vars_ = i;
        last_svar_flag = false;
      }
    }

    // Create a string stream object to capture stdout
    std::stringstream stdout_buf;
    std::streambuf* old_stdout;
     
    // Initialize python's session
    Py_Initialize();

    // Add script's name to argv[0] of python's session
    std::string argv0_str = pymat::script_name_;
    argv0_str.append(".py");
    const char* argv0_char = argv0_str.c_str();
    size_t argv0_len = argv0_str.size();
    wchar_t** argv = new wchar_t*[1](); 
    argv[0] = Py_DecodeLocale(argv0_char, &argv0_len);
#if PY_VERSION_HEX >= 0x030B0000  // Python 3.11+
    PyConfig pyconfig;
    PyConfig_InitPythonConfig(&pyconfig);
    PyWideStringList_Append(&pyconfig.argv, argv[0]);
    Py_InitializeFromConfig(&pyconfig);
    PyConfig_Clear(&pyconfig);
#else
    PySys_SetArgvEx(1, argv, 0);  // For Python < 3.11
#endif
    delete[] argv;

    // Import useful modules and print python version
    PyRun_SimpleString("import sys, os");

    // Change principal working directory (move to simulation's directory)
    std::stringstream ss;
    ss << "os.chdir('" << sim_directory << "')";
    PyRun_SimpleString(ss.str().c_str());

    // Add current working directory to python paths in python's session 
    PyRun_SimpleString("sys.path.insert(1, os.getcwd())");

    // Import user's script
    PyObject *pName;
    const char* script_name_char = script_name_original.c_str();
    pName = PyUnicode_DecodeFSDefault(script_name_char);
    pymat::pModule_ = PyImport_Import(pName);
    Py_DECREF(pName);

    // Check if user's script was correctly imported
    if (pymat::pModule_ == NULL) {
      if (PyErr_Occurred())
        pymat::console_->error("Python raised an error when trying to import the script `{}`: \n{}", pymat::script_name_+".py", pymat::get_python_exception()); 
    }

    // Import user's function
    pymat::pFunc_ = PyObject_GetAttrString(pymat::pModule_, function_name_.c_str());

    // Check if user's function was correctly imported
    if (!(pymat::pFunc_ && PyCallable_Check(pymat::pFunc_))) {
      if (PyErr_Occurred())
          PyErr_Print();
        pymat::console_->error("Python raised an error when trying to load the function `{}` in the script `{}`: \n{}", pymat::function_name_, pymat::script_name_+".py", pymat::get_python_exception());
    }

  // If material wasn't correctly created, send an error to the console
  } catch (Json::exception& except) {
    pymat::console_->error("Material parameter not set: {} {}\n", except.what(),
                    except.id);
  }
}


//! Initialise state variables
pymat::dense_map pymat::initialise_state_variables() {
  dense_map state_vars;
  for (int i=0; i<n_state_vars_; ++i) {
    std::string var_name = "svars_";
    var_name += std::to_string(i);
    state_vars.insert(std::make_pair(var_name, init_svars_[i]));
  }
  return state_vars;
}

//! Return updated stress after computing a dstress increment
pymat::Vector6d pymat::compute_stress(
    Vector6d stress, Vector6d dstrain, dense_map* state_vars, Index mp_id) {

      pymat::Vector6d dstress(Vector6d::Zero()); // the stress increment computed below
      int i;

      // Prepare input for user's python function
      pArgs_ = PyTuple_New(n_state_vars_ + 7);
      for (i = 0; i < n_state_vars_ + 7; ++i) {
        if (i==0) { // the current index corresponds to the material point id
          pValue_ = PyLong_FromUnsignedLong(mp_id);
        }
        else if (i>6) { // the current index corresponds to a state variable 
          std::string var_name = "svars_";
          var_name += std::to_string(i-7);
          pValue_ = PyFloat_FromDouble((*state_vars).at(var_name));
        }
        else { // the current index corresponds to a strain component 
          pValue_ = PyFloat_FromDouble(dstrain[i-1]);
        }
        // Check if the value was correctly read
        if (!pValue_) {
          Py_DECREF(pArgs_);
          Py_DECREF(pModule_);
          console_->error("An input parameter for Python function {} in script {} could not be read\n", function_name_, script_name_+".py");
          return stress + dstress;
        }
        // Add value to python's input tuple
        PyTuple_SetItem(pArgs_, i, pValue_);
      }
      // Getting user's python function output (executing python constitutive relation)
      pValue_ = PyObject_CallObject(pFunc_, pArgs_); // pValue_ is now a python tuple containing output elements (it is no longer a single input value)
      Py_DECREF(pArgs_);
      // Check if the function was correctly called
      if (pValue_ == NULL) {
        Py_DECREF(pFunc_);
        Py_DECREF(pModule_);
        PyErr_Print();
        console_->error("An error occurred while calling the Python function {} in script {}\n", function_name_, script_name_+".py");
        return stress + dstress;
      }
      // Assign python's return values to c++ variables 
      else {
        for (i = 0; i < n_state_vars_ + 6; ++i) {
          if (i>5) { // the current index corresponds to a state variable 
            std::string var_name = "svars_";
            var_name += std::to_string(i-6);
            (*state_vars).at(var_name) = PyFloat_AsDouble(PyTuple_GetItem(pValue_, i));
          }
          else { // the current index corresponds to a stress component 
            dstress[i] = PyFloat_AsDouble(PyTuple_GetItem(pValue_, i));
          }
        }
      }
  return (stress + dstress);
}

//! Finalize python session
void pymat::finalize_session() {
  Py_XDECREF(pFunc_);
  Py_DECREF(pModule_);
  Py_Finalize();
}

//! Format Python exception
std::string pymat::get_python_exception() {

  // Create a stringstream to capture the error output
  std::stringstream errorStream;

  // Save the current error state
  PyObject* ptype, * pvalue, * ptraceback;
  PyErr_Fetch(&ptype, &pvalue, &ptraceback);

  // Redirect the error output to the stringstream
  PyErr_NormalizeException(&ptype, &pvalue, &ptraceback);

  PyObject* tracebackModule = PyImport_ImportModule("traceback");
  PyObject* formatExceptionFunc = PyObject_GetAttrString(tracebackModule, "format_exception");
  PyObject* formatResult = PyObject_CallFunctionObjArgs(formatExceptionFunc, ptype, pvalue, ptraceback, NULL);

  if (formatResult != NULL && PyList_Check(formatResult)) {
      Py_ssize_t size = PyList_Size(formatResult);
      for (Py_ssize_t i = 0; i < size; ++i) {
          PyObject* item = PyList_GetItem(formatResult, i);
          if (item != NULL && PyUnicode_Check(item)) {
              const char* errorLine = PyUnicode_AsUTF8(item);
              errorStream << errorLine;
          }
      }
  }

  Py_XDECREF(formatResult);
  Py_XDECREF(formatExceptionFunc);
  Py_XDECREF(tracebackModule);

  // Restore the error state
  PyErr_Restore(ptype, pvalue, ptraceback);

  // Get the captured error message from the stringstream
  std::string except_str = errorStream.str();


  std::stringstream output;
  std::stringstream inputStream(except_str);
  std::string line;
  
  while (std::getline(inputStream, line)) {
      output << "\t|" << line << '\n';
  }
  
  return output.str(); 
}
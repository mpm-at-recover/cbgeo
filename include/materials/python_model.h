#ifndef MPM_MATERIAL_PYTHON_MODEL_H_
#define MPM_MATERIAL_PYTHON_MODEL_H_
#define PY_SSIZE_T_CLEAN

#include <limits>
#include <wchar.h>

#include "Eigen/Dense"

#include "material.h"

#include <signal.h>
#include <poll.h>
#include <iostream>
#include <fstream>
#include <boost/filesystem.hpp>

namespace mpm {

//! PythonModel class
//! \brief Use a custom law defined in a python script
//! \details PythonModel class stresses and strains
//! \tparam Tdim Dimension
template <unsigned Tdim>
class PythonModel : public Material<Tdim> {
 public:
  //! Define a vector of 6 dof
  using Vector6d = Eigen::Matrix<double, 6, 1>;
  //! Define a Matrix of 6 x 6
  using Matrix6x6 = Eigen::Matrix<double, 6, 6>;

  //! Constructor with id
  //! \param[in] material_properties Material properties
  PythonModel(unsigned id, const Json& material_properties);

  //! Destructor
  ~PythonModel() override{};

  //! Delete copy constructor
  PythonModel(const PythonModel&) = delete;

  //! Delete assignement operator
  PythonModel& operator=(const PythonModel&) = delete;

  //! Initialise history variables
  //! \retval state_vars State variables with history
  mpm::dense_map initialise_state_variables() override;

  //! Finalize material
  void finalize_material() override;

  //! State variables
  std::vector<std::string> state_variables() const override;

  //! Compute stress
  //! \param[in] stress Stress
  //! \param[in] dstrain Strain
  //! \param[in] particle Constant point to particle base
  //! \param[in] state_vars History-dependent state variables
  //! \retval updated_stress Updated value of stress
  Vector6d compute_stress(const Vector6d& stress, const Vector6d& dstrain,
                          const ParticleBase<Tdim>* ptr,
                          mpm::dense_map* state_vars) override;

  //! Create a message queue
  void create_fifo(unsigned mp_id);

  //! Send a message to a child
  void send_message(unsigned mp_id, std::string message_char);

  //! Receive a message from a child
  std::string receive_message(unsigned mp_id);

  //! Split string output received from children
  std::vector<std::string> str_split(const std::string &s, char delim);

 protected:
  //! material id
  using Material<Tdim>::id_;
  //! Material properties
  using Material<Tdim>::properties_;
  //! Logger
  using Material<Tdim>::console_;

 private:
  //! Density
  double density_{std::numeric_limits<double>::max()};
  //! Script name
  std::string script_name_;
  //! Function name
  std::string function_name_;
  //! Number of particles
  unsigned n_particles_;
  //! Path of the input file
  std::string input_file_;
  //! Number of threads
  unsigned n_threads_;
  //! CPU offset
  unsigned cpu_offset_;
  //! Vector of children PIDs
  std::vector<int> processes_;
  //! Number of state variables
  int n_state_vars_;
  //! Initial values of state variables
  std::vector<double> init_svars_;
  //! Memory usage logging files flag
  bool log_mem_;
  //! Memory usage logging files step
  int log_mem_step_;
  //! File for storing children PIDs 
  std::string children_pids_file_;
  //! MPM step, for logging (not ideal to have a counter here again)
  std::vector<int> mpm_step_;
  //! RVE directory ptah
  std::string rve_dir_;

  //! Material's uuid
  char uuid[37];
  //! FIFO's name vector
  std::vector<std::string> fifos_str;

};  // PythonModel class
}  // namespace mpm

#include "python_model.tcc"

#endif  // MPM_MATERIAL_PYTHON_MODEL_H_
